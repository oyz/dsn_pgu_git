
#ifndef  _SCPI_H_
#define  _SCPI_H_

// scpi
////#define _SCPI_DEBUG_ //$$ SCPI server debug
//#define _SCPI_DEBUG_WCMSG_

#define DATA_BUF_SIZE_SCPI   2048 //$$ init

//#define MAX_CNT_STAY_SOCK_ESTABLISHED 0x00100000
#define MAX_CNT_STAY_SOCK_ESTABLISHED 0 // for no time limit to stay established


int32_t scpi_tcps(uint8_t sn, uint8_t* buf, uint16_t port); //$$ scpi server

#endif   // _SCPI_H_
