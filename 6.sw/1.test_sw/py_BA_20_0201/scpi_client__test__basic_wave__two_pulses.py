## scpi_client__test__basic_wave.py
import socket
#import time
from time import sleep
import sys

# connect(), close() ... style

# https://docs.python.org/3/library/socket.html
# https://docs.python.org/2/library/socket.html

## control parameters 

HOST = '192.168.168.123'  # The server's hostname or IP address
HOST2 = '192.168.172.1'  # The server's hostname or IP address
PORT = 5025               # The port used by the server
TIMEOUT = 5.3 # socket timeout
SO_SNDBUF = 2048
SO_RCVBUF = 32768
INTVAL = 0.1 # sec for waiting before recv()
BUF_SIZE_NORMAL = 2048
BUF_SIZE_LARGE = 16384
TIMEOUT_LARGE = TIMEOUT*10

ss = None # socket

## command strings ########################################################
cmd_str__IDN = b'*IDN?\n'
cmd_str__RST = b'*RST\n'
#
cmd_str__PGEP_EN = b':PGEP:EN'
#
cmd_str__PGU_PWR            = b':PGU:PWR'
cmd_str__PGU_OUTP           = b':PGU:OUTP'
cmd_str__PGU_DCS_TRIG       = b':PGU:DCS:TRIG'
cmd_str__PGU_DCS_DAC0_PNT   = b':PGU:DCS:DAC0:PNT'
cmd_str__PGU_DCS_DAC1_PNT   = b':PGU:DCS:DAC1:PNT'
cmd_str__PGU_DCS_RPT        = b':PGU:DCS:RPT'
cmd_str__PGU_FDCS_TRIG      = b':PGU:FDCS:TRIG'
cmd_str__PGU_FDCS_DAC0      = b':PGU:FDCS:DAC0'
cmd_str__PGU_FDCS_DAC1      = b':PGU:FDCS:DAC1'
cmd_str__PGU_FDCS_RPT       = b':PGU:FDCS:RPT'
cmd_str__PGU_FREQ           = b':PGU:FREQ'
cmd_str__PGU_OFST_DAC0      = b':PGU:OFST:DAC0'
cmd_str__PGU_OFST_DAC1      = b':PGU:OFST:DAC1'
cmd_str__PGU_GAIN_DAC0      = b':PGU:GAIN:DAC0'
cmd_str__PGU_GAIN_DAC1      = b':PGU:GAIN:DAC1'
#


## functions #############################################################

def scpi_open (timeout=TIMEOUT):
	try:
		ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		ss.settimeout(timeout)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, SO_SNDBUF)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, SO_RCVBUF) # 8192 16384 32768 65536
	except OSError as msg:
		ss = None
		print('error in socket: ', msg)
		raise
	return ss

def scpi_connect (ss, HOST, PORT):
	try:
		ss.connect((HOST, PORT))
	except OSError as msg:
		ss.close()
		ss = None
		print('error in connect: ', msg)
		raise


def scpi_close (ss):
	try:
		ss.close()
	except:
		if ss == None:
			print('error: ss==None')
		raise

def scpi_comm_resp_ss (ss, cmd_str, buf_size=BUF_SIZE_NORMAL, intval=INTVAL) :
	try:
		print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# recv data until finding the sentinel '\n'
	try:
		data = ss.recv(buf_size) # try 1024 131072 524288
		# try   
		while (1):
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
	except:
		print('error in recv')
		raise
	#
	## check response 
	if (len(data)>20):
		print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		print('Received:', repr(data))
	#
	# NG response check 
	if data[0:2]==b'NG':
		print('Received: NG as response')
		#input('Press Enter key!')
		#sleep(3)
	#
	return data


# scpi command for numeric block response
def scpi_comm_resp_numb_ss (ss, cmd_str, buf_size=BUF_SIZE_LARGE, intval=INTVAL, timeout_large=TIMEOUT_LARGE) :
	try:
		print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# cmd: ":PGEP:PO#HBC 524288\n"
	# rsp: "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
	#
	# recv data until finding the sentinel '\n' // TODO: numeric block
	# but check the sentinel after the data byte count is met.
	#
	# read timeout
	to = ss.gettimeout()
	#print(to)
	# increase timeout
	ss.settimeout(timeout_large)
	#
	try:
		# find the numeric head : must 10 in data 
		data = ss.recv(buf_size)
		while True:
			if len(data)>=10:
				break
			data = data + ss.recv(buf_size)
		#
		#print('header: ', repr(data[0:10])) # header
		#
		# find byte count 
		byte_count = int(data[3:9])
		#print('byte_count=', repr(byte_count)) 
		#
		# collect all data by byte count
		count_to_recv = byte_count + 10 + 1# add header count #add /n
		while True:
			if len(data)>=count_to_recv:
				break
			data = data + ss.recv(buf_size)
		#
		# check the sentinel 
		while True:
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
		#
	except:
		print('error in recv')
		raise
	#
	if (len(data)>20):
		print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		print('Received:', repr(data))
	#
	# timeout back to prev
	ss.settimeout(to)
	#
	data = data[10:(10+byte_count)]
	print('data:', data[0:20].hex(),  ' (first 20 bytes)')
	#
	return [byte_count, data]
	
## test routines ###########################################################################

## open socket and connect scpi server
try:
	ss = scpi_open()
	scpi_connect(ss, HOST, PORT)
except socket.timeout:
	ss = scpi_open()
	scpi_connect(ss, HOST2, PORT)
except:
	raise


###########################################################################

### scpi : *IDN?
print('\n>>> {} : {}'.format('Test',cmd_str__IDN))
rsp = scpi_comm_resp_ss(ss, cmd_str__IDN)
print('hex code rcvd: ' + rsp.hex())
print('string rcvd: ' + repr(rsp))


### :PGEP:EN
# :PGEP:EN ON|OFF <NL>			
# :PGEP:EN? <NL>			
#
# ":PGEP:EN ON\n"
# ":PGEP:EN OFF\n"
# ":PGEP:EN?"
print('\n>>> {} : {}'.format('Test',cmd_str__PGEP_EN))
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b' ON\n')
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b'?\n')


### scpi command: ":PGU:PWR"
# :PGU:PWR ON|OFF <NL>			
# :PGU:PWR? <NL>			
#
# ":PGU:PWR ON\n"
# ":PGU:PWR OFF\n"
# ":PGU:PWR?"
#

### power on 
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_PWR))
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b' ON\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')

### scpi command: ":PGU:OUTP" #### // TODO: cmd_str__PGU_OUTP
# :PGU:OUTP ON|OFF <NL>			
# :PGU:OUTP? <NL>			
#
# ":PGU:OUTP ON\n"
# ":PGU:OUTP OFF\n"
# ":PGU:OUTP?"
#

### output on
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_OUTP))
scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b' ON\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b'?\n')


###########################################################################

### scpi command: ":PGU:FREQ" 
# set DAC update freq based on 100kHz
#
# :PGU:FREQ nnnn <NL>			
#
# ":PGU:FREQ 4000 \n"  // for 400.0 MHz
# ":PGU:FREQ 2000 \n"  // for 200.0 MHz
# ":PGU:FREQ 1000 \n"  // for 100.0 MHz
# ":PGU:FREQ 0800 \n"  // for 080.0 MHz
# ":PGU:FREQ 0500 \n"  // for 050.0 MHz
# ":PGU:FREQ 0200 \n"  // for 020.0 MHz
#
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FREQ))
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 4000 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 2000 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 1000 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0800 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0500 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0200 \n') #OK
#
scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 2000 \n') # 200MHz
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 1000 \n') # 100MHz
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0500 \n') # 50MHz


### scpi command: ":PGU:OFST:DAC0" 
# cmd_str__PGU_OFST_DAC0      = b':PGU:OFST:DAC0'
#
# :PGU:OFST:DAC0? <NL>			
# :PGU:OFST:DAC0 #Hnnnnnnnn <NL>			
#
# // data = {DAC_ch1_aux, DAC_ch2_aux}
# // DAC_ch#_aux = {PN_Pol_sel, Source_Sink_sel, 0000, 10 bit data}
# //                PN_Pol_sel      = 0/1 for P/N
# //                Source_Sink_sel = 0/1 for Source/Sink
#
#  AD9783 offset current spec: 
#    0x000 for 0.0mA; 0x200 for 1.0mA; 0x3FF for 2.0mA
#
# offset DAC : 0x140 0.625mA, AUX2N active[7] (1) , sink current[6] (1) 
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_OFST_DAC0))
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+b' #HC141C140 \n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+b' #H00000000 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+b'?\n')

### scpi command: ":PGU:OFST:DAC1" 
# cmd_str__PGU_OFST_DAC1      = b':PGU:OFST:DAC1'
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_OFST_DAC1))
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+b' #HC141C140 \n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+b' #H00000000 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+b'?\n')

### scpi command: ":PGU:GAIN:DAC0" 
# cmd_str__PGU_GAIN_DAC0      = b':PGU:GAIN:DAC0'
#
# ":PGU:GAIN:DAC0? \n" 
# ":PGU:GAIN:DAC0 #H02D002D0 \n" 
#
# // data = {DAC_ch1_fsc, DAC_ch2_fsc}
# // DAC_ch#_fsc = {000000, 10 bit data}
#
#  AD9783 full scale current spec: 
#    0x000 for 8.66mA; 0x200 for 20.0mA; 0x3FF for 31.66mA
#
# full scale DAC : 28.1mA  @ 0x02D0
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_GAIN_DAC0))
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b' #H02D102D0 \n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b' #H02A002A0 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b'?\n')

### scpi command: ":PGU:GAIN:DAC1" 
# cmd_str__PGU_GAIN_DAC1      = b':PGU:GAIN:DAC1'
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_GAIN_DAC1))
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b' #H02D102D0 \n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b' #H02A002A0 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b'?\n')


################################################################################

## wait for an input  ######################################################
input('Enter any key to trigger')


###########################################################################
## PULSE BLOCK  ##############################################################################

### TODO: cmd_str__PGU_FDCS_DAC0      = b':PGU:FDCS:DAC0'
# :PGU:FDCS:DAC0 #N_dddddd_hhhhhhhh_hhhhhhhh_..._hhhhhhhh <NL>
#   dddddd   = decimal number of bytes in data
#   hhhhhhhh = data in hexadeciaml format
#
# :PGU:FDCS:DAC0 #N_000200_00010004_1FFF0000_3FFF0000_5FFF0000_7FFF0005_6FFF0000_5FFF0001_4FFF0001_3FFF0001_2FFF0001_1FFF0001_0FFF0000_00000009_0FFF0001_1FFF0001_2FFF0001_3FFF0009_37FF0000_2FFF0001_27FF0001_1FFF0001_17FF0001_0FFF0001_07FF0000_FFFF000F <NL>
#
#  // 25 steps ... 
#  // byte number = 25 steps * 8 = 200:  
#
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_DAC0))
#scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_DAC0+b' #N_000200_00010004_1FFF0000_3FFF0000_5FFF0000_7FFF0005_6FFF0000_5FFF0001_4FFF0001_3FFF0001_2FFF0001_1FFF0001_0FFF0000_00000009_0FFF0001_1FFF0001_2FFF0001_3FFF0009_37FF0000_2FFF0001_27FF0001_1FFF0001_17FF0001_0FFF0001_07FF0000_FFFF000F \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_DAC0+b' #N_000200_00000004_1FFF0000_3FFF0000_5FFF0000_7FFF0005_6FFF0000_5FFF0001_4FFF0001_3FFF0001_2FFF0001_1FFF0001_0FFF0000_00000009_0FFF0001_1FFF0001_2FFF0001_3FFF0009_37FF0000_2FFF0001_27FF0001_1FFF0001_17FF0001_0FFF0001_07FF0000_0000000F \n')


### TODO: cmd_str__PGU_FDCS_DAC1      = b':PGU:FDCS:DAC1'
# :PGU:FDCS:DAC1 #N_nnnnnn_hhhhhhhh_hhhhhhhh_..._hhhhhhhh <NL>
#   nnnnnn   = decimal number of bytes in data
#   hhhhhhhh = data in hexadeciaml format
#
# :PGU:FDCS:DAC1 #N_000200_00010004_1FFF0000_3FFF0000_5FFF0000_7FFF0005_6FFF0000_5FFF0001_4FFF0001_3FFF0001_2FFF0001_1FFF0001_0FFF0000_00000009_0FFF0001_1FFF0001_2FFF0001_3FFF0009_37FF0000_2FFF0001_27FF0001_1FFF0001_17FF0001_0FFF0001_07FF0000_FFFF000F <NL>
#
#  // 25 steps ... 
#  // byte number = 25 steps * 8 = 200:  
#
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_DAC1))
#scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_DAC1+b' #N_000200_00010004_1FFF0000_3FFF0000_5FFF0000_7FFF0005_6FFF0000_5FFF0001_4FFF0001_3FFF0001_2FFF0001_1FFF0001_0FFF0000_00000009_0FFF0001_1FFF0001_2FFF0001_3FFF0009_37FF0000_2FFF0001_27FF0001_1FFF0001_17FF0001_0FFF0001_07FF0000_FFFF000F \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_DAC1+b' #N_000200_00000004_1FFF0000_3FFF0000_5FFF0000_7FFF0005_6FFF0000_5FFF0001_4FFF0001_3FFF0001_2FFF0001_1FFF0001_0FFF0000_00000009_0FFF0001_1FFF0001_2FFF0001_3FFF0009_37FF0000_2FFF0001_27FF0001_1FFF0001_17FF0001_0FFF0001_07FF0000_0000000F \n')





### TODO: cmd_str__PGU_FDCS_RPT       = b':PGU:FDCS:RPT'
### scpi command: ":PGU:FDCS:RPT"
# :PGU:FDCS:RPT? <NL>			
# :PGU:FDCS:RPT #Hnnnnnnnn <NL>			
#
# // DACn repeat count = {16-bit DAC1 repeat count, 16-bit DAC0 repeat count}
#
# ":PGU:FDCS:RPT? \n"
# ":PGU:FDCS:RPT #H00040005 \n"
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_RPT))
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_RPT+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_RPT+b' #H00040002\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_RPT+b'?\n')



#############################################################################

### scpi command: ":PGU:FDCS:TRIG"
# :PGU:FDCS:TRIG ON|OFF <NL>			
#
# ":PGU:FDCS:TRIG ON\n"
# ":PGU:FDCS:TRIG OFF\n"
#

### trig on : FDCS mode
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_TRIG))
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_TRIG+b' ON\n')

## wait for a while
sleep(1)

### trig off : FDCS mode
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_TRIG))
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_TRIG+b' OFF\n')


################################################################################

### power off 
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_PWR))
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b' OFF\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')



## PGEP disable
print('\n>>> {} : {}'.format('Test',cmd_str__PGEP_EN))
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b' OFF\n')
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b'?\n')


## close socket
scpi_close(ss)


##



