## scpi_client__test__sine_wave__rev_disp.py.py
import socket
#import time
from time import sleep
import sys

# connect(), close() ... style

# https://docs.python.org/3/library/socket.html
# https://docs.python.org/2/library/socket.html

## control parameters 

HOST = '192.168.168.123'  # The server's hostname or IP address
HOST119 = '192.168.168.119'  # The server's hostname or IP address // firmware test ip
HOST122 = '192.168.168.122'  # The server's hostname or IP address
HOST123 = '192.168.168.123'  # The server's hostname or IP address
HOST124 = '192.168.168.124'  # The server's hostname or IP address
HOST2 = '192.168.172.1'  # The server's hostname or IP address
PORT = 5025               # The port used by the server
#TIMEOUT = 5.3 # socket timeout
TIMEOUT = 20 # socket timeout // for big data 
#TIMEOUT = 1000 # socket timeout // for debug 1000s
SO_SNDBUF = 2048
SO_RCVBUF = 32768
INTVAL = 0.1 # sec for waiting before recv()
BUF_SIZE_NORMAL = 2048
#BUF_SIZE_NORMAL = 524288 
BUF_SIZE_LARGE = 16384
TIMEOUT_LARGE = TIMEOUT*10

ss = None # socket

## command strings ##############################################################
cmd_str__IDN = b'*IDN?\n'
cmd_str__RST = b'*RST\n'
#
cmd_str__PGEP_EN = b':PGEP:EN'
#
cmd_str__PGU_PWR            = b':PGU:PWR'
cmd_str__PGU_OUTP           = b':PGU:OUTP'
cmd_str__PGU_DCS_TRIG       = b':PGU:DCS:TRIG'
cmd_str__PGU_DCS_DAC0_PNT   = b':PGU:DCS:DAC0:PNT'
cmd_str__PGU_DCS_DAC1_PNT   = b':PGU:DCS:DAC1:PNT'
cmd_str__PGU_DCS_RPT        = b':PGU:DCS:RPT'
cmd_str__PGU_FDCS_TRIG      = b':PGU:FDCS:TRIG'
cmd_str__PGU_FDCS_DAC0      = b':PGU:FDCS:DAC0'
cmd_str__PGU_FDCS_DAC1      = b':PGU:FDCS:DAC1'
cmd_str__PGU_FDCS_RPT       = b':PGU:FDCS:RPT'
cmd_str__PGU_FREQ           = b':PGU:FREQ'
cmd_str__PGU_OFST_DAC0      = b':PGU:OFST:DAC0'
cmd_str__PGU_OFST_DAC1      = b':PGU:OFST:DAC1'
cmd_str__PGU_GAIN_DAC0      = b':PGU:GAIN:DAC0'
cmd_str__PGU_GAIN_DAC1      = b':PGU:GAIN:DAC1'
#


## functions ####################################################################

def scpi_open (timeout=TIMEOUT):
	try:
		ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		ss.settimeout(timeout)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, SO_SNDBUF)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, SO_RCVBUF) # 8192 16384 32768 65536
	except OSError as msg:
		ss = None
		print('error in socket: ', msg)
		raise
	return ss

def scpi_connect (ss, HOST, PORT):
	try:
		ss.connect((HOST, PORT))
	except OSError as msg:
		ss.close()
		ss = None
		print('error in connect: ', msg)
		raise


def scpi_close (ss):
	try:
		ss.close()
	except:
		if ss == None:
			print('error: ss==None')
		raise

def scpi_comm_resp_ss (ss, cmd_str, buf_size=BUF_SIZE_NORMAL, intval=INTVAL) :
	try:
		print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# recv data until finding the sentinel '\n'
	try:
		data = ss.recv(buf_size) # try 1024 131072 524288
		# try   
		while (1):
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
	except:
		print('error in recv')
		raise
	#
	## check response 
	if (len(data)>20):
		print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		print('Received:', repr(data))
	#
	# NG response check 
	if data[0:2]==b'NG':
		print('Received: NG as response')
		#input('Press Enter key!')
		#sleep(3)
	#
	return data


# scpi command for numeric block response
def scpi_comm_resp_numb_ss (ss, cmd_str, buf_size=BUF_SIZE_LARGE, intval=INTVAL, timeout_large=TIMEOUT_LARGE) :
	try:
		print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# cmd: ":PGEP:PO#HBC 524288\n"
	# rsp: "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
	#
	# recv data until finding the sentinel '\n' 
	# but check the sentinel after the data byte count is met.
	#
	# read timeout
	to = ss.gettimeout()
	#print(to)
	# increase timeout
	ss.settimeout(timeout_large)
	#
	try:
		# find the numeric head : must 10 in data 
		data = ss.recv(buf_size)
		while True:
			if len(data)>=10:
				break
			data = data + ss.recv(buf_size)
		#
		#print('header: ', repr(data[0:10])) # header
		#
		# find byte count 
		byte_count = int(data[3:9])
		#print('byte_count=', repr(byte_count)) 
		#
		# collect all data by byte count
		count_to_recv = byte_count + 10 + 1# add header count #add /n
		while True:
			if len(data)>=count_to_recv:
				break
			data = data + ss.recv(buf_size)
		#
		# check the sentinel 
		while True:
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
		#
	except:
		print('error in recv')
		raise
	#
	if (len(data)>20):
		print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		print('Received:', repr(data))
	#
	# timeout back to prev
	ss.settimeout(to)
	#
	data = data[10:(10+byte_count)]
	print('data:', data[0:20].hex(),  ' (first 20 bytes)')
	#
	return [byte_count, data]
	
	

## generate pulse info for FDCS command

def conv_dec_to_bit_2s_comp_16bit(dec, full_scale=20):
	if dec > full_scale/2-full_scale/2**16 :
		dec = full_scale/2-full_scale/2**16
	if dec < -full_scale/2-full_scale/2**16 :
		dec = -full_scale/2-full_scale/2**16
	bit_2s_comp = int( 0x10000 * ( dec + full_scale/2)    / full_scale ) + 0x8000
	if bit_2s_comp > 0xFFFF :
		bit_2s_comp -= 0x10000
	return bit_2s_comp

#test_codes = [ conv_dec_to_bit_2s_comp_16bit(x) for x in [-10,-5,0,5,10] ]
#print(test_codes)
	
def conv_bit_2s_comp_16bit_to_dec(bit_2s_comp, full_scale=20):
	if bit_2s_comp >= 0x8000:
		bit_2s_comp -= 0x8000
		#dec = full_scale * (bit_2s_comp) / 0x10000 -10
		dec = full_scale * (bit_2s_comp) / 0x10000 - full_scale/2
	else :
		dec = full_scale * (bit_2s_comp) / 0x10000
		if dec == full_scale/2-full_scale/2**16 :
			dec = full_scale/2
	return dec

#test_codes2 = [ conv_bit_2s_comp_16bit_to_dec(x) for x in test_codes ]
#print(test_codes2)

# gen_pulse_info_num_block__inc_step
def gen_pulse_info_num_block__inc_step(code_start, code_step, num_steps, code_duration):
	# num_steps : including last step
	#
	pulse_info_num_block_str = b''
	#
	num_codes = num_steps
	#
	# set number of bytes to send : 8 bytes * (number of codes)
	pulse_info_num_block_str += ' #N_{:06d}'.format(num_codes*8).encode()
	#
	code_list = []
	duration_list = []
	sample_list = []
	#
	sample_value = 0
	code_value  = code_start
	#
	for ii in range(num_codes):
		# merge dac code and duration
		test_value   = (code_value<<16) + code_duration
		# convert into text bytes
		test_str = '_{:08X}'.format(test_value).encode()
		# append it to numberic block
		pulse_info_num_block_str += test_str
		#
		# save step and code in list
		code_list     += [code_value]
		duration_list += [code_duration+1] # 0 for 1 step
		sample_list   += [sample_value]
		#
		# update next code 
		sample_value += code_duration+1
		code_value   += code_step
		if code_value > 0xFFFF:
			code_value -= 0x10000
		#
	#
	# add sentinel
	pulse_info_num_block_str += b' \n'
	#
	sample_code = [code_list, duration_list, sample_list]
	#
	return pulse_info_num_block_str, sample_code

# gen_pulse_info_num_block__from_code_list
def gen_pulse_info_num_block__from_code_list(code_list, code_duration):
	# num_steps : including last step
	#
	pulse_info_num_block_str = b''
	#
	num_codes = len(code_list)
	#
	# set number of bytes to send : 8 bytes * (number of codes)
	pulse_info_num_block_str += ' #N_{:06d}'.format(num_codes*8).encode()
	#
	duration_list = []
	sample_list = []
	#
	sample_value = 0
	#
	for ii in range(num_codes):
		# read DAC code
		code_value = code_list[ii]
		# merge dac code and duration
		test_value   = (code_value<<16) + code_duration
		# convert into text bytes
		test_str = '_{:08X}'.format(test_value).encode()
		# append it to numberic block
		pulse_info_num_block_str += test_str
		#
		# save step and code in list
		duration_list += [code_duration+1] # 0 for 1 step
		sample_list   += [sample_value]
		#
		# update sample_value
		sample_value += code_duration+1
		#
	#
	# add sentinel
	pulse_info_num_block_str += b' \n'
	#
	sample_code = [code_list, duration_list, sample_list]
	#
	return pulse_info_num_block_str, sample_code


## test parameters #########################################################################
# TODO: USER VAR

## need to setup multi-level pulse sequence

# pulse duration calculation:
#
# // (duration count)/(DAC update freq)*(number of codes) =  (pulse duration) 
#    (duration count)  = code_duration + 1
#    (number of codes) = num_steps + 1
#
# // 40000/( 50MHz)*125 =  100 ms // 1 + 0x9C3F = 40000
# // 40000/(200MHz)*125 =   25 ms // 1 + 0x9C3F = 40000
# //    10/(200MHz)*125 = 6.25 us // 1 + 0x0009 = 10
# //     1/(200MHz)*125 =  625 ns // 1 + 0x0000 = 1
#

# DAC update rates 
#
# // DAC 400MHz update =  2.5ns update
# // DAC 200MHz update =    5ns update 
# // DAC 100MHz update =   10ns update
# // DAC  80MHz update = 12.5ns update
# // DAC  50MHz update =   20ns update
# // DAC  20MHz update =   50ns update
#

# code duration = 5ns 
# code duration = 50ns 
# code duration = 200us or 200000ns 
# 
# level_code_top    = int( 0x7FFF * level_volt_top    / 10 )
# level_code_bottom = int( 0x7FFF * level_volt_bottom / 10 )
#

# generate sine waveform 

import numpy as np

# sine wave: 120Hz(=40Hz*3), 1kHz, 10kHz, 100kHz, 1MHz, 2MHz, 5MHz, 10MHz, 50MHz
#   note 120Hz ~ 8.33333333ms
#      8.33333333ms/(10ns*2^16)=12.7156575
#      8.33333333ms/(50ns*2^16)=2.54313151
#      not coherent!!
#      8.33333333ms/(50ns*200)=833.333
#      8.33333333ms/(50ns*160)=1041.66667
#      (1 / (120 Hz)) / ((50 ns) * 160) = 1 041.66667
#      (1 / (120 Hz)) / ((50 ns) * 160)*2 = 2083.33333333
#      (1 / (120 Hz)) / ((50 ns) * 160)*3 = 3125
#       3 cycle ... 40Hz ... can make 120Hz with 20MHz and 160 code duration.
#
#
#test_wave_freq__Hz = 100e6 # 100MHz # 2.5ns    
#test_wave_freq__Hz = 50e6 # 50MHz  # 2.5ns
#test_wave_freq__Hz = 10e6 # 10MHz # 2.5ns
#test_wave_freq__Hz = 5e6 # 5MHz # 2.5ns
#test_wave_freq__Hz = 2e6 # 2MHz # 2.5ns
test_wave_freq__Hz = 1e6 # 1MHz # 10ns ##
#test_wave_freq__Hz = 200e3 # 200kHz # 20ns
#test_wave_freq__Hz = 100e3 # 100kHz # 50ns
#test_wave_freq__Hz = 10e3 # 10kHz # 500ns 
#test_wave_freq__Hz = 1e3 # 1kHz  # 5000ns ##
#test_wave_freq__Hz = 120 # 120Hz # 50us ##
#
#test_sample_time__ns = 50e3
#test_sample_time__ns = 5000
#test_sample_time__ns = 1000
#test_sample_time__ns = 500
#test_sample_time__ns = 100
#test_sample_time__ns = 50
#test_sample_time__ns = 20
#test_sample_time__ns = 10
test_sample_time__ns = 5
#test_sample_time__ns = 2.5
#
test_num_cycles = 3
#test_num_samples = 400 # consider FIFO limit
test_num_samples = int(1/test_wave_freq__Hz/(test_sample_time__ns*1e-9)*test_num_cycles + 0.5)
#
if test_num_samples>1024:
	print('>>> please check test_num_samples: {}'.format(test_num_samples))
	raise
#
test_time__ns__list = [ t*test_sample_time__ns for t in range(test_num_samples) ]
print(test_time__ns__list)

pgu_amp = 10
pgu_phi = np.pi/2
#pgu_phi = 0
test_sine__unit__list = [ pgu_amp*np.sin( 2*np.pi*test_wave_freq__Hz*(t*1e-9) + pgu_phi ) for t in test_time__ns__list ]

print(test_sine__unit__list)
#
print(len(test_time__ns__list))
#
print('{} = {}'.format('test_num_samples',test_num_samples))
print('{} = {}'.format('test_wave_freq__Hz',test_wave_freq__Hz))
print('{} = {}'.format('test_sample_time__ns',test_sample_time__ns))
print('{} = {}'.format('test_num_cycles',test_num_cycles))

# convert sine to points
time_ns_list    = test_time__ns__list
level_volt_list = test_sine__unit__list


# scaling
#time_ns_list    = [ x*1 for x in time_ns_list]         # no scale 
#time_ns_list    = [ x*1 for x in time_ns_list]         # 20ns
#time_ns_list    = [ x*30.0/20.0 for x in time_ns_list] # 30ns
#time_ns_list    = [ x*50.0/20.0 for x in time_ns_list] # 50ns
#time_ns_list    = [ x*100.0/20.0 for x in time_ns_list] # 100ns#
#time_ns_list    = [ x*1000.0/20.0 for x in time_ns_list] # 1us#
#time_ns_list    = [ x*10000.0/20.0 for x in time_ns_list] # 10_000ns
#level_volt_list = [ x*0.8 for x in level_volt_list]

# merge list
merged_time_level_list = [ [t,y] for t,y in zip(time_ns_list,level_volt_list) ]

print('{} = {}'.format('merged_time_level_list', merged_time_level_list))#

# set DAC update period
#time_ns__dac_update    =   2.5 # 400MHz
time_ns__dac_update    =   5
#time_ns__dac_update    =   10 # 100MHz
#time_ns__dac_update    =   20 # 50MHz
#time_ns__dac_update    =   50 # 20MHz

# set DAC code duration // < (time_ns__dac_update)* 2^16
#time_ns__code_duration =   2.5 
#time_ns__code_duration =   5
#time_ns__code_duration =   10
#time_ns__code_duration =   20  
#time_ns__code_duration =   25
#time_ns__code_duration =   50  
#
time_ns__code_duration =   test_sample_time__ns # for sine wave form

print('{} = {}'.format('time_ns__dac_update', time_ns__dac_update))#
print('{} = {}'.format('time_ns__code_duration', time_ns__code_duration))#

# set DAC update freq
pgu_freq_in_100kHz = int( 1/(time_ns__dac_update*1e-9)/100000 )
#
pgu_freq_in_100kHz_str = ' {:04d} \n'.format(pgu_freq_in_100kHz).encode()
#
print('pgu_freq_in_100kHz_str:', repr(pgu_freq_in_100kHz_str))

# set pulse repeat number
pulse_repeat_number_dac0 = 2
pulse_repeat_number_dac1 = 3
#
pgu_repeat_num_str = ' #H{:04X}{:04X} \n'.format(pulse_repeat_number_dac1,pulse_repeat_number_dac0).encode()
#
print('pgu_repeat_num_str:', repr(pgu_repeat_num_str))

# set DAC full scale current gain 
#   
# // data = {DAC_ch1_fsc, DAC_ch2_fsc}
# // DAC_ch#_fsc = {000000, 10 bit data}
#
#  AD9783 full scale current spec: 
#    0x000 for 8.66mA; 0x200 for 20.0mA; 0x3FF for 31.66mA
#
#   // #    0x000 for 8.66mA; 0x200 for 20.0mA; 0x3FF for 31.66mA
#   // I_FS = (86.6 + (0.220 × DAC_gain)) × 1000/R_FS // 	R_FS = 10k
#   // DAC_gain = (I_FS/1000*R_FS - 86.6)/0.220
#
#DAC_full_scale_current__mA = 30
DAC_full_scale_current__mA = 20
#DAC_full_scale_current__mA = 14
#DAC_full_scale_current__mA = 10
#
#I_FS__mA  = 20 
#I_FS__mA  = 14
I_FS__mA  = DAC_full_scale_current__mA
#
R_FS__ohm = 10e3
DAC_gain  = int( (I_FS__mA/1000*R_FS__ohm - 86.6)/0.220 + 0.5)
#
pgu_fsc_gain_str = ' #H{:04X}{:04X} \n'.format(DAC_gain,DAC_gain).encode()
#
print('pgu_fsc_gain_str:', repr(pgu_fsc_gain_str))
#
if DAC_gain>0x3FF or DAC_gain<0 :
	print('>>> please check the full scale current: {}'.format(DAC_full_scale_current__mA))
	raise

# set DAC offset current 
#   pgu_offset_con_str
#
# // data = {DAC_ch1_aux, DAC_ch2_aux}
# // DAC_ch#_aux = {PN_Pol_sel, Source_Sink_sel, 0000, 10 bit data}
# //                PN_Pol_sel      = 0/1 for P/N
# //                Source_Sink_sel = 0/1 for Source/Sink
#
#  AD9783 offset current spec: 
#    0x000 for 0.0mA; 0x200 for 1.0mA; 0x3FF for 2.0mA
#
DAC_offset_current__mA = 0 # 0 # 0.625 mA
N_pol_sel = 0 # 1
Sink_sel  = 0 # 1
#
DAC_offset_current__code = int(DAC_offset_current__mA * 0x200 + 0.5)
#
if DAC_offset_current__code > 0x3FF :
	print('>>> please check the offset current: {}'.format(DAC_offset_current__mA))
	raise
#
DAC_offset = (N_pol_sel<<15) + (Sink_sel<<14) + DAC_offset_current__code;
#
pgu_offset_con_str = ' #H{:04X}{:04X} \n'.format(DAC_offset,DAC_offset).encode()
#
print('pgu_offset_con_str:', repr(pgu_offset_con_str))
#

## convert dec into hexadeciaml codes

# calculate levels in code
level_code_list = [ conv_dec_to_bit_2s_comp_16bit(m[1]) for m in merged_time_level_list ]
print( '{} = [{}]'.format('level_code_list', ', '.join('0x{:04X}'.format(x) for x in level_code_list)) )#
#
# calculate code duration 
time_step_code   = int( time_ns__code_duration / time_ns__dac_update ) - 1
print('{} = 0x{:04X}'.format('time_step_code  ',time_step_code  ))#


# check codes
if any(x > 0xFFFF for x in level_code_list) :
	print('>>> please check the 16-bit code overflow: {}'.format('level_code_list'))
	raise
if time_step_code > 0xFFFF :
	print('>>> please check the 16-bit code overflow: {}'.format('time_step_code'))
	raise


# generate number block from DAC codes
# TODO: set MAX_num_dac_codes_in_number_block
MAX_num_dac_codes_in_number_block = 256 # OK
#MAX_num_dac_codes_in_number_block = 400 # OK
#MAX_num_dac_codes_in_number_block = 500 # NG
#MAX_num_dac_codes_in_number_block = 512 # NG
#MAX_num_dac_codes_in_number_block = 1024 # NG

print(len(level_code_list))

new_level_code_list = [level_code_list[x:x+MAX_num_dac_codes_in_number_block] \
	for x in range(0, len(level_code_list), MAX_num_dac_codes_in_number_block)]

print(len(new_level_code_list))

level_code_list = new_level_code_list

num_block_str__sample_code__list = \
[gen_pulse_info_num_block__from_code_list(x,time_step_code) for x in level_code_list]

print('{} = {}'.format('num_block_str__sample_code__list',num_block_str__sample_code__list))

print(len(num_block_str__sample_code__list))




## test routines ###########################################################################

## ## test 
## ret = gen_pulse_info_num_block(code_start=0x0000, code_stop=0x7FFF, num_steps=10, code_duration=0x001F)
## print('Send:', repr(ret))
## ret = gen_pulse_info_num_block(code_start=0x7FFF, code_stop=0x0000, num_steps=10, code_duration=0x001F)
## print('Send:', repr(ret))
## #

#print('num_block_str__delay_start:', repr(num_block_str__delay_start))
#print('num_block_str__rise_slope :', repr(num_block_str__rise_slope ))
#print('num_block_str__flat_top   :', repr(num_block_str__flat_top   ))
#print('num_block_str__fall_slope :', repr(num_block_str__fall_slope ))
#print('num_block_str__flat_bottom:', repr(num_block_str__flat_bottom))
#
#
#print('sample_code__delay_start:', repr(sample_code__delay_start))
#print('sample_code__rise_slope :', repr(sample_code__rise_slope ))
#print('sample_code__flat_top   :', repr(sample_code__flat_top   ))
#print('sample_code__fall_slope :', repr(sample_code__fall_slope ))
#print('sample_code__flat_bottom:', repr(sample_code__flat_bottom))


## display waveform command #############################################################
# TODO: plot

print('\n>> {}'.format('Display waveform'))

import matplotlib.pyplot as plt
#import matplotlib.gridspec as gridspec

plt.ion() # matplotlib interactive mode 
#
FIG_NUM = None # for new figure windows
#FIG_NUM = 1 # for only one figure window
plt.figure(FIG_NUM,figsize=(12,9))
#plt.figure(FIG_NUM,figsize=(8,8))


## command waveform
# data located in  merged_time_level_list
#
t_list = [ m[0] for m in merged_time_level_list ]
y_list = [ m[1] for m in merged_time_level_list ]
#
#t_list = [None]*6
#y_list = [None]*6
#
#t_list[0] = 0
#t_list[1] = t_list[0] + time_ns__delay_start
#t_list[2] = t_list[1] + time_ns__rise_slope
#t_list[3] = t_list[2] + time_ns__flat_top
#t_list[4] = t_list[3] + time_ns__fall_slope
#t_list[5] = t_list[4] + time_ns__flat_bottom
##
#y_list[0] = level_volt_bottom
#y_list[1] = level_volt_bottom
#y_list[2] = level_volt_top
#y_list[3] = level_volt_top
#y_list[4] = level_volt_bottom
#y_list[5] = level_volt_bottom
#
print('{} = {}'.format('t_list ',t_list ))#
print('{} = {}'.format('y_list ',y_list ))#
# plot
plt.subplot(221) ### 
plt.plot(t_list,y_list, 'ro-', markersize=10)
plt.title('command waveform (red)')
plt.ylabel('Voltage')
plt.xlabel('Time(ns)')
plt.grid(True)
#plt.autoscale(enable=True, axis='x', tight=True)


## DAC points
#dac_list = []
##merge
#dac_list += sample_code__delay_start[0]
#dac_list += sample_code__rise_slope[0] 
#dac_list += sample_code__flat_top[0]   
#dac_list += sample_code__fall_slope[0] 
#dac_list += sample_code__flat_bottom[0]
#
# DAC codes located in num_block_str__sample_code__list 
dac_list = [ m[1][0] for m in num_block_str__sample_code__list ]
# remove nested list 
dac_list = [n for m in dac_list for n in m]
print('{} = {}'.format('dac_list ',dac_list ))#
#
# convert into voltage
#dac_volt_list = [ conv_bit_2s_comp_16bit_to_dec(x) for x in dac_list ]
# full scale current correction
dac_volt_list = [ conv_bit_2s_comp_16bit_to_dec(x, 20/20*DAC_full_scale_current__mA) for x in dac_list ]
print('{} = {}'.format('dac_volt_list ',dac_volt_list ))#
#
# update t_list for dac_volt_list
t_dac_list = [ x * time_ns__code_duration for x in range(len(dac_volt_list)) ]
print('{} = {}'.format('t_dac_list ',t_dac_list ))#
#
# check data length fitted in FIFO length
len_FIFO = 1024
len_DAC_data = len(dac_list)
print('{} = {}'.format('len_DAC_data ',len_DAC_data ))#
if len_DAC_data > len_FIFO:
	print('> Warning: DAC data is not fitted with FIFO length! ')
	raise
#
# plot
plt.subplot(222) ### 
plt.plot(t_list,y_list, 'ro-', markersize=10)
plt.plot(t_dac_list,dac_volt_list, 'bs', markersize=5)
plt.title('command waveform (red) and DAC points generated (blue)')
plt.ylabel('Voltage')
plt.xlabel('Time(ns)')
plt.grid(True)
#plt.autoscale(enable=True, axis='x', tight=True)


## DAC sample-and-hold output (SnH)
# input : dac_volt_list, t_dac_list
# output: dac_volt_snh_list, t_dac_snh_list
def conv_dac_points_2_sample_n_hold_outputs(dac_volt_list, t_dac_list):
	dac_volt_snh_list = [] 
	t_dac_snh_list   = []
	# insert hold data
	dac_volt_snh_list = [ x for x in dac_volt_list for _ in range(2)]
	t_dac_snh_list   = [t_dac_list[0]] + [ x for x in t_dac_list[1:] for _ in range(2)] + [t_dac_list[-1]*2-t_dac_list[-2]]
	# remove duplicate
	# ...
	return dac_volt_snh_list, t_dac_snh_list
#
dac_volt_snh_list, t_dac_snh_list = conv_dac_points_2_sample_n_hold_outputs(dac_volt_list, t_dac_list)
print('{} = {}'.format('dac_volt_snh_list ',dac_volt_snh_list ))#
print('{} = {}'.format('t_dac_snh_list ',t_dac_snh_list ))#
#
# plot
plt.subplot(223) ### 
plt.plot(t_list,y_list, 'ro-', markersize=10)
plt.plot(t_dac_list,dac_volt_list, 'bs', markersize=5)
plt.plot(t_dac_snh_list,dac_volt_snh_list, 'g-', markersize=2)
plt.title('DAC points generated (blue) and DAC sample_n_hold output (green)')
plt.ylabel('Voltage')
plt.xlabel('Time(ns)')
plt.grid(True)
#
plt.subplot(224) ### 
plt.plot(t_list,y_list, 'ro-', markersize=10)
plt.plot(t_dac_snh_list,dac_volt_snh_list, 'g-', markersize=2)
plt.title('command waveform (red) and DAC sample_n_hold output (green)')
plt.ylabel('Voltage')
plt.xlabel('Time(ns)')
plt.grid(True)


# repeat waveform 
#   pulse_repeat_number_dac0
#   input:
#   t_list, y_list
#   t_dac_snh_list, dac_volt_snh_list
#   output:
#   t_rep_list, y_rep_list
#   t_dac_snh_rep_list, dac_volt_snh_rep_list
def add_repeated_pattern_in_time_domain(t_list, y_list, num_repeat=1):
	#t_rep_list = t_list
	#y_rep_list = y_list
	t_rep_list = [ x for x in t_list ]
	#
	if num_repeat>1 :
		for _ in range(num_repeat-1):
			tmp_list = [ x+t_rep_list[-1] for x in t_list ]
			t_rep_list += tmp_list
	#
	y_rep_list = [x for _ in range(num_repeat) for x in y_list]
	#
	#
	return t_rep_list, y_rep_list
#
def add_repeated_pattern_w_space_in_time_domain(t_list, y_list, num_repeat=1):
	#t_rep_list = t_list
	#y_rep_list = y_list
	t_rep_list = [ x for x in t_list ]
	#
	if num_repeat>1 :
		for _ in range(num_repeat-1):
			tmp_list = [ x+(-t_rep_list[-2]+2*t_rep_list[-1]) for x in t_list ]
			t_rep_list += tmp_list
	#
	y_rep_list = [x for _ in range(num_repeat) for x in y_list]
	#
	#
	return t_rep_list, y_rep_list
#
t_rep_list, y_rep_list                    = add_repeated_pattern_w_space_in_time_domain (t_list, y_list, pulse_repeat_number_dac0)
#
#t_dac_snh_rep_list, dac_volt_snh_rep_list = add_repeated_pattern_w_space_in_time_domain (t_dac_snh_list, dac_volt_snh_list, pulse_repeat_number_dac0)
dac_volt_snh_rep_list, t_dac_snh_rep_list = conv_dac_points_2_sample_n_hold_outputs (y_rep_list, t_rep_list)
#
print('{} = {}'.format('t_list    ',t_list     ))#
print('{} = {}'.format('y_list    ',y_list     ))#
print('{} = {}'.format('t_rep_list',t_rep_list ))#
print('{} = {}'.format('y_rep_list',y_rep_list ))#
#
print('{} = {}'.format('len(t_list    )',len(t_list    ) ))#
print('{} = {}'.format('len(y_list    )',len(y_list    ) ))#
print('{} = {}'.format('len(t_rep_list)',len(t_rep_list) ))#
print('{} = {}'.format('len(y_rep_list)',len(y_rep_list) ))#
#

#time_ns__dac_update    =   2.5
#time_ns__code_duration =   2.5
title_str = 'command waveform (red) and DAC sample_n_hold output (green)'
title_str += '\n DAC {}ns, Step duration {}ns'.format(time_ns__dac_update,time_ns__code_duration)
#
plt.figure(FIG_NUM,figsize=(8,4))
#
plt.plot(t_rep_list, y_rep_list, 'ro-', markersize=10)
plt.plot(t_dac_snh_rep_list, dac_volt_snh_rep_list, 'g-', markersize=2)
plt.title(title_str)
plt.ylabel('Voltage')
plt.xlabel('Time(ns)')
plt.grid(True)


input('test stop')


## open socket and connect scpi server ####################################################

## try:
## 	ss = scpi_open()
## 	scpi_connect(ss, HOST, PORT)
## except socket.timeout:
## 	ss = scpi_open()
## 	scpi_connect(ss, HOST2, PORT)
## except:
## 	raise

def my_open(host, port):
	#
	ss = scpi_open()
	try:
		print('>> try to connect : {}:{}'.format(host, port))
		scpi_connect(ss, host, port)
	except socket.timeout:
		ss = None
	except:
		raise
	return ss

# TODO: IP setup

# firmware test
ss = my_open(HOST119,PORT) # firmware test
if ss == None :
	raise


## # browse ip
## ss = my_open(HOST122,PORT)
## if ss == None:	ss = my_open(HOST123,PORT)
## if ss == None:	ss = my_open(HOST124,PORT)
## if ss == None:	raise


###########################################################################

### scpi : *IDN?
print('\n>>> {} : {}'.format('Test',cmd_str__IDN))
rsp = scpi_comm_resp_ss(ss, cmd_str__IDN)
print('hex code rcvd: ' + rsp.hex())
print('string rcvd: ' + repr(rsp))


### :PGEP:EN
# :PGEP:EN ON|OFF <NL>			
# :PGEP:EN? <NL>			
#
# ":PGEP:EN ON\n"
# ":PGEP:EN OFF\n"
# ":PGEP:EN?"
print('\n>>> {} : {}'.format('Test',cmd_str__PGEP_EN))
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b' ON\n')
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b'?\n')


### scpi command: ":PGU:PWR"
# :PGU:PWR ON|OFF <NL>			
# :PGU:PWR? <NL>			
#
# ":PGU:PWR ON\n"
# ":PGU:PWR OFF\n"
# ":PGU:PWR?"
#

### power on 
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_PWR))
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b' ON\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')

### scpi command: ":PGU:OUTP" #### // TODO: cmd_str__PGU_OUTP
# :PGU:OUTP ON|OFF <NL>			
# :PGU:OUTP? <NL>			
#
# ":PGU:OUTP ON\n"
# ":PGU:OUTP OFF\n"
# ":PGU:OUTP?"
#

### output on or off
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_OUTP))
scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b' ON\n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b' OFF\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b'?\n')


###########################################################################

### scpi command: ":PGU:FREQ" // TODO: cmd_str__PGU_FREQ
# set DAC update freq based on 100kHz
#
# :PGU:FREQ nnnn <NL>			
#
# ":PGU:FREQ 4000 \n"  // for 400.0 MHz
# ":PGU:FREQ 2000 \n"  // for 200.0 MHz
# ":PGU:FREQ 1000 \n"  // for 100.0 MHz
# ":PGU:FREQ 0800 \n"  // for 080.0 MHz
# ":PGU:FREQ 0500 \n"  // for 050.0 MHz
# ":PGU:FREQ 0200 \n"  // for 020.0 MHz
#
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FREQ))
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 4000 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 2000 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 1000 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0800 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0500 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0200 \n') #OK
#
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 4000 \n') # 400MHz
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 2000 \n') # 200MHz
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 1000 \n') # 100MHz
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0500 \n') # 50MHz
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0200 \n') # 20MHz
#
scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+pgu_freq_in_100kHz_str) #

### check pll status ###
#define EP_ADRS__TEST_IO_MON__PGU      0x23
print('\n>>> {} : {}'.format('Read', 'PLL status'))
scpi_comm_resp_ss(ss, b':PGEP:WO#H23? \n') #

### scpi command: ":PGU:OFST:DAC0" 
# cmd_str__PGU_OFST_DAC0      = b':PGU:OFST:DAC0'
#
# :PGU:OFST:DAC0? <NL>			
# :PGU:OFST:DAC0 #Hnnnnnnnn <NL>			
#
# // data = {DAC_ch1_aux, DAC_ch2_aux}
# // DAC_ch#_aux = {PN_Pol_sel, Source_Sink_sel, 0000, 10 bit data}
# //                PN_Pol_sel      = 0/1 for P/N
# //                Source_Sink_sel = 0/1 for Source/Sink
#
#  AD9783 offset current spec: 
#    0x000 for 0.0mA; 0x200 for 1.0mA; 0x3FF for 2.0mA
#
# offset DAC : 0x140 0.625mA, AUX2N active[7] (1) , sink current[6] (1) 
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_OFST_DAC0))
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+b'?\n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+b' #HC141C140 \n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+b' #H00000000 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+pgu_offset_con_str)
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+b'?\n')

### scpi command: ":PGU:OFST:DAC1" 
# cmd_str__PGU_OFST_DAC1      = b':PGU:OFST:DAC1'
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_OFST_DAC1))
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+b'?\n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+b' #HC141C140 \n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+b' #H00000000 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+pgu_offset_con_str)
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+b'?\n')

### scpi command: ":PGU:GAIN:DAC0" 
# cmd_str__PGU_GAIN_DAC0      = b':PGU:GAIN:DAC0'
#
# ":PGU:GAIN:DAC0? \n" 
# ":PGU:GAIN:DAC0 #H02D002D0 \n" 
#
# // data = {DAC_ch1_fsc, DAC_ch2_fsc}
# // DAC_ch#_fsc = {000000, 10 bit data}
#
#  AD9783 full scale current spec: 
#    0x000 for 8.66mA; 0x200 for 20.0mA; 0x3FF for 31.66mA
#
# full scale DAC : 28.1mA  @ 0x02D0
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_GAIN_DAC0))
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b'?\n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b' #H02D102D0 \n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b' #H02A002A0 \n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b' #H02000200 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+pgu_fsc_gain_str)
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b'?\n')

### scpi command: ":PGU:GAIN:DAC1" 
# cmd_str__PGU_GAIN_DAC1      = b':PGU:GAIN:DAC1'
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_GAIN_DAC1))
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b'?\n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b' #H02D102D0 \n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b' #H02A002A0 \n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b' #H02000200 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+pgu_fsc_gain_str)
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b'?\n')


################################################################################

## wait for an input  ######################################################
input('Enter any key to trigger')


###########################################################################
## PULSE BLOCK  ##############################################################################



### TODO: cmd_str__PGU_FDCS_DAC0      = b':PGU:FDCS:DAC0'
# :PGU:FDCS:DAC0 #N_dddddd_hhhhhhhh_hhhhhhhh_..._hhhhhhhh <NL>
#   dddddd   = decimal number of bytes in data
#   hhhhhhhh = data in hexadeciaml format
#
# :PGU:FDCS:DAC0 #N_000200_00010004_1FFF0000_3FFF0000_5FFF0000_7FFF0005_6FFF0000_5FFF0001_4FFF0001_3FFF0001_2FFF0001_1FFF0001_0FFF0000_00000009_0FFF0001_1FFF0001_2FFF0001_3FFF0009_37FF0000_2FFF0001_27FF0001_1FFF0001_17FF0001_0FFF0001_07FF0000_FFFF000F <NL>
#
#  // 25 steps ... 
#  // byte number = 25 steps * 8 = 200:  
#
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_DAC0))
#scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_DAC0+b' #N_000200_00000004_1FFF0000_3FFF0000_5FFF0000_7FFF0005_6FFF0000_5FFF0001_4FFF0001_3FFF0001_2FFF0001_1FFF0001_0FFF0000_00000009_0FFF0001_1FFF0001_2FFF0001_3FFF0009_37FF0000_2FFF0001_27FF0001_1FFF0001_17FF0001_0FFF0001_07FF0000_0000000F \n')
#
# send number blocks
for mm in num_block_str__sample_code__list:
	scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_DAC0 + mm[0])


### TODO: cmd_str__PGU_FDCS_DAC1      = b':PGU:FDCS:DAC1'
# :PGU:FDCS:DAC1 #N_nnnnnn_hhhhhhhh_hhhhhhhh_..._hhhhhhhh <NL>
#   nnnnnn   = decimal number of bytes in data
#   hhhhhhhh = data in hexadeciaml format
#
# :PGU:FDCS:DAC1 #N_000200_00010004_1FFF0000_3FFF0000_5FFF0000_7FFF0005_6FFF0000_5FFF0001_4FFF0001_3FFF0001_2FFF0001_1FFF0001_0FFF0000_00000009_0FFF0001_1FFF0001_2FFF0001_3FFF0009_37FF0000_2FFF0001_27FF0001_1FFF0001_17FF0001_0FFF0001_07FF0000_FFFF000F <NL>
#
#  // 25 steps ... 
#  // byte number = 25 steps * 8 = 200:  
#
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_DAC1))
#scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_DAC1+b' #N_000200_00000004_1FFF0000_3FFF0000_5FFF0000_7FFF0005_6FFF0000_5FFF0001_4FFF0001_3FFF0001_2FFF0001_1FFF0001_0FFF0000_00000009_0FFF0001_1FFF0001_2FFF0001_3FFF0009_37FF0000_2FFF0001_27FF0001_1FFF0001_17FF0001_0FFF0001_07FF0000_0000000F \n')
#
# send number blocks
for mm in num_block_str__sample_code__list:
	scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_DAC1 + mm[0])


### TODO: cmd_str__PGU_FDCS_RPT       = b':PGU:FDCS:RPT'
### scpi command: ":PGU:FDCS:RPT"
# :PGU:FDCS:RPT? <NL>			
# :PGU:FDCS:RPT #Hnnnnnnnn <NL>			
#
# // DACn repeat count = {16-bit DAC1 repeat count, 16-bit DAC0 repeat count}
#
# ":PGU:FDCS:RPT? \n"
# ":PGU:FDCS:RPT #H00040005 \n"
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_RPT))
#scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_RPT+b'?\n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_RPT+b' #H00040002\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_RPT+pgu_repeat_num_str)
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_RPT+b'?\n')



#############################################################################

### scpi command: ":PGU:FDCS:TRIG"
# :PGU:FDCS:TRIG ON|OFF <NL>			
#
# ":PGU:FDCS:TRIG ON\n"
# ":PGU:FDCS:TRIG OFF\n"
#

### trig on : FDCS mode
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_TRIG))
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_TRIG+b' ON\n')

## wait for a while
sleep(3.5)
sleep(3.5)

### trig off : FDCS mode
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_TRIG))
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_TRIG+b' OFF\n')


################################################################################

## check power
input('Enter any key to power off')

## wait for a while
sleep(1.0)

### power off 
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_PWR))
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b' OFF\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')



## PGEP disable
print('\n>>> {} : {}'.format('Test',cmd_str__PGEP_EN))
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b' OFF\n')
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b'?\n')


## close socket
scpi_close(ss)


##




