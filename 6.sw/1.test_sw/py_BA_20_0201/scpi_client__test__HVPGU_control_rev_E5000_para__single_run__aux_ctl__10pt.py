## scpi_client__test__HVPGU_control_rev_E5000_para__single_run__aux_ctl__10pt.py
#
# support 10 time-voltage pairs

import socket
#import time
from time import sleep
import sys

## socket module
# connect(), close() ... style
# https://docs.python.org/3/library/socket.html
# https://docs.python.org/2/library/socket.html

## tkinter GUI module 
# https://www.tutorialspoint.com/python3/python_gui_programming.htm
# https://www.tutorialspoint.com/python3/tk_button.htm
# https://www.tutorialspoint.com/python3/tk_checkbutton.htm
# https://www.python-course.eu/tkinter_checkboxes.php
# https://www.tutorialspoint.com/python3/tk_entry.htm
# https://www.python-course.eu/tkinter_entry_widgets.php ... entry text number...

## TODO: loading shell parameters 
print(sys.argv[0])
argc=len(sys.argv)
print(argc)

## 3-level parameters: (previously)
# peak         /3------4\
# base   1---2/          \         7/-----8
#                         \        /
# peak1                   5\-----6/
#          trans      trans1    trans2

## 10-point parameters:
# peak         /3------4\    
# base   1---2/          \             9/-----10
# peak2                  5\--6\        /
# peak1                       7\-----8/
#          trans      trans1    trans2 trans3

if argc>1:
	if argc < 27:
		raise # exit code 
	# some arguments : 8EA
	# example command in shell: 
	#    python scpi_client__test__HVPGU_control_rev_E5000_para__single_run__aux_ctl.py  192.168.168.119  0x0000  1  1e6  3  10  0 0.0  1000 0.0 2000 1.0 3000 1.0 4000 2.0 5000 2.0 6000 0.5 7000 0.5 8000 0.0 9000 0.0 
	# 
	__gui_host_ip            =      (sys.argv[ 1])  # ex:  192.168.168.119  
	__gui_aux_io_control     = int  (sys.argv[ 2])  # ex:  0x0000           
	__gui_output_enable      = int  (sys.argv[ 3])  # ex:  1  for output enabled
	__gui_load_impedance_ohm = float(sys.argv[ 4])  # ex:   1e6 # 1e6 or 50.0
	__gui_cycle_count        = int  (sys.argv[ 5])  # ex:  3
	__gui_num_points         = int  (sys.argv[ 6])  # ex:  10 // the number of points to make lines 
	__gui_pt01_time_ns       = int  (sys.argv[ 7])  # ex:  0
	__gui_pt01_volt_V        = float(sys.argv[ 8])  # ex:  0.0   
	__gui_pt02_time_ns       = int  (sys.argv[ 9])  # ex:  1000
	__gui_pt02_volt_V        = float(sys.argv[10])  # ex:  0.0   
	__gui_pt03_time_ns       = int  (sys.argv[11])  # ex:  2000
	__gui_pt03_volt_V        = float(sys.argv[12])  # ex:  1.0   
	__gui_pt04_time_ns       = int  (sys.argv[13])  # ex:  3000
	__gui_pt04_volt_V        = float(sys.argv[14])  # ex:  1.0   
	__gui_pt05_time_ns       = int  (sys.argv[15])  # ex:  4000
	__gui_pt05_volt_V        = float(sys.argv[16])  # ex:  2.0   
	__gui_pt06_time_ns       = int  (sys.argv[17])  # ex:  5000
	__gui_pt06_volt_V        = float(sys.argv[18])  # ex:  2.0   
	__gui_pt07_time_ns       = int  (sys.argv[19])  # ex:  6000
	__gui_pt07_volt_V        = float(sys.argv[20])  # ex:  0.5   
	__gui_pt08_time_ns       = int  (sys.argv[21])  # ex:  7000
	__gui_pt08_volt_V        = float(sys.argv[22])  # ex:  0.5   
	__gui_pt09_time_ns       = int  (sys.argv[23])  # ex:  8000
	__gui_pt09_volt_V        = float(sys.argv[24])  # ex:  0.0   
	__gui_pt10_time_ns       = int  (sys.argv[25])  # ex:  9000
	__gui_pt10_volt_V        = float(sys.argv[26])  # ex:  0.0   
	#
	## aux_io_control bit locations:
	# var_ch2_gain__ = 0x8000
	# var_ch1_gain__ = 0x4000
	# var_ch2_be____ = 0x2000
	# var_ch2_fe____ = 0x1000
	# var_ch1_be____ = 0x0800
	# var_ch1_fe____ = 0x0400
	# var_ch2_sleepn = 0x0200
	# var_ch1_sleepn = 0x0100
	# * control examples
	# 10V mode ex: __gui_aux_io_control = 0x0000
	# 40V mode ex: __gui_aux_io_control = 0x3F00
	#
else:
	# no argument ... load default values
	__gui_host_ip            = '192.168.168.119'
	__gui_aux_io_control     = 0x3F00 # 0x0000 or 0x3F00
	__gui_output_enable      =      0 # '0' for output disabled (test). 
	__gui_load_impedance_ohm = 1e6 # 1e6 or 50.0
	__gui_cycle_count        = 3
	__gui_num_points         = 10    #// the number of points to make lines 
	__gui_pt01_time_ns       = 0
	__gui_pt01_volt_V        = 0.0              
	__gui_pt02_time_ns       = 1000
	__gui_pt02_volt_V        = 0.0              
	__gui_pt03_time_ns       = 2000
	__gui_pt03_volt_V        = 1.0              
	__gui_pt04_time_ns       = 3000
	__gui_pt04_volt_V        = 1.0              
	__gui_pt05_time_ns       = 4000
	__gui_pt05_volt_V        = 2.0              
	__gui_pt06_time_ns       = 5000
	__gui_pt06_volt_V        = 2.0              
	__gui_pt07_time_ns       = 6000
	__gui_pt07_volt_V        = 0.5              
	__gui_pt08_time_ns       = 7000
	__gui_pt08_volt_V        = 0.5              
	__gui_pt09_time_ns       = 8000
	__gui_pt09_volt_V        = 0.0              
	__gui_pt10_time_ns       = 9000
	__gui_pt10_volt_V        = 0.0              	

## control parameters 

HOST = '192.168.168.123'  # The server's hostname or IP address
HOST119 = '192.168.168.119'  # The server's hostname or IP address // firmware test ip
HOST122 = '192.168.168.122'  # The server's hostname or IP address
HOST123 = '192.168.168.123'  # The server's hostname or IP address
HOST124 = '192.168.168.124'  # The server's hostname or IP address
HOST2 = '192.168.172.1'  # The server's hostname or IP address
PORT = 5025               # The port used by the server
#PORT = 5050               # test port
TIMEOUT = 5.3 # socket timeout
#TIMEOUT = 1000 # socket timeout // for debug 1000s
SO_SNDBUF = 2048
SO_RCVBUF = 32768
INTVAL = 0.1 # sec for waiting before recv()
BUF_SIZE_NORMAL = 2048
BUF_SIZE_LARGE = 16384
TIMEOUT_LARGE = TIMEOUT*10

ss = None # socket

## command strings ##############################################################
cmd_str__IDN = b'*IDN?\n'
cmd_str__RST = b'*RST\n'
#
cmd_str__PGEP_EN = b':PGEP:EN'
#
cmd_str__PGU_PWR            = b':PGU:PWR'
cmd_str__PGU_OUTP           = b':PGU:OUTP'
cmd_str__PGU_AUX_OUTP       = b':PGU:AUX:OUTP'
cmd_str__PGU_DCS_TRIG       = b':PGU:DCS:TRIG'
cmd_str__PGU_DCS_DAC0_PNT   = b':PGU:DCS:DAC0:PNT'
cmd_str__PGU_DCS_DAC1_PNT   = b':PGU:DCS:DAC1:PNT'
cmd_str__PGU_DCS_RPT        = b':PGU:DCS:RPT'
cmd_str__PGU_FDCS_TRIG      = b':PGU:FDCS:TRIG'
cmd_str__PGU_FDCS_DAC0      = b':PGU:FDCS:DAC0'
cmd_str__PGU_FDCS_DAC1      = b':PGU:FDCS:DAC1'
cmd_str__PGU_FDCS_RPT       = b':PGU:FDCS:RPT'
cmd_str__PGU_FREQ           = b':PGU:FREQ'
cmd_str__PGU_OFST_DAC0      = b':PGU:OFST:DAC0'
cmd_str__PGU_OFST_DAC1      = b':PGU:OFST:DAC1'
cmd_str__PGU_GAIN_DAC0      = b':PGU:GAIN:DAC0'
cmd_str__PGU_GAIN_DAC1      = b':PGU:GAIN:DAC1'
#


## functions ####################################################################

def scpi_open (timeout=TIMEOUT):
	try:
		ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		ss.settimeout(timeout)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, SO_SNDBUF)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, SO_RCVBUF) # 8192 16384 32768 65536
	except OSError as msg:
		ss = None
		print('error in socket: ', msg)
		raise
	return ss

def scpi_connect (ss, HOST, PORT):
	try:
		ss.connect((HOST, PORT))
	except OSError as msg:
		ss.close()
		ss = None
		print('error in connect: ', msg)
		raise


def scpi_close (ss):
	try:
		ss.close()
	except:
		if ss == None:
			print('error: ss==None')
		raise

def scpi_comm_resp_ss (ss, cmd_str, buf_size=BUF_SIZE_NORMAL, intval=INTVAL) :
	try:
		print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# recv data until finding the sentinel '\n'
	try:
		data = ss.recv(buf_size) # try 1024 131072 524288
		# try   
		while (1):
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
	except:
		print('error in recv')
		raise
	#
	## check response 
	if (len(data)>20):
		print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		print('Received:', repr(data))
	#
	# NG response check 
	if data[0:2]==b'NG':
		print('Received: NG as response')
		#input('Press Enter key!')
		#sleep(3)
	#
	return data


# scpi command for numeric block response
def scpi_comm_resp_numb_ss (ss, cmd_str, buf_size=BUF_SIZE_LARGE, intval=INTVAL, timeout_large=TIMEOUT_LARGE) :
	try:
		print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# cmd: ":PGEP:PO#HBC 524288\n"
	# rsp: "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
	#
	# recv data until finding the sentinel '\n' 
	# but check the sentinel after the data byte count is met.
	#
	# read timeout
	to = ss.gettimeout()
	#print(to)
	# increase timeout
	ss.settimeout(timeout_large)
	#
	try:
		# find the numeric head : must 10 in data 
		data = ss.recv(buf_size)
		while True:
			if len(data)>=10:
				break
			data = data + ss.recv(buf_size)
		#
		#print('header: ', repr(data[0:10])) # header
		#
		# find byte count 
		byte_count = int(data[3:9])
		#print('byte_count=', repr(byte_count)) 
		#
		# collect all data by byte count
		count_to_recv = byte_count + 10 + 1# add header count #add /n
		while True:
			if len(data)>=count_to_recv:
				break
			data = data + ss.recv(buf_size)
		#
		# check the sentinel 
		while True:
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
		#
	except:
		print('error in recv')
		raise
	#
	if (len(data)>20):
		print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		print('Received:', repr(data))
	#
	# timeout back to prev
	ss.settimeout(to)
	#
	data = data[10:(10+byte_count)]
	print('data:', data[0:20].hex(),  ' (first 20 bytes)')
	#
	return [byte_count, data]
	
	

## generate pulse info for FDCS command

def conv_dec_to_bit_2s_comp_16bit(dec, full_scale=20):
	if dec > full_scale/2-full_scale/2**16 :
		dec = full_scale/2-full_scale/2**16
	if dec < -full_scale/2-full_scale/2**16 :
		dec = -full_scale/2-full_scale/2**16
	#bit_2s_comp = int( 0x10000 * ( dec + full_scale/2)    / full_scale ) + 0x8000
	bit_2s_comp = int( 0x10000 * ( dec + full_scale/2)    / full_scale +0.5) + 0x8000
	if bit_2s_comp > 0xFFFF :
		bit_2s_comp -= 0x10000
	return bit_2s_comp

#test_codes = [ conv_dec_to_bit_2s_comp_16bit(x) for x in [-10,-5,0,5,10] ]
#print(test_codes)
	
def conv_bit_2s_comp_16bit_to_dec(bit_2s_comp, full_scale=20):
	if bit_2s_comp >= 0x8000:
		bit_2s_comp -= 0x8000
		#dec = full_scale * (bit_2s_comp) / 0x10000 -10
		dec = full_scale * (bit_2s_comp) / 0x10000 - full_scale/2
	else :
		dec = full_scale * (bit_2s_comp) / 0x10000
		if dec == full_scale/2-full_scale/2**16 :
			dec = full_scale/2
	return dec

#test_codes2 = [ conv_bit_2s_comp_16bit_to_dec(x) for x in test_codes ]
#print(test_codes2)

	
def gen_pulse_info_num_block__inc_step(code_start, code_step, num_steps, code_duration):
	# num_steps : including last step
	#
	pulse_info_num_block_str = b''
	#
	num_codes = num_steps
	#
	# set number of bytes to send : 8 bytes * (number of codes)
	pulse_info_num_block_str += ' #N_{:06d}'.format(num_codes*8).encode()
	#
	code_list = []
	duration_list = []
	sample_list = []
	#
	sample_value = 0
	code_value  = code_start
	#
	for ii in range(num_codes):
		# merge dac code and duration
		test_value   = (code_value<<16) + code_duration
		# convert into text bytes
		test_str = '_{:08X}'.format(test_value).encode()
		# append it to numberic block
		pulse_info_num_block_str += test_str
		#
		# save step and code in list
		code_list     += [code_value]
		duration_list += [code_duration+1] # 0 for 1 step
		sample_list   += [sample_value]
		#
		# update next code 
		sample_value += code_duration+1
		code_value_prev = code_value
		code_value   += code_step
		# check overflow...
		if code_value > 0xFFFF:
			code_value -= 0x10000
		#
		# check unwanted sign reversal ... 
		#   push it to 0x7FFF of 0x8000
		#   case of 0x7FFF : when + --> -
		#   case of 0x8000 : when - --> +
		if code_value_prev >= 0x8000 and code_value_prev < 0xC000:
			if code_value <=0x7FFF and code_value > 0x3FFF:
				code_value = 0x8000
		elif code_value_prev <= 0x7FFF and code_value_prev > 0x3FFF:
			if code_value >= 0x8000 and code_value < 0xC000:
				code_value = 0x7FFF
	#
	# add sentinel
	pulse_info_num_block_str += b' \n'
	#
	sample_code = [code_list, duration_list, sample_list]
	#
	return pulse_info_num_block_str, sample_code


## test tk ########################

#### Button
##  from tkinter import *
##  
##  from tkinter import messagebox
##  
##  top = Tk()
##  top.geometry("100x100")
##  def helloCallBack():
##     msg = messagebox.showinfo( "Hello Python", "Hello World")
##  
##  bb = Button(top, text = "Hello", command = helloCallBack)
##  bb.place(x = 50,y = 50)
##  top.mainloop()




## test parameters #########################################################################
## pass

## test routines ###########################################################################
## pass

## open socket and connect scpi server ####################################################

## try:
## 	ss = scpi_open()
## 	scpi_connect(ss, HOST, PORT)
## except socket.timeout:
## 	ss = scpi_open()
## 	scpi_connect(ss, HOST2, PORT)
## except:
## 	raise

def my_open(host, port):
	#
	ss = scpi_open()
	try:
		print('>> try to connect : {}:{}'.format(host, port))
		scpi_connect(ss, host, port)
	except socket.timeout:
		ss = None
	except:
		raise
	return ss

# TODO: IP setup

## firmware test
#ss = my_open(HOST119,PORT) # firmware test
#if ss == None :
#	raise

##  # browse ip
##  ss = my_open(HOST122,PORT)
##  if ss == None:	ss = my_open(HOST123,PORT)
##  if ss == None:	ss = my_open(HOST124,PORT)
##  if ss == None:	raise

## use __gui_host_ip
ss = my_open(__gui_host_ip,PORT)
if ss == None :
	raise

###########################################################################

### scpi : *IDN?
print('\n>>> {} : {}'.format('Test',cmd_str__IDN))
rsp = scpi_comm_resp_ss(ss, cmd_str__IDN)
print('hex code rcvd: ' + rsp.hex())
print('string rcvd: ' + repr(rsp))


### :PGEP:EN
# :PGEP:EN ON|OFF <NL>			
# :PGEP:EN? <NL>			
#
# ":PGEP:EN ON\n"
# ":PGEP:EN OFF\n"
# ":PGEP:EN?"
print('\n>>> {} : {}'.format('Test',cmd_str__PGEP_EN))
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b' ON\n')
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b'?\n')


### scpi command: ":PGU:PWR"
# :PGU:PWR ON|OFF <NL>			
# :PGU:PWR? <NL>			
#
# ":PGU:PWR ON\n"
# ":PGU:PWR OFF\n"
# ":PGU:PWR?"
#

### power on 
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_PWR))
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b' ON\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')

### scpi command: ":PGU:OUTP" #### // TODO: cmd_str__PGU_OUTP
# :PGU:OUTP ON|OFF <NL>			
# :PGU:OUTP? <NL>			
#
# ":PGU:OUTP ON\n"
# ":PGU:OUTP OFF\n"
# ":PGU:OUTP?"
#

### output on or off
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_OUTP))
scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b'?\n')
#
if __gui_output_enable == 1:
	scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b' ON\n') # normal operation
else:
	scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b' OFF\n') # test operation
#
scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b'?\n')

###########################################################################

## 
print('>>> Use tk GUI control to send AUX IO command.')

#### Checkbutton
##  from tkinter import *
##  
##  import tkinter
##      
##  top = Tk()
##  CheckVar1 = IntVar()
##  CheckVar2 = IntVar()
##  C1 = Checkbutton(top, text = "Music", variable = CheckVar1, \
##                   onvalue = 1, offvalue = 0, height=5, \
##                   width = 20, )
##  C2 = Checkbutton(top, text = "Video", variable = CheckVar2, \
##                   onvalue = 1, offvalue = 0, height=5, \
##                   width = 20)
##  C1.pack()
##  C2.pack()
##  top.mainloop()

#### GUI: Checkbutton and Button
from tkinter import *
from functools import partial

# no call gui ## master = Tk()

## gui variables 
# no call gui ## var_ch2_gain__ = IntVar()
# no call gui ## var_ch1_gain__ = IntVar()
# no call gui ## var_ch2_be____ = IntVar()
# no call gui ## var_ch2_fe____ = IntVar()
# no call gui ## var_ch1_be____ = IntVar()
# no call gui ## var_ch1_fe____ = IntVar()
# no call gui ## var_ch2_sleepn = IntVar()
# no call gui ## var_ch1_sleepn = IntVar()


## aux io functions
def initialize_aux_io():
	print('\n>>>>>>')
	rsp = scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+ b':INIT\n')
#
def write_aux_io():
	print('\n>>>>>>')
	print('{} = {}'.format('var_ch2_gain__', var_ch2_gain__.get()))
	print('{} = {}'.format('var_ch1_gain__', var_ch1_gain__.get()))
	print('{} = {}'.format('var_ch2_be____', var_ch2_be____.get()))
	print('{} = {}'.format('var_ch2_fe____', var_ch2_fe____.get()))
	print('{} = {}'.format('var_ch1_be____', var_ch1_be____.get()))
	print('{} = {}'.format('var_ch1_fe____', var_ch1_fe____.get()))
	print('{} = {}'.format('var_ch2_sleepn', var_ch2_sleepn.get()))
	print('{} = {}'.format('var_ch1_sleepn', var_ch1_sleepn.get()))
	#
	# generate control frame 
	# 
	# var_ch2_gain__ = 0x8000
	# var_ch1_gain__ = 0x4000
	# var_ch2_be____ = 0x2000
	# var_ch2_fe____ = 0x1000
	# var_ch1_be____ = 0x0800
	# var_ch1_fe____ = 0x0400
	# var_ch2_sleepn = 0x0200
	# var_ch1_sleepn = 0x0100
	#
	para_ctrl = 0x0000
	if var_ch2_gain__.get()==1:	para_ctrl += 0x8000
	if var_ch1_gain__.get()==1:	para_ctrl += 0x4000
	if var_ch2_be____.get()==1:	para_ctrl += 0x2000
	if var_ch2_fe____.get()==1:	para_ctrl += 0x1000
	if var_ch1_be____.get()==1:	para_ctrl += 0x0800
	if var_ch1_fe____.get()==1:	para_ctrl += 0x0400
	if var_ch2_sleepn.get()==1:	para_ctrl += 0x0200
	if var_ch1_sleepn.get()==1:	para_ctrl += 0x0100
	#
	para_ctrl_str = '#H{:04X}'.format(para_ctrl).encode()
	print('{} = {}'.format('para_ctrl_str', para_ctrl_str))
	#
	# send command 
	scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+ b' ' +para_ctrl_str + b'\n')
#
def write_aux_io__direct(para_ctrl):
	print('\n>>>>>> write_aux_io__direct')
	#
	para_ctrl_str = '#H{:04X}'.format(para_ctrl).encode()
	print('{} = {}'.format('para_ctrl_str', para_ctrl_str))
	#
	# send command 
	scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+ b' ' +para_ctrl_str + b'\n')	
#
def read_aux_io():
	print('\n>>>>>>')
	rsp = scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+ b'?\n')
	#print('hex code rcvd: ' + rsp.hex())	
	#print('rsp: ' + rsp.decode())
	print('rsp: ' + rsp.decode()[2:6])
	#
	rsp_hexa = int(rsp.decode()[2:6],base=16)
	print( 'rsp_hexa : 0x{:04X} '.format(rsp_hexa) )
	#
	var_ch2_gain__.set(1) if rsp_hexa&0x8000 != 0 else var_ch2_gain__.set(0)
	var_ch1_gain__.set(1) if rsp_hexa&0x4000 != 0 else var_ch1_gain__.set(0)
	var_ch2_be____.set(1) if rsp_hexa&0x2000 != 0 else var_ch2_be____.set(0)
	var_ch2_fe____.set(1) if rsp_hexa&0x1000 != 0 else var_ch2_fe____.set(0)
	var_ch1_be____.set(1) if rsp_hexa&0x0800 != 0 else var_ch1_be____.set(0)
	var_ch1_fe____.set(1) if rsp_hexa&0x0400 != 0 else var_ch1_fe____.set(0)
	var_ch2_sleepn.set(1) if rsp_hexa&0x0200 != 0 else var_ch2_sleepn.set(0)
	var_ch1_sleepn.set(1) if rsp_hexa&0x0100 != 0 else var_ch1_sleepn.set(0)
#

## pgu functions
def initialize_pgu():
	print('\n>>>>>> initialize_pgu() : ')
	#
	## TODO: USER VAR // rev E5000
	#
	# E5000 GUI variables:
	#   cycle_count        (integer) //     3 (default)
	#   period_time_ns     (integer) // 20000 (default)
	#   base_voltage_V     (float)   //   0.0 (default)
	#   peak_voltage_V     (float)   //   5.0 (default)
	#   load_impedance_ohm (float)   //   1e6 (default)
	#   width_time_ns      (integer) // 10000 (default)
	#   trans_time_ns      (integer) //  1000 (default)
	#   delay_time_ns      (integer) //   100 (default)
	#
	#gui_cycle_count        =     3
	#gui_period_time_ns     = 20000
	#gui_base_voltage_V     =   0.0
	#gui_peak_voltage_V     =   5.0
	#gui_load_impedance_ohm =    50 # 1e6 or 50
	#gui_width_time_ns      = 10000
	#gui_trans_time_ns      =  1000
	#gui_delay_time_ns      =   100
	#
	##  # load from shell arguments
	##  gui_cycle_count        = __gui_cycle_count       
	##  gui_period_time_ns     = __gui_period_time_ns    
	##  gui_base_voltage_V     = __gui_base_voltage_V    
	##  gui_peak_voltage_V     = __gui_peak_voltage_V    
	##  gui_load_impedance_ohm = __gui_load_impedance_ohm
	##  gui_width_time_ns      = __gui_width_time_ns     
	##  gui_trans_time_ns      = __gui_trans_time_ns     
	##  gui_delay_time_ns      = __gui_delay_time_ns     
	##  gui_trans1_time_ns     = __gui_trans1_time_ns     # new
	##  gui_peak1_voltage_V    = __gui_peak1_voltage_V    # new
	##  gui_width1_time_ns     = __gui_width1_time_ns     # new
	##  gui_trans2_time_ns     = __gui_trans2_time_ns     # new
	#
	## voltage output parameter setup 
	scale_voltage_10V    = 7.650/10 # found by inspection // resolution 0.400mV // full scale current 25.5mA
	output_impedance_ohm = 50 # by board design
	# apply load impedance 
	scale_voltage_10V    = scale_voltage_10V * ( (output_impedance_ohm+__gui_load_impedance_ohm) / __gui_load_impedance_ohm )
	#
	## timing parameter setup
	#time_ns__dac_update    = 5
	time_ns__dac_update    = 10
	#time_ns__code_duration = 10
	time_ns__code_duration = 50
	#
	## time points
	##  T1_ns   = 0
	##  T2_ns   = gui_delay_time_ns 
	##  T3_ns   = gui_delay_time_ns + gui_trans_time_ns
	##  T4_ns   = gui_delay_time_ns                     + gui_width_time_ns
	##  T5_ns   = gui_delay_time_ns                     + gui_width_time_ns + gui_trans1_time_ns
	##  T6_ns   = gui_delay_time_ns                     + gui_width_time_ns                      + gui_width1_time_ns
	##  T7_ns   = gui_delay_time_ns                     + gui_width_time_ns                      + gui_width1_time_ns + gui_trans2_time_ns
	##  T8_ns   = gui_period_time_ns
	##  #
	##  time_ns_list = [
	##  	T1_ns ,
	##  	T2_ns ,
	##  	T3_ns ,
	##  	T4_ns ,
	##  	T5_ns ,
	##  	T6_ns ,
	##  	T7_ns ,
	##  	T8_ns ]
	#
	time_ns_list = [
			__gui_pt01_time_ns ,
			__gui_pt02_time_ns ,
			__gui_pt03_time_ns ,
			__gui_pt04_time_ns ,
			__gui_pt05_time_ns ,
			__gui_pt06_time_ns ,
			__gui_pt07_time_ns ,
			__gui_pt08_time_ns ,
			__gui_pt09_time_ns ,
			__gui_pt10_time_ns ]
	
	## voltage points
	##  V1_volt = gui_base_voltage_V  * scale_voltage_10V
	##  V2_volt = gui_base_voltage_V  * scale_voltage_10V
	##  V3_volt = gui_peak_voltage_V  * scale_voltage_10V
	##  V4_volt = gui_peak_voltage_V  * scale_voltage_10V
	##  V5_volt = gui_peak1_voltage_V * scale_voltage_10V
	##  V6_volt = gui_peak1_voltage_V * scale_voltage_10V
	##  V7_volt = gui_base_voltage_V  * scale_voltage_10V
	##  V8_volt = gui_base_voltage_V  * scale_voltage_10V
	##  #
	##  level_volt_list = [
	##  	V1_volt ,
	##  	V2_volt ,
	##  	V3_volt ,
	##  	V4_volt ,
	##  	V5_volt ,
	##  	V6_volt ,
	##  	V7_volt ,
	##  	V8_volt ]
	level_volt_list = [
		__gui_pt01_volt_V * scale_voltage_10V ,
		__gui_pt02_volt_V * scale_voltage_10V ,
		__gui_pt03_volt_V * scale_voltage_10V ,
		__gui_pt04_volt_V * scale_voltage_10V ,
		__gui_pt05_volt_V * scale_voltage_10V ,
		__gui_pt06_volt_V * scale_voltage_10V ,
		__gui_pt07_volt_V * scale_voltage_10V ,
		__gui_pt08_volt_V * scale_voltage_10V ,
		__gui_pt09_volt_V * scale_voltage_10V ,
		__gui_pt10_volt_V * scale_voltage_10V ]
	
	# reduce
	time_ns_list    = time_ns_list   [0:__gui_num_points]
	level_volt_list = level_volt_list[0:__gui_num_points]
	
	## merge list
	merged_time_level_list = [ [t,y] for t,y in zip(time_ns_list,level_volt_list) ]
	# print
	print('{} = {}'.format('merged_time_level_list', merged_time_level_list))#
	print('{} = {}'.format('time_ns__dac_update', time_ns__dac_update))#
	print('{} = {}'.format('time_ns__code_duration', time_ns__code_duration))#
	#
	#
	## set DAC update freq
	pgu_freq_in_100kHz = int( 1/(time_ns__dac_update*1e-9)/100000 )
	#
	pgu_freq_in_100kHz_str = ' {:04d} \n'.format(pgu_freq_in_100kHz).encode()
	#
	print('pgu_freq_in_100kHz_str:', repr(pgu_freq_in_100kHz_str))
	#
	#
	## set pulse repeat number
	# TODO: repeat number
	pulse_repeat_number_dac0 = __gui_cycle_count
	pulse_repeat_number_dac1 = __gui_cycle_count
	#
	#
	pgu_repeat_num_str = ' #H{:04X}{:04X} \n'.format(pulse_repeat_number_dac1,pulse_repeat_number_dac0).encode()
	#
	print('pgu_repeat_num_str:', repr(pgu_repeat_num_str))
	#
	#
	## set DAC full scale current gain 
	# TODO: DAC full scale current gain 
	DAC_full_scale_current__mA = 25.5 # 20.1Vpp
	I_FS__mA  = DAC_full_scale_current__mA
	R_FS__ohm = 10e3
	DAC_gain  = int( (I_FS__mA/1000*R_FS__ohm - 86.6)/0.220 + 0.5)
	#
	pgu_fsc_gain_str = ' #H{:04X}{:04X} \n'.format(DAC_gain,DAC_gain).encode()
	#
	print('pgu_fsc_gain_str:', repr(pgu_fsc_gain_str))
	#
	if DAC_gain>0x3FF or DAC_gain<0 :
		print('>>> please check the full scale current: {}'.format(DAC_full_scale_current__mA))
		raise
	#
	#
	## set DAC offset current 
	#   pgu_offset_con_str
	#
	# // data = {DAC_ch1_aux, DAC_ch2_aux}
	# // DAC_ch#_aux = {PN_Pol_sel, Source_Sink_sel, 0000, 10 bit data}
	# //                PN_Pol_sel      = 0/1 for P/N
	# //                Source_Sink_sel = 0/1 for Source/Sink
	#
	#  AD9783 offset current spec: 
	#    0x000 for 0.0mA; 0x200 for 1.0mA; 0x3FF for 2.0mA
	#
	DAC_offset_current__mA = 0 # 0 # 0.625 mA
	N_pol_sel = 0 # 1
	Sink_sel  = 0 # 1
	#
	DAC_offset_current__code = int(DAC_offset_current__mA * 0x200 + 0.5)
	#
	if DAC_offset_current__code > 0x3FF :
		print('>>> please check the offset current: {}'.format(DAC_offset_current__mA))
		raise
	#
	DAC_offset = (N_pol_sel<<15) + (Sink_sel<<14) + DAC_offset_current__code;
	#
	pgu_offset_con_str = ' #H{:04X}{:04X} \n'.format(DAC_offset,DAC_offset).encode()
	#
	print('pgu_offset_con_str:', repr(pgu_offset_con_str))
	#
	#
	## convert dec into hexadeciaml codes
	# calculate numbers of steps 
	num_steps_list = [ int((b[0] - a[0])/time_ns__code_duration + 0.5) for a,b in zip( merged_time_level_list[:-1], merged_time_level_list[1:]) ]
	#
	print('{} = {}'.format('num_steps_list', num_steps_list))#
	#
	# calculate steps in voltage
	level_diff_volt_list = [  n[1]-m[1]  for m,n in zip(merged_time_level_list[:-1],merged_time_level_list[1:]) ]
	print( '{} = [{}]'.format('level_diff_volt_list', ', '.join('{}'.format(x) for x in level_diff_volt_list)) )#
	
	# calculate levels in code
	level_code_list = [ conv_dec_to_bit_2s_comp_16bit(m[1]) for m in merged_time_level_list ]
	print( '{} = [{}]'.format('level_code_list', ', '.join('0x{:04X}'.format(x) for x in level_code_list)) )#
	#
	# calculate steps in code
	level_step_code_list = [ conv_dec_to_bit_2s_comp_16bit( dd/ss ) for dd,ss in zip(level_diff_volt_list,num_steps_list) ]
	print( '{} = [{}]'.format('level_step_code_list', ', '.join('0x{:04X}'.format(x) for x in level_step_code_list)) )#
	#
	# calculate code duration 
	#time_step_code   = int( time_ns__code_duration / time_ns__dac_update ) - 1
	time_step_code   = int( time_ns__code_duration / time_ns__dac_update +0.5) - 1
	print('{} = 0x{:04X}'.format('time_step_code  ',time_step_code  ))#
	
	# check codes
	if any(x > 0xFFFF for x in level_code_list) :
		print('>>> please check the 16-bit code overflow: {}'.format('level_code_list'))
		raise
	if any(x > 0xFFFF for x in level_step_code_list) :
		print('>>> please check the 16-bit code overflow: {}'.format('level_step_code_list'))
		raise
	if time_step_code > 0xFFFF :
		print('>>> please check the 16-bit code overflow: {}'.format('time_step_code'))
		raise
	
	## generate DAC codes : line-by-line
	print('>> Generate DAC codes line-by-line.')
	
	# calculate the number of lines to generate
	num_lines = len(level_step_code_list)
	print('{} = {}'.format('num_lines',num_lines))
	
	# generate number block and DAC codes
	num_block_str__sample_code__list = \
	[ list(gen_pulse_info_num_block__inc_step(code_start,code_step,num_steps,time_step_code))
	for code_start,code_step,num_steps in zip(level_code_list[:-1],level_step_code_list,num_steps_list) ]
	print('{} = {}'.format('num_block_str__sample_code__list',num_block_str__sample_code__list))
	
	# save it to global
	
	## display waveform
	
	import matplotlib.pyplot as plt
	plt.ion() # matplotlib interactive mode 

	# data located in  merged_time_level_list
	t_list = [ m[0] for m in merged_time_level_list ]
	y_list = [ m[1] for m in merged_time_level_list ]

	# DAC codes located in num_block_str__sample_code__list 
	dac_list = [ m[1][0] for m in num_block_str__sample_code__list ]
	# remove nested list 
	dac_list = [n for m in dac_list for n in m]
	print('{} = {}'.format('dac_list ',dac_list ))#
	
	# convert into voltage
	#dac_volt_list = [ conv_bit_2s_comp_16bit_to_dec(x) for x in dac_list ]
	# full scale current correction
	display_scale_voltage_10V = 1.0252270686477878 # found by inspection
	dac_volt_list = [ conv_bit_2s_comp_16bit_to_dec(x*display_scale_voltage_10V, 20/20*DAC_full_scale_current__mA) for x in dac_list ]
	print('{} = {}'.format('dac_volt_list ',dac_volt_list ))#
	#
	# update t_list for dac_volt_list
	t_dac_list = [ x * time_ns__code_duration for x in range(len(dac_volt_list)) ]
	print('{} = {}'.format('t_dac_list ',t_dac_list ))#

	# check data length fitted in FIFO length
	# TODO: check FIFO data length
	len_FIFO = 1024
	len_DAC_data = len(dac_list)
	print('{} = {}'.format('len_DAC_data ',len_DAC_data ))#
	if len_DAC_data > len_FIFO:
		print('> Warning: DAC data is not fitted with FIFO length! ')
		raise
	#
	
	# DAC sample-and-hold output (SnH)
	# input : dac_volt_list, t_dac_list
	# output: dac_volt_snh_list, t_dac_snh_list
	def conv_dac_points_2_sample_n_hold_outputs(dac_volt_list, t_dac_list):
		dac_volt_snh_list = [] 
		t_dac_snh_list   = []
		# insert hold data
		dac_volt_snh_list = [ x for x in dac_volt_list for _ in range(2)]
		t_dac_snh_list   = [t_dac_list[0]] + [ x for x in t_dac_list[1:] for _ in range(2)] + [t_dac_list[-1]*2-t_dac_list[-2]]
		# remove duplicate
		# ...
		return dac_volt_snh_list, t_dac_snh_list
	#
	dac_volt_snh_list, t_dac_snh_list = conv_dac_points_2_sample_n_hold_outputs(dac_volt_list, t_dac_list)
	print('{} = {}'.format('dac_volt_snh_list ',dac_volt_snh_list ))#
	print('{} = {}'.format('t_dac_snh_list ',t_dac_snh_list ))#
	#

	# repeat waveform 
	#   pulse_repeat_number_dac0
	#   input:
	#   t_list, y_list
	#   t_dac_snh_list, dac_volt_snh_list
	#   output:
	#   t_rep_list, y_rep_list
	#   t_dac_snh_rep_list, dac_volt_snh_rep_list
	def add_repeated_pattern_in_time_domain(t_list, y_list, num_repeat=1):
		#t_rep_list = t_list
		#y_rep_list = y_list
		t_rep_list = [ x for x in t_list ]
		#
		if num_repeat>1 :
			for _ in range(num_repeat-1):
				tmp_list = [ x+t_rep_list[-1] for x in t_list ]
				t_rep_list += tmp_list
		#
		y_rep_list = [x for _ in range(num_repeat) for x in y_list]
		#
		#
		return t_rep_list, y_rep_list
	#
	# check if pulse_repeat_number_dac0 == 0
	if pulse_repeat_number_dac0 == 0:
		pulse_repeat_number_dac0 = 2 # for display only
	#
	t_rep_list, y_rep_list                    = add_repeated_pattern_in_time_domain (t_list, y_list, pulse_repeat_number_dac0)
	t_dac_snh_rep_list, dac_volt_snh_rep_list = add_repeated_pattern_in_time_domain (t_dac_snh_list, dac_volt_snh_list, pulse_repeat_number_dac0)
	#
	#t_dac_rep_list, dac_volt_rep_list         = add_repeated_pattern_in_time_domain (t_dac_list, dac_volt_list, pulse_repeat_number_dac0)
	# add last point to t_dac_list
	t_dac_last    =  2*t_dac_list[-1] - t_dac_list[-2]
	t_dac_list    += [t_dac_last]
	dac_volt_list += [dac_volt_list[-1]]
	#
	t_dac_rep_list, dac_volt_rep_list         = add_repeated_pattern_in_time_domain (t_dac_list, dac_volt_list, pulse_repeat_number_dac0)
	#
	print('{} = {}'.format('t_list    ',t_list     ))#
	print('{} = {}'.format('y_list    ',y_list     ))#
	print('{} = {}'.format('t_rep_list',t_rep_list ))#
	print('{} = {}'.format('y_rep_list',y_rep_list ))#
	#
	
	# check voltage against 10V ref
	#print('{} = {}'.format('dac_volt_list[15]    ',dac_volt_list[15]     ))#
	#scale_voltage_10V = 10/dac_volt_list[15] # 1.0252270686477878
	#print('{} = {}'.format('scale_voltage_10V    ',scale_voltage_10V     ))#
	
	# title str
	title_str = 'command waveform (red) and DAC sample_n_hold output (green)'
	title_str += '\n DAC update {}ns, Step duration {}ns'.format(time_ns__dac_update,time_ns__code_duration)
	title_str += ', Full scale current {}mA'.format(DAC_full_scale_current__mA)
	#
	
	# TODO: plot
	FIG_NUM = 0
	plt.figure(FIG_NUM,figsize=(8,4))
	#
	plt.plot(t_rep_list, y_rep_list, 'ro-', markersize=10)
	plt.plot(t_dac_rep_list,dac_volt_rep_list, 'bs', markersize=5)
	plt.plot(t_dac_snh_rep_list, dac_volt_snh_rep_list, 'g-', markersize=2)
	plt.title(title_str)
	plt.ylabel('Voltage')
	plt.xlabel('Time(ns)')
	plt.grid(True)


	## send initial commands
	
	# set DAC update freq based on 100kHz
	print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FREQ))
	scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+pgu_freq_in_100kHz_str) #
	
	# offset DAC : 0x140 0.625mA, AUX2N active[7] (1) , sink current[6] (1) 
	print('\n>>> {} : {}'.format('Test',cmd_str__PGU_OFST_DAC0))
	scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+pgu_offset_con_str)
	print('\n>>> {} : {}'.format('Test',cmd_str__PGU_OFST_DAC1))
	scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+pgu_offset_con_str)
	
	# full scale DAC : 28.1mA  @ 0x02D0
	print('\n>>> {} : {}'.format('Test',cmd_str__PGU_GAIN_DAC0))
	scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+pgu_fsc_gain_str)
	print('\n>>> {} : {}'.format('Test',cmd_str__PGU_GAIN_DAC1))
	scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+pgu_fsc_gain_str)	
	#
	return num_block_str__sample_code__list, pgu_repeat_num_str
#
def load_pgu_waveform(num_block_str__sample_code__list, pgu_repeat_num_str):
	print('\n>>>>>> load_pgu_waveform()')
	#
	# read num_block_str__sample_code__list from global...
	#
	# setup fifo data
	print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_DAC0))
	# send number blocks
	for mm in num_block_str__sample_code__list:
		scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_DAC0 + mm[0])
	#
	print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_DAC1))
	# send number blocks
	for mm in num_block_str__sample_code__list:
		scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_DAC1 + mm[0])
	#
	# setup repeat number
	print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_RPT))
	scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_RPT+pgu_repeat_num_str)
	#
	pass
#
def trig_pgu_output():
	print('\n>>>>>> trig_pgu_output()')
	
	### trig on : FDCS mode
	print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_TRIG))
	scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_TRIG+b' ON\n')
	
	## wait for a while
	sleep(3.5)
	#sleep(3.5)
	
	## TODO: check trig
	#input('Enter any key to trig off')
	
	### trig off : FDCS mode
	print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_TRIG))
	scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_TRIG+b' OFF\n')	
	
	pass
#

## gui setup
def tk_win_setup(master):
	Label(master, text="AUX IO:").grid(row=0, sticky=W)
	#
	# initialize AUX IO
	initialize_aux_io()
	# must update aux io 
	read_aux_io()
	#
	Checkbutton(master, text="var_ch2_gain__", variable=var_ch2_gain__).grid(row=1, sticky=W)
	Checkbutton(master, text="var_ch1_gain__", variable=var_ch1_gain__).grid(row=2, sticky=W)
	Checkbutton(master, text="var_ch2_be____", variable=var_ch2_be____).grid(row=3, sticky=W)
	Checkbutton(master, text="var_ch2_fe____", variable=var_ch2_fe____).grid(row=4, sticky=W)
	Checkbutton(master, text="var_ch1_be____", variable=var_ch1_be____).grid(row=5, sticky=W)
	Checkbutton(master, text="var_ch1_fe____", variable=var_ch1_fe____).grid(row=6, sticky=W)
	Checkbutton(master, text="var_ch2_sleepn", variable=var_ch2_sleepn).grid(row=7, sticky=W)
	Checkbutton(master, text="var_ch1_sleepn", variable=var_ch1_sleepn).grid(row=8, sticky=W)
	#
	# initialize PGU 
	num_block_str__sample_code__list, pgu_repeat_num_str = initialize_pgu()
	#
	
	## command buttons
	Button(master, text='Quit', command=master.quit).grid(row=9,  sticky=W, pady=4)
	#
	# AUX IO control
	Button(master, text='Send AUX IO control', command=write_aux_io ).grid(row=10, sticky=W, pady=4)
	Button(master, text='Read AUX IO control', command=read_aux_io  ).grid(row=11, sticky=W, pady=4)
	#
	# pulse output control 
	Button(master, text='Load PGU waveform ', command=partial(load_pgu_waveform,num_block_str__sample_code__list,pgu_repeat_num_str) ).grid(row=12, sticky=W, pady=4)
	Button(master, text='Trigger PGU output', command=trig_pgu_output   ).grid(row=13, sticky=W, pady=4)
	#
	return
#
## no call gui ## tk_win_setup(master)

## main loop for gui
## no call gui ## mainloop() ####


## single run without gui control

# initialize PGU and make waveform data
num_block_str__sample_code__list, pgu_repeat_num_str = initialize_pgu()

# set path/gain control 
para_ctrl = __gui_aux_io_control
write_aux_io__direct(para_ctrl & 0xFCFF) # without sleepn enabled

# load wavefomm
load_pgu_waveform(num_block_str__sample_code__list,pgu_repeat_num_str)

# sleepn control on
write_aux_io__direct(para_ctrl & 0xFFFF) 

# trigger output
trig_pgu_output()

# sleepn control off
write_aux_io__direct(para_ctrl & 0xFCFF) # without sleepn enabled


################################################################################

## wait for an input  ######################################################
#input('Enter any key to stop')


################################################################################

## wait for a while
#sleep(0.5)

### power off 
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_PWR))
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b' OFF\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')



## PGEP disable
# print('\n>>> {} : {}'.format('Test',cmd_str__PGEP_EN))
# scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b' OFF\n')
# scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b'?\n')


## close socket
scpi_close(ss)
print('\n>>>>>> socket closed.')


##



