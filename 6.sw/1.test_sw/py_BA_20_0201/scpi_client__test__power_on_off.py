## scpi_client__test__basic_wave__long_slopes.py
import socket
#import time
from time import sleep
import sys

# connect(), close() ... style

# https://docs.python.org/3/library/socket.html
# https://docs.python.org/2/library/socket.html

## control parameters 

HOST = '192.168.168.123'  # The server's hostname or IP address
HOST119 = '192.168.168.119'  # The server's hostname or IP address // firmware test ip
HOST122 = '192.168.168.122'  # The server's hostname or IP address
HOST123 = '192.168.168.123'  # The server's hostname or IP address
HOST124 = '192.168.168.124'  # The server's hostname or IP address
HOST2 = '192.168.172.1'  # The server's hostname or IP address
PORT = 5025               # The port used by the server
#TIMEOUT = 5.3 # socket timeout
TIMEOUT = 1000 # socket timeout // for debug 1000s
SO_SNDBUF = 2048
SO_RCVBUF = 32768
INTVAL = 0.1 # sec for waiting before recv()
BUF_SIZE_NORMAL = 2048
BUF_SIZE_LARGE = 16384
TIMEOUT_LARGE = TIMEOUT*10

ss = None # socket

## command strings ##############################################################
cmd_str__IDN = b'*IDN?\n'
cmd_str__RST = b'*RST\n'
#
cmd_str__PGEP_EN = b':PGEP:EN'
#
cmd_str__PGU_PWR            = b':PGU:PWR'
cmd_str__PGU_OUTP           = b':PGU:OUTP'
cmd_str__PGU_DCS_TRIG       = b':PGU:DCS:TRIG'
cmd_str__PGU_DCS_DAC0_PNT   = b':PGU:DCS:DAC0:PNT'
cmd_str__PGU_DCS_DAC1_PNT   = b':PGU:DCS:DAC1:PNT'
cmd_str__PGU_DCS_RPT        = b':PGU:DCS:RPT'
cmd_str__PGU_FDCS_TRIG      = b':PGU:FDCS:TRIG'
cmd_str__PGU_FDCS_DAC0      = b':PGU:FDCS:DAC0'
cmd_str__PGU_FDCS_DAC1      = b':PGU:FDCS:DAC1'
cmd_str__PGU_FDCS_RPT       = b':PGU:FDCS:RPT'
cmd_str__PGU_FREQ           = b':PGU:FREQ'
cmd_str__PGU_OFST_DAC0      = b':PGU:OFST:DAC0'
cmd_str__PGU_OFST_DAC1      = b':PGU:OFST:DAC1'
cmd_str__PGU_GAIN_DAC0      = b':PGU:GAIN:DAC0'
cmd_str__PGU_GAIN_DAC1      = b':PGU:GAIN:DAC1'
#


## functions ####################################################################

def scpi_open (timeout=TIMEOUT):
	try:
		ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		ss.settimeout(timeout)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, SO_SNDBUF)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, SO_RCVBUF) # 8192 16384 32768 65536
	except OSError as msg:
		ss = None
		print('error in socket: ', msg)
		raise
	return ss

def scpi_connect (ss, HOST, PORT):
	try:
		ss.connect((HOST, PORT))
	except OSError as msg:
		ss.close()
		ss = None
		print('error in connect: ', msg)
		raise


def scpi_close (ss):
	try:
		ss.close()
	except:
		if ss == None:
			print('error: ss==None')
		raise

def scpi_comm_resp_ss (ss, cmd_str, buf_size=BUF_SIZE_NORMAL, intval=INTVAL) :
	try:
		print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# recv data until finding the sentinel '\n'
	try:
		data = ss.recv(buf_size) # try 1024 131072 524288
		# try   
		while (1):
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
	except:
		print('error in recv')
		raise
	#
	## check response 
	if (len(data)>20):
		print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		print('Received:', repr(data))
	#
	# NG response check 
	if data[0:2]==b'NG':
		print('Received: NG as response')
		#input('Press Enter key!')
		#sleep(3)
	#
	return data


# scpi command for numeric block response
def scpi_comm_resp_numb_ss (ss, cmd_str, buf_size=BUF_SIZE_LARGE, intval=INTVAL, timeout_large=TIMEOUT_LARGE) :
	try:
		print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# cmd: ":PGEP:PO#HBC 524288\n"
	# rsp: "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
	#
	# recv data until finding the sentinel '\n' 
	# but check the sentinel after the data byte count is met.
	#
	# read timeout
	to = ss.gettimeout()
	#print(to)
	# increase timeout
	ss.settimeout(timeout_large)
	#
	try:
		# find the numeric head : must 10 in data 
		data = ss.recv(buf_size)
		while True:
			if len(data)>=10:
				break
			data = data + ss.recv(buf_size)
		#
		#print('header: ', repr(data[0:10])) # header
		#
		# find byte count 
		byte_count = int(data[3:9])
		#print('byte_count=', repr(byte_count)) 
		#
		# collect all data by byte count
		count_to_recv = byte_count + 10 + 1# add header count #add /n
		while True:
			if len(data)>=count_to_recv:
				break
			data = data + ss.recv(buf_size)
		#
		# check the sentinel 
		while True:
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
		#
	except:
		print('error in recv')
		raise
	#
	if (len(data)>20):
		print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		print('Received:', repr(data))
	#
	# timeout back to prev
	ss.settimeout(to)
	#
	data = data[10:(10+byte_count)]
	print('data:', data[0:20].hex(),  ' (first 20 bytes)')
	#
	return [byte_count, data]
	
	

## generate pulse info for FDCS command

def conv_dec_to_bit_2s_comp_16bit(dec, full_scale=20):
	if dec > full_scale/2-full_scale/2**16 :
		dec = full_scale/2-full_scale/2**16
	if dec < -full_scale/2-full_scale/2**16 :
		dec = -full_scale/2-full_scale/2**16
	bit_2s_comp = int( 0x10000 * ( dec + full_scale/2)    / full_scale ) + 0x8000
	if bit_2s_comp > 0xFFFF :
		bit_2s_comp -= 0x10000
	return bit_2s_comp

def gen_pulse_info_num_block__inc_step(code_start, code_step, num_steps, code_duration):
	# num_steps : including last step
	#
	pulse_info_num_block_str = b''
	#
	num_codes = num_steps
	#
	# set number of bytes to send : 8 bytes * (number of codes)
	pulse_info_num_block_str += ' #N_{:06d}'.format(num_codes*8).encode()
	#
	code_value = code_start
	#
	for ii in range(num_codes):
		# merge dac code and duration
		test_value = (code_value<<16) + code_duration
		# convert into text bytes
		test_str = '_{:08X}'.format(test_value).encode()
		# append it to numberic block
		pulse_info_num_block_str += test_str
		#
		# update next code 
		code_value += code_step
		if code_value > 0xFFFF:
			code_value -= 0x10000
		#
	#
	# add sentinel
	pulse_info_num_block_str += b' \n'
	#
	return pulse_info_num_block_str

## test parameters #########################################################################
# TODO: USER VAR


## test routines ###########################################################################


## open socket and connect scpi server ####################################################

## try:
## 	ss = scpi_open()
## 	scpi_connect(ss, HOST, PORT)
## except socket.timeout:
## 	ss = scpi_open()
## 	scpi_connect(ss, HOST2, PORT)
## except:
## 	raise

def my_open(host, port):
	#
	ss = scpi_open()
	try:
		print('>> try to connect : {}:{}'.format(host, port))
		scpi_connect(ss, host, port)
	except socket.timeout:
		ss = None
	except:
		raise
	return ss

## TODO: IP setup

##  # firmware test
##  ss = my_open(HOST119,PORT) # firmware test
##  if ss == None :
##  	raise


# browse ip
ss = my_open(HOST122,PORT)
if ss == None:	ss = my_open(HOST123,PORT)
if ss == None:	ss = my_open(HOST124,PORT)
if ss == None:	raise


###########################################################################

### scpi : *IDN?
print('\n>>> {} : {}'.format('Test',cmd_str__IDN))
rsp = scpi_comm_resp_ss(ss, cmd_str__IDN)
print('hex code rcvd: ' + rsp.hex())
print('string rcvd: ' + repr(rsp))


### :PGEP:EN
# :PGEP:EN ON|OFF <NL>			
# :PGEP:EN? <NL>			
#
# ":PGEP:EN ON\n"
# ":PGEP:EN OFF\n"
# ":PGEP:EN?"
print('\n>>> {} : {}'.format('Test',cmd_str__PGEP_EN))
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b' ON\n')
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b'?\n')


### scpi command: ":PGU:PWR"
# :PGU:PWR ON|OFF <NL>			
# :PGU:PWR? <NL>			
#
# ":PGU:PWR ON\n"
# ":PGU:PWR OFF\n"
# ":PGU:PWR?"
#

### power on 
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_PWR))
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b' ON\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')

### scpi command: ":PGU:OUTP" #### // TODO: cmd_str__PGU_OUTP
# :PGU:OUTP ON|OFF <NL>			
# :PGU:OUTP? <NL>			
#
# ":PGU:OUTP ON\n"
# ":PGU:OUTP OFF\n"
# ":PGU:OUTP?"
#

### output on or off
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_OUTP))
scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b' ON\n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b' OFF\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b'?\n')


###########################################################################

### scpi command: ":PGU:FREQ" // TODO: cmd_str__PGU_FREQ
# set DAC update freq based on 100kHz
#
# :PGU:FREQ nnnn <NL>			
#
# ":PGU:FREQ 4000 \n"  // for 400.0 MHz
# ":PGU:FREQ 2000 \n"  // for 200.0 MHz
# ":PGU:FREQ 1000 \n"  // for 100.0 MHz
# ":PGU:FREQ 0800 \n"  // for 080.0 MHz
# ":PGU:FREQ 0500 \n"  // for 050.0 MHz
# ":PGU:FREQ 0200 \n"  // for 020.0 MHz
#
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FREQ))
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 4000 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 2000 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 1000 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0800 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0500 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0200 \n') #OK
#
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 4000 \n') # 400MHz
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 2000 \n') # 200MHz
scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 1000 \n') # 100MHz
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0500 \n') # 50MHz
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0200 \n') # 20MHz
#
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+pgu_freq_in_100kHz_str) #

### check pll status ###
#define EP_ADRS__TEST_IO_MON__PGU      0x23
print('\n>>> {} : {}'.format('Read', 'PLL status'))
scpi_comm_resp_ss(ss, b':PGEP:WO#H23? \n') #

### scpi command: ":PGU:OFST:DAC0" 
# cmd_str__PGU_OFST_DAC0      = b':PGU:OFST:DAC0'
#
# :PGU:OFST:DAC0? <NL>			
# :PGU:OFST:DAC0 #Hnnnnnnnn <NL>			
#
# // data = {DAC_ch1_aux, DAC_ch2_aux}
# // DAC_ch#_aux = {PN_Pol_sel, Source_Sink_sel, 0000, 10 bit data}
# //                PN_Pol_sel      = 0/1 for P/N
# //                Source_Sink_sel = 0/1 for Source/Sink
#
#  AD9783 offset current spec: 
#    0x000 for 0.0mA; 0x200 for 1.0mA; 0x3FF for 2.0mA
#
# offset DAC : 0x140 0.625mA, AUX2N active[7] (1) , sink current[6] (1) 
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_OFST_DAC0))
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+b'?\n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+b' #HC141C140 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+b' #H00000000 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+b'?\n')

### scpi command: ":PGU:OFST:DAC1" 
# cmd_str__PGU_OFST_DAC1      = b':PGU:OFST:DAC1'
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_OFST_DAC1))
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+b'?\n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+b' #HC141C140 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+b' #H00000000 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+b'?\n')

### scpi command: ":PGU:GAIN:DAC0" 
# cmd_str__PGU_GAIN_DAC0      = b':PGU:GAIN:DAC0'
#
# ":PGU:GAIN:DAC0? \n" 
# ":PGU:GAIN:DAC0 #H02D002D0 \n" 
#
# // data = {DAC_ch1_fsc, DAC_ch2_fsc}
# // DAC_ch#_fsc = {000000, 10 bit data}
#
#  AD9783 full scale current spec: 
#    0x000 for 8.66mA; 0x200 for 20.0mA; 0x3FF for 31.66mA
#
# full scale DAC : 28.1mA  @ 0x02D0
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_GAIN_DAC0))
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b'?\n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b' #H02D102D0 \n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b' #H02A002A0 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b' #H02000200 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b'?\n')

### scpi command: ":PGU:GAIN:DAC1" 
# cmd_str__PGU_GAIN_DAC1      = b':PGU:GAIN:DAC1'
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_GAIN_DAC1))
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b'?\n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b' #H02D102D0 \n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b' #H02A002A0 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b' #H02000200 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b'?\n')


################################################################################

## wait for an input  ######################################################
input('Enter any key to stop')


################################################################################

## wait for a while
sleep(1.0)

### power off 
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_PWR))
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b' OFF\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')



## PGEP disable
print('\n>>> {} : {}'.format('Test',cmd_str__PGEP_EN))
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b' OFF\n')
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b'?\n')


## close socket
scpi_close(ss)


##



