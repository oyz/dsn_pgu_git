## ok module for python 3.5
## works both with x64 and win32
## 
##   Windows DLL Usage
##   http://www.opalkelly.com:8090/display/FPSDK/Programming+Languages
##   http://www.microsoft.com/en-us/download/details.aspx?id=40784
##
## https://www.opalkelly.com/examples/home/
## https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel.html
## http://www.opalkelly.com:8090/display/FPSDK/Getting+Started
## wxPython 2.8 http://www.wxpython.org
#
## ok_pgu_cpu__08_dac_amp_path_test.py : test code for DAC AMP path test
#  

####
## controls
#  
#  
FPGA_CONFIGURE = 0
#
#BIT_FILENAME = ''; 
#
####

####
## library call
import ok_pgu_cpu__lib as pgu
#
# display board conf data in library  
ret = pgu.display_conf_header()
print(ret)
#
# set bit filename including path 
BIT_FILENAME = pgu.conf.OK_EP_ADRS_CONFIG['bit_filename']
#
####

####
## init : dev
dev = pgu.ok_init()
print(dev)
####

####
ret = pgu.ok_caller_id()
print(ret)
####

####
## open
FPGA_SERIAL = '' # for any
#FPGA_SERIAL = '1739000J7V' # for module: CMU-CPU-F5500-FPGA1
#FPGA_SERIAL = '1908000OVP' # for module: CMU-CPU-F5500-FPGA2
#FPGA_SERIAL = '1739000J8A' # for module: CMU-CPU-F5500-FPGA3
#FPGA_SERIAL = '1739000J63' # for module: CMU-CPU-F5500-FPGA4
#FPGA_SERIAL = '1739000J8I' # for module: CMU-CPU-F5500-FPGA5
#
ret = pgu.ok_open(FPGA_SERIAL)
print(ret)
####


####
## FPGA_CONFIGURE
if FPGA_CONFIGURE==1: 
	ret = pgu.ok_conf(BIT_FILENAME)
	print(ret)
	print('Downloading bit file makes your debugger be reset!!')
####  

####
## read fpga_image_id
fpga_image_id__str = pgu.read_fpga_image_id()
print(fpga_image_id__str)
####

####
## read FPGA internal temp and volt
ret = pgu.monitor_fpga()
print(ret)
####

####
## test counter on 
ret = pgu.test_counter('ON')
print(ret)
####

####
## power control on : LED, PWR_DAC
#
print('> power on: led=1,pwr_dac=1,pwr_adc=0,pwr_amp=0')
pgu.spio_ext__pwr_led(led=1,pwr_dac=1,pwr_adc=0,pwr_amp=1)
####

####
## CLKD on : 400MHz clock output
#
print('> CLKD test')
pgu.clkd_init()
pgu.clkd_reg_read_b8(0x000) # readback 0x18
pgu.clkd_reg_read_b8(0x003) # read IC 0x41 

## CLKD reg  

##  Settings for Clock Distribution : 
#reg_0x010 = pgu.clkd_reg_read_b8(0x010)
#reg_0x1E1 = pgu.clkd_reg_read_b8(0x1E1)
#pgu.clkd_reg_write_b8(0x010,(reg_0x010&0xFC)+0x01) # PLL power-down
#pgu.clkd_reg_write_b8(0x1E1,(reg_0x1E1&0xFC)+0x01) # Bypass VCO divider
#reg_0x010 = pgu.clkd_reg_read_b8(0x010)
#reg_0x1E1 = pgu.clkd_reg_read_b8(0x1E1)
#print('{} = 0x{:02X}'.format('reg_0x010',reg_0x010))
#print('{} = 0x{:02X}'.format('reg_0x1E1',reg_0x1E1))
#
pgu.clkd_reg_write_b8(0x010,0x7D) # PLL power-down
#
# 400MHz common = 400MHz/1
#pgu.clkd_reg_write_b8(0x1E1,0x01) # Bypass VCO divider # for 400MHz common clock 
#
# 200MHz common = 400MHz/(2+0)
pgu.clkd_reg_write_b8(0x1E0,0x00) # Set VCO divider # [0,1,2,3,4] for [/2,/3,/4,/5,/6]
pgu.clkd_reg_write_b8(0x1E1,0x00) # Use VCO divider # for 400MHz/X common clock 
#
# 133.3MHz common = 400MHz/(2+1)
#pgu.clkd_reg_write_b8(0x1E0,0x01) # Set VCO divider # [0,1,2,3,4] for [/2,/3,/4,/5,/6]
#pgu.clkd_reg_write_b8(0x1E1,0x00) # Use VCO divider # for 400MHz/X common clock 
#
# 100MHz common = 400MHz/(2+2)
#pgu.clkd_reg_write_b8(0x1E0,0x02) # Set VCO divider # [0,1,2,3,4] for [/2,/3,/4,/5,/6]
#pgu.clkd_reg_write_b8(0x1E1,0x00) # Use VCO divider # for 400MHz/X common clock 
#
# 80MHz common = 400MHz/(2+3)
#pgu.clkd_reg_write_b8(0x1E0,0x03) # Set VCO divider # [0,1,2,3,4] for [/2,/3,/4,/5,/6]
#pgu.clkd_reg_write_b8(0x1E1,0x00) # Use VCO divider # for 400MHz/X common clock 
#
# 66.6MHz common = 400MHz/(2+4)
#pgu.clkd_reg_write_b8(0x1E0,0x04) # Set VCO divider # [0,1,2,3,4] for [/2,/3,/4,/5,/6]
#pgu.clkd_reg_write_b8(0x1E1,0x00) # Use VCO divider # for 400MHz/X common clock 
#
#
# // TODO: DAC update clock
#pgu.clkd_reg_write_b8(0x193,0xBB) # DVD1 div 2+11+11=24 --> DACx: 400MHz/24 = 16.7MHz # OK
#pgu.clkd_reg_write_b8(0x193,0x00) # DVD1 div 2+0+0=2 --> DACx: 400MHz/2 = 200MHz
#pgu.clkd_reg_write_b8(0x193,0x11) # DVD1 div 2+1+1=4 --> DACx: 400MHz/4 = 100MHz
#pgu.clkd_reg_write_b8(0x193,0x33) # DVD1 div 2+3+3=8 --> DACx: 400MHz/8 = 50MHz
#pgu.clkd_reg_write_b8(0x193,0x44) # DVD1 div 2+4+4=10 --> DACx: 400MHz/10 = 40MHz # NG with DAC0
#pgu.clkd_reg_write_b8(0x193,0x55) # DVD1 div 2+5+5=12 --> DACx: 400MHz/12 = 33.3MHz # NG
#pgu.clkd_reg_write_b8(0x193,0x66) # DVD1 div 2+6+6=14 --> DACx: 400MHz/14 = 28.6MHz # NG
#pgu.clkd_reg_write_b8(0x193,0x67) # DVD1 div 2+6+7=15 --> DACx: 400MHz/15 = 26.6MHz # OK
#pgu.clkd_reg_write_b8(0x193,0x77) # DVD1 div 2+7+7=16 --> DACx: 400MHz/16 = 25MHz # OK
#pgu.clkd_reg_write_b8(0x193,0xFF) # DVD1 div 2+15+15=32 --> DACx: 400MHz/32 = 12.5MHz # OK
#
pgu.clkd_reg_write_b8(0x194,0x80) # DVD1 bypass --> DACx: 400MHz/1 = 400MHz
#
pgu.clkd_reg_write_b8(0x0F3,0x08) # enable path for DAC1
#
#pgu.clkd_reg_write_b8(0x199,0x22) # DVD3.1 div 2+2+2=6 
#pgu.clkd_reg_write_b8(0x19B,0x11) # DVD3.2 div 2+1+1=4  --> REFo: 400MHz/24 = 16.7MHz
#
#pgu.clkd_reg_write_b8(0x199,0x00) # DVD3.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x19B,0x00) # DVD3.2 div 2+0+0=2  --> REFo: 400MHz/4 = 100MHz
#
#pgu.clkd_reg_write_b8(0x199,0x00) # DVD3.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x19B,0x11) # DVD3.2 div 2+1+1=4  --> REFo: 400MHz/8 = 50MHz
#
#pgu.clkd_reg_write_b8(0x199,0x00) # DVD3.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x19B,0x33) # DVD3.2 div 2+3+3=8  --> REFo: 400MHz/16 = 25MHz 
#
pgu.clkd_reg_write_b8(0x19C,0x30) # DVD3.1, DVD3.2 all bypass --> REFo: 400MHz/1 = 400MHz
#
# // TODO: FPGA ref clock
#pgu.clkd_reg_write_b8(0x19E,0x22) # DVD4.1 div 2+2+2=6 
#pgu.clkd_reg_write_b8(0x1A0,0x11) # DVD4.2 div 2+1+1=4  --> FPGA: 400MHz/24 = 16.7MHz # OK
#
#pgu.clkd_reg_write_b8(0x19E,0x00) # DVD4.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x1A0,0x00) # DVD4.2 div 2+0+0=2  --> FPGA: 400MHz/4 = 100MHz #OK with LVDS_25 DIFF_TERM
#
#pgu.clkd_reg_write_b8(0x19E,0x00) # DVD4.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x1A0,0x11) # DVD4.2 div 2+1+1=4  --> FPGA: 400MHz/8 = 50MHz #OK with LVDS_25 DIFF_TERM
#
#pgu.clkd_reg_write_b8(0x19E,0x00) # DVD4.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x1A0,0x12) # DVD4.2 div 2+1+2=5  --> FPGA: 400MHz/10 = 40MHz #OK with LVDS_25 DIFF_TERM
#
#pgu.clkd_reg_write_b8(0x19E,0x00) # DVD4.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x1A0,0x22) # DVD4.2 div 2+2+2=6  --> FPGA: 400MHz/12 = 33.3MHz #OK with LVDS_25 DIFF_TERM
#
#pgu.clkd_reg_write_b8(0x19E,0x00) # DVD4.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x1A0,0x33) # DVD4.2 div 2+3+3=8  --> FPGA: 400MHz/16 = 25MHz # OK 
#
#pgu.clkd_reg_write_b8(0x19E,0x11) # DVD4.1 div 2+1+1=4 
#pgu.clkd_reg_write_b8(0x1A0,0x11) # DVD4.2 div 2+1+1=4  --> FPGA: 400MHz/16 = 25MHz # OK 
#
pgu.clkd_reg_write_b8(0x1A1,0x30) # DVD4.1, DVD4.2 all bypass --> FPGA: 400MHz/1 = 400MHz #OK with LVDS_25 DIFF_TERM
#
#pgu.clkd_reg_write_b8(0x142,0x44) # OUT8 drive current up 0x40(1.75mA) --> 0x42(3.5mA) --> 0x44(5.25mA) --> 0x46(7mA)
#pgu.clkd_reg_write_b8(0x142,0x42) # OUT8 drive current up 0x40(1.75mA) --> 0x42(3.5mA) --> 0x44(5.25mA) --> 0x46(7mA)
pgu.clkd_reg_write_b8(0x142,0x40) # OUT8 drive current up 0x40(1.75mA) --> 0x42(3.5mA) --> 0x44(5.25mA) --> 0x46(7mA)
#
pgu.clkd_reg_write_b8(0x232,0x01) # update registers
#
#
reg_0x010 = pgu.clkd_reg_read_b8(0x010)
reg_0x1E1 = pgu.clkd_reg_read_b8(0x1E1)
reg_0x194 = pgu.clkd_reg_read_b8(0x194)
reg_0x0F3 = pgu.clkd_reg_read_b8(0x0F3)
reg_0x19C = pgu.clkd_reg_read_b8(0x19C)
reg_0x1A1 = pgu.clkd_reg_read_b8(0x1A1)
reg_0x142 = pgu.clkd_reg_read_b8(0x142)
#
print('{} = 0x{:02X}'.format('reg_0x010',reg_0x010))
print('{} = 0x{:02X}'.format('reg_0x1E1',reg_0x1E1))
print('{} = 0x{:02X}'.format('reg_0x194',reg_0x194))
print('{} = 0x{:02X}'.format('reg_0x0F3',reg_0x0F3))
print('{} = 0x{:02X}'.format('reg_0x19C',reg_0x19C))
print('{} = 0x{:02X}'.format('reg_0x1A1',reg_0x1A1))
print('{} = 0x{:02X}'.format('reg_0x142',reg_0x142))


####
pgu.sleep(1)
####


##--------------------------------------------------##


## test DACX SPI frame for AD9783
print('> DACX test')
pgu.dacx_init()
#
# test write 
#
# pulse path 0 : full scale 20mA @ 0x0200    
#pgu.dac0_reg_write_b8(0x0F,0x00)
#pgu.dac0_reg_write_b8(0x10,0x02)
# pulse path 0 : full scale 23.0mA  @ 0x0244 
#pgu.dac0_reg_write_b8(0x0F,0x44)
#pgu.dac0_reg_write_b8(0x10,0x02)
# pulse path 0 : full scale 24.2mA  @ 0x0263
#pgu.dac0_reg_write_b8(0x0F,0x63)
#pgu.dac0_reg_write_b8(0x10,0x02)
#
# // TODO: DAC full scale current setting 
# pulse path 0 : full scale 25.6mA  @ 0x0290 <<<<<< 21.2V / 18.16ns = 1167.40088 V/us // best with 14V supply
#pgu.dac0_reg_write_b8(0x0F,0x90)
#pgu.dac0_reg_write_b8(0x10,0x02)
#pgu.dac0_reg_write_b8(0x0B,0x90)
#pgu.dac0_reg_write_b8(0x0C,0x02)
##
#pgu.dac1_reg_write_b8(0x0F,0x90)
#pgu.dac1_reg_write_b8(0x10,0x02)
#pgu.dac1_reg_write_b8(0x0B,0x90)
#pgu.dac1_reg_write_b8(0x0C,0x02)
#
# pulse path 0 : full scale 26.25 mA  @ 0x02A0 <<<<<< 19.6V / 14.62ns // best with 13V supply
#pgu.dac0_reg_write_b8(0x0F,0xA0)
#pgu.dac0_reg_write_b8(0x10,0x02)
#pgu.dac0_reg_write_b8(0x0B,0xA0)
#pgu.dac0_reg_write_b8(0x0C,0x02)
##
#pgu.dac1_reg_write_b8(0x0F,0xA0)
#pgu.dac1_reg_write_b8(0x10,0x02)
#pgu.dac1_reg_write_b8(0x0B,0xA0)
#pgu.dac1_reg_write_b8(0x0C,0x02)
#
# pulse path 0 : full scale  mA  @ 0x02B0 <<<<<< 19.6V / 15.16ns
#pgu.dac0_reg_write_b8(0x0F,0xB0)
#pgu.dac0_reg_write_b8(0x10,0x02)
#
# pulse path  : full scale 28.1mA  @ 0x02D0 <<<<<< 21.6V / 13.5ns = 1600 V/us // best with 14V supply
pgu.dac0_reg_write_b8(0x0F,0xD0)
pgu.dac0_reg_write_b8(0x10,0x02)
pgu.dac0_reg_write_b8(0x0B,0xD0)
pgu.dac0_reg_write_b8(0x0C,0x02)
#
pgu.dac1_reg_write_b8(0x0F,0xD0)
pgu.dac1_reg_write_b8(0x10,0x02)
pgu.dac1_reg_write_b8(0x0B,0xD0)
pgu.dac1_reg_write_b8(0x0C,0x02)
#
# pulse path 0 : full scale 28.75mA  @ 0x02E0 <<<
#pgu.dac0_reg_write_b8(0x0F,0xE0)
#pgu.dac0_reg_write_b8(0x10,0x02)
# pulse path 0 : full scale 29.4mA  @ 0x02F0
#pgu.dac0_reg_write_b8(0x0F,0xF0)
#pgu.dac0_reg_write_b8(0x10,0x02)
# pulse path 0 : full scale 30.0mA  @ 0x0300
#pgu.dac0_reg_write_b8(0x0F,0x00)
#pgu.dac0_reg_write_b8(0x10,0x03)
#pgu.dac0_reg_write_b8(0x0B,0x00)
#pgu.dac0_reg_write_b8(0x0C,0x03)
##
#pgu.dac1_reg_write_b8(0x0F,0x00)
#pgu.dac1_reg_write_b8(0x10,0x03)
#pgu.dac1_reg_write_b8(0x0B,0x00)
#pgu.dac1_reg_write_b8(0x0C,0x03)
#
# pulse path 0 : full scale 31.66mA @ 0x03FF
#pgu.dac0_reg_write_b8(0x0F,0xFF)
#pgu.dac0_reg_write_b8(0x10,0x03)
#
#
# offset DAC : 0x200 1.00mA, AUX2N active[7] (1) , sink current[6] (1)
# offset DAC : 0x100 0.50mA, AUX2N active[7] (1) , sink current[6] (1)
# offset DAC : 0x080 0.25mA, AUX2N active[7] (1) , sink current[6] (1)
# offset DAC : 0x000 0.00mA, AUX2N active[7] (1) , sink current[6] (1)
#
# offset DAC : 0x100 0.50mA, AUX2N active[7] (1) , sink current[6] (1) <<< offset -12mV
#pgu.dac0_reg_write_b8(0x11,0x00)
#pgu.dac0_reg_write_b8(0x12,0xC1)
#pgu.dac0_reg_write_b8(0x0D,0x00)
#pgu.dac0_reg_write_b8(0x0E,0xC1)
##
#pgu.dac1_reg_write_b8(0x11,0x00)
#pgu.dac1_reg_write_b8(0x12,0xC1)
#pgu.dac1_reg_write_b8(0x0D,0x00)
#pgu.dac1_reg_write_b8(0x0E,0xC1)
#
# offset DAC : 0x1A0 0.8125mA, AUX2N active[7] (1) , sink current[6] (1) <<< offset 14mV
#pgu.dac0_reg_write_b8(0x11,0xA0)
#pgu.dac0_reg_write_b8(0x12,0xC1)
#pgu.dac0_reg_write_b8(0x0D,0xA0)
#pgu.dac0_reg_write_b8(0x0E,0xC1)
##
#pgu.dac1_reg_write_b8(0x11,0xA0)
#pgu.dac1_reg_write_b8(0x12,0xC1)
#pgu.dac1_reg_write_b8(0x0D,0xA0)
#pgu.dac1_reg_write_b8(0x0E,0xC1)
#
# offset DAC : 0x180 0.75mA, AUX2N active[7] (1) , sink current[6] (1) <<< offset 11.4mV
#pgu.dac0_reg_write_b8(0x11,0x80)
#pgu.dac0_reg_write_b8(0x12,0xC1)
#pgu.dac0_reg_write_b8(0x0D,0x80)
#pgu.dac0_reg_write_b8(0x0E,0xC1)
##
#pgu.dac1_reg_write_b8(0x11,0x80)
#pgu.dac1_reg_write_b8(0x12,0xC1)
#pgu.dac1_reg_write_b8(0x0D,0x80)
#pgu.dac1_reg_write_b8(0x0E,0xC1)
#
# offset DAC : 0x160 0.6875mA, AUX2N active[7] (1) , sink current[6] (1) <<< offset 6.9mV
#pgu.dac0_reg_write_b8(0x11,0x60)
#pgu.dac0_reg_write_b8(0x12,0xC1)
#pgu.dac0_reg_write_b8(0x0D,0x60)
#pgu.dac0_reg_write_b8(0x0E,0xC1)
##
#pgu.dac1_reg_write_b8(0x11,0x60)
#pgu.dac1_reg_write_b8(0x12,0xC1)
#pgu.dac1_reg_write_b8(0x0D,0x60)
#pgu.dac1_reg_write_b8(0x0E,0xC1)
#
# offset DAC : 0x140 0.625mA, AUX2N active[7] (1) , sink current[6] (1) <<< offset 1.81mV
pgu.dac0_reg_write_b8(0x11,0x40)
pgu.dac0_reg_write_b8(0x12,0xC1)
pgu.dac0_reg_write_b8(0x0D,0x40)
pgu.dac0_reg_write_b8(0x0E,0xC1)
#
pgu.dac1_reg_write_b8(0x11,0x40)
pgu.dac1_reg_write_b8(0x12,0xC1)
pgu.dac1_reg_write_b8(0x0D,0x40)
pgu.dac1_reg_write_b8(0x0E,0xC1)
#
#
reg_0x0A = pgu.dac0_reg_read_b8(0x0A)
reg_0x0B = pgu.dac0_reg_read_b8(0x0B)
reg_0x0C = pgu.dac0_reg_read_b8(0x0C)
reg_0x0D = pgu.dac0_reg_read_b8(0x0D)
reg_0x0E = pgu.dac0_reg_read_b8(0x0E)
reg_0x0F = pgu.dac0_reg_read_b8(0x0F)
reg_0x10 = pgu.dac0_reg_read_b8(0x10)
reg_0x11 = pgu.dac0_reg_read_b8(0x11)
reg_0x12 = pgu.dac0_reg_read_b8(0x12)
#
print('>> DAC0 reg:')
print('{} = 0x{:02X}'.format('reg_0x0A',reg_0x0A))
print('{} = 0x{:02X}'.format('reg_0x0B',reg_0x0B))
print('{} = 0x{:02X}'.format('reg_0x0C',reg_0x0C))
print('{} = 0x{:02X}'.format('reg_0x0D',reg_0x0D))
print('{} = 0x{:02X}'.format('reg_0x0E',reg_0x0E))
print('{} = 0x{:02X}'.format('reg_0x0F',reg_0x0F))
print('{} = 0x{:02X}'.format('reg_0x10',reg_0x10))
print('{} = 0x{:02X}'.format('reg_0x11',reg_0x11))
print('{} = 0x{:02X}'.format('reg_0x12',reg_0x12))
#
reg_0x0A = pgu.dac1_reg_read_b8(0x0A)
reg_0x0B = pgu.dac1_reg_read_b8(0x0B)
reg_0x0C = pgu.dac1_reg_read_b8(0x0C)
reg_0x0D = pgu.dac1_reg_read_b8(0x0D)
reg_0x0E = pgu.dac1_reg_read_b8(0x0E)
reg_0x0F = pgu.dac1_reg_read_b8(0x0F)
reg_0x10 = pgu.dac1_reg_read_b8(0x10)
reg_0x11 = pgu.dac1_reg_read_b8(0x11)
reg_0x12 = pgu.dac1_reg_read_b8(0x12)
#
print('>> DAC1 reg:')
print('{} = 0x{:02X}'.format('reg_0x0A',reg_0x0A))
print('{} = 0x{:02X}'.format('reg_0x0B',reg_0x0B))
print('{} = 0x{:02X}'.format('reg_0x0C',reg_0x0C))
print('{} = 0x{:02X}'.format('reg_0x0D',reg_0x0D))
print('{} = 0x{:02X}'.format('reg_0x0E',reg_0x0E))
print('{} = 0x{:02X}'.format('reg_0x0F',reg_0x0F))
print('{} = 0x{:02X}'.format('reg_0x10',reg_0x10))
print('{} = 0x{:02X}'.format('reg_0x11',reg_0x11))
print('{} = 0x{:02X}'.format('reg_0x12',reg_0x12))
#

# // TODO: PLL LOCK
# check lock detection
ret = pgu.read_TEST_IO_MON()
print(ret)
# 0x6000_0000 readback 
#assign w_TEST_IO_MON[28:27] =  2'b0;
#assign w_TEST_IO_MON[26] = dac1_dco_clk_locked;
#assign w_TEST_IO_MON[25] = dac0_dco_clk_locked;
#assign w_TEST_IO_MON[24] = clk_dac_locked;
#assign w_TEST_IO_MON[23:20] =  4'b0;
#assign w_TEST_IO_MON[19] = clk4_locked;
#assign w_TEST_IO_MON[18] = clk3_locked;
#assign w_TEST_IO_MON[17] = clk2_locked;
#assign w_TEST_IO_MON[16] = clk1_locked;
#assign w_TEST_IO_MON[15: 0] = 16'b0;
#

# reset FPGA pll
pgu.dacx_fpga_pll_rst(clkd_out_rst=1, dac0_dco_rst=1, dac1_dco_rst=1)

# re-check lock detection
ret = pgu.read_TEST_IO_MON()
print(ret)

# run FPGA pll
pgu.dacx_fpga_pll_rst(clkd_out_rst=0, dac0_dco_rst=0, dac1_dco_rst=0)

# re-check lock detection
ret = pgu.read_TEST_IO_MON()
print(ret)


## test sequence
## MLS (multi-level sequence) : 4 seq 
#
# 2's comp: 
# 0x7FFF = 32767
# 0x6FFF
# 0x5FFF
# 0x4FFF
# 0x3FFF
# 0x2FFF
# 0x1FFF
# 0x0FFF = 4095
# 0x0000 = 0
# 0xF000 = -4096
# 0xE000
# 0xD000
# 0xC000
# 0xB000
# 0xA000
# 0x9000
# 0x8000 = -32768
#
#
# // TODO: TEST sequence levels
# Vpp 3V
#dac0_level_seq0 = 0x0FFF
#dac0_level_seq1 = 0xF000
#dac0_level_seq2 = 0x0FFF
#dac0_level_seq3 = 0xF000
#
#dac1_level_seq0 = 0x0FFF
#dac1_level_seq1 = 0xF000
#dac1_level_seq2 = 0x0FFF
#dac1_level_seq3 = 0xF000
#
# Vpp 20V
#dac0_level_seq0 = 0x7FFF
#dac0_level_seq1 = 0x8000
#dac0_level_seq2 = 0x7FFF
#dac0_level_seq3 = 0x8000
#dac0_level_seq4 = 0x7FFF
#dac0_level_seq5 = 0x8000
#dac0_level_seq6 = 0x7FFF
#dac0_level_seq7 = 0x8000
#
dac0_level_seq0 = 0x03FF #// OK .. NG 03FF FC00 level NG ... bit line out??
dac0_level_seq1 = 0x07FF
dac0_level_seq2 = 0x03FF
dac0_level_seq3 = 0x0000
dac0_level_seq4 = 0xFC00
dac0_level_seq5 = 0xF800
dac0_level_seq6 = 0xFC00
dac0_level_seq7 = 0x0000
#
#dac1_level_seq0 = 0x1FFF #// NG positive supply off ...
#dac1_level_seq1 = 0x3FFF 
#dac1_level_seq2 = 0x1FFF
#dac1_level_seq3 = 0x0000
#dac1_level_seq4 = 0xE000
#dac1_level_seq5 = 0xC000
#dac1_level_seq6 = 0xE000
#dac1_level_seq7 = 0x0000
#
#dac1_level_seq0 = 0x7FFF
#dac1_level_seq1 = 0x8000
#dac1_level_seq2 = 0x3FFF
#dac1_level_seq3 = 0xC000
#dac1_level_seq4 = 0x7FFF
#dac1_level_seq5 = 0x8000
#dac1_level_seq6 = 0x3FFF
#dac1_level_seq7 = 0xC000
#
#dac1_level_seq0 = 0x3FFF #// NG .. starting OK ... but finally negative off...
#dac1_level_seq1 = 0x7FFF
#dac1_level_seq2 = 0x7FFF
#dac1_level_seq3 = 0x3FFF
#dac1_level_seq4 = 0xC000
#dac1_level_seq5 = 0x8000
#dac1_level_seq6 = 0x8000
#dac1_level_seq7 = 0xC000
#
#dac1_level_seq0 = 0x3FFF #// OK 
#dac1_level_seq1 = 0x7FFF
#dac1_level_seq2 = 0x3FFF
#dac1_level_seq3 = 0x0000
#dac1_level_seq4 = 0xC000
#dac1_level_seq5 = 0x8000
#dac1_level_seq6 = 0xC000
#dac1_level_seq7 = 0x0000
#
#dac1_level_seq0 = 0x0FFF #// OK 
#dac1_level_seq1 = 0x1FFF
#dac1_level_seq2 = 0x0FFF
#dac1_level_seq3 = 0x0000
#dac1_level_seq4 = 0xF000
#dac1_level_seq5 = 0xE000
#dac1_level_seq6 = 0xF000
#dac1_level_seq7 = 0x0000
#
dac1_level_seq0 = 0x03FF #// OK
dac1_level_seq1 = 0x07FF
dac1_level_seq2 = 0x03FF
dac1_level_seq3 = 0x0000
dac1_level_seq4 = 0xFC00
dac1_level_seq5 = 0xF800
dac1_level_seq6 = 0xFC00
dac1_level_seq7 = 0x0000
#
#dac1_level_seq0 = 0x07FF #// NG with 200MHz
#dac1_level_seq1 = 0xF800
#dac1_level_seq2 = 0x0000
#dac1_level_seq3 = 0x07FF
#dac1_level_seq4 = 0x07FF
#dac1_level_seq5 = 0xF800
#dac1_level_seq6 = 0xF800
#dac1_level_seq7 = 0x0000
#
#dac1_level_seq0 = 0x07FF #// 100MHz signaling # NG
#dac1_level_seq1 = 0xF800
#dac1_level_seq2 = 0x07FF
#dac1_level_seq3 = 0xF800
#dac1_level_seq4 = 0x07FF
#dac1_level_seq5 = 0xF800
#dac1_level_seq6 = 0x07FF
#dac1_level_seq7 = 0xF800
#
#dac1_level_seq0 = 0x07FF #// 50MHz signaling # NG 
#dac1_level_seq1 = 0x07FF
#dac1_level_seq2 = 0xF800
#dac1_level_seq3 = 0xF800
#dac1_level_seq4 = 0x07FF
#dac1_level_seq5 = 0x07FF
#dac1_level_seq6 = 0xF800
#dac1_level_seq7 = 0xF800
#
#dac1_level_seq0 = 0x07FF #//  25MHz signaling
#dac1_level_seq1 = 0x07FF
#dac1_level_seq2 = 0x07FF
#dac1_level_seq3 = 0x07FF
#dac1_level_seq4 = 0xF800
#dac1_level_seq5 = 0xF800
#dac1_level_seq6 = 0xF800
#dac1_level_seq7 = 0xF800
#
#dac1_level_seq0 = 0x0000
#dac1_level_seq1 = 0x0000
#dac1_level_seq2 = 0x0000
#dac1_level_seq3 = 0x0000
#dac1_level_seq4 = 0x0000
#dac1_level_seq5 = 0x0000
#dac1_level_seq6 = 0x0000
#dac1_level_seq7 = 0x0000
#
#
# set  level0
pgu.dacx_mls_write_adrs(0)
mls_adrs = pgu.dacx_mls_read_adrs()
print('{} = {}'.format('mls_adrs',mls_adrs))#
pgu.dacx_mls_write_data(dac0_val_b16=dac0_level_seq0, dac1_val_b16=dac1_level_seq0)
[rb_dac0_val_b16, rb_dac1_val_b16]=pgu.dacx_mls_read_data()
print('{} = {}'.format('rb_dac0_val_b16',rb_dac0_val_b16))#
print('{} = {}'.format('rb_dac1_val_b16',rb_dac1_val_b16))#
# set  level1
pgu.dacx_mls_write_adrs(1)
pgu.dacx_mls_write_data(dac0_val_b16=dac0_level_seq1, dac1_val_b16=dac1_level_seq1)
# set  level2
pgu.dacx_mls_write_adrs(2)
pgu.dacx_mls_write_data(dac0_val_b16=dac0_level_seq2, dac1_val_b16=dac1_level_seq2)
# set  level3
pgu.dacx_mls_write_adrs(3)
pgu.dacx_mls_write_data(dac0_val_b16=dac0_level_seq3, dac1_val_b16=dac1_level_seq3)
#
# set  level4
pgu.dacx_mls_write_adrs(4)
pgu.dacx_mls_write_data(dac0_val_b16=dac0_level_seq4, dac1_val_b16=dac1_level_seq4)
# set  level5
pgu.dacx_mls_write_adrs(5)
pgu.dacx_mls_write_data(dac0_val_b16=dac0_level_seq5, dac1_val_b16=dac1_level_seq5)
# set  level6
pgu.dacx_mls_write_adrs(6)
pgu.dacx_mls_write_data(dac0_val_b16=dac0_level_seq6, dac1_val_b16=dac1_level_seq6)
# set  level7
pgu.dacx_mls_write_adrs(7)
pgu.dacx_mls_write_data(dac0_val_b16=dac0_level_seq7, dac1_val_b16=dac1_level_seq7)

#
input('>>> Press Enter to Next')
# run test 
pgu.dacx_mls_run_test()
#
input('>>> Press Enter to Stop')
# stop test
pgu.dacx_mls_stop_test()
#

##--------------------------------------------------##


####
## power control off
#
print('>> power off')
pgu.spio_ext__pwr_led(led=0,pwr_dac=0,pwr_adc=0,pwr_amp=0)
#

####
pgu.sleep(3)
####

####
## test counter off
ret = pgu.test_counter('OFF')
print(ret)
####

####
## test counter reset
ret = pgu.test_counter('RESET')
print(ret)
####


####
## close
ret = pgu.ok_close()
print(ret)
####
