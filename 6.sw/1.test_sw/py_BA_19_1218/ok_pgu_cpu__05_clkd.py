## ok module for python 3.5
## works both with x64 and win32
## 
##   Windows DLL Usage
##   http://www.opalkelly.com:8090/display/FPSDK/Programming+Languages
##   http://www.microsoft.com/en-us/download/details.aspx?id=40784
##
## https://www.opalkelly.com/examples/home/
## https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel.html
## http://www.opalkelly.com:8090/display/FPSDK/Getting+Started
## wxPython 2.8 http://www.wxpython.org
#
## ok_pgu_cpu__05_clkd.py : test code for CLKD AD9516-1
#  

####
## controls
#  
#  
FPGA_CONFIGURE = 0
#
#BIT_FILENAME = ''; 
#BIT_FILENAME = '../img_B3_19_1114/xem7310__pgu_cpu__top.bit'
#
####

####
## library call
import ok_pgu_cpu__lib as pgu
#
# display board conf data in library  
ret = pgu.display_conf_header()
print(ret)
#
# set bit filename including path 
BIT_FILENAME = pgu.conf.OK_EP_ADRS_CONFIG['bit_filename']
#
####

####
## init : dev
dev = pgu.ok_init()
print(dev)
####

####
ret = pgu.ok_caller_id()
print(ret)
####

####
## open
FPGA_SERIAL = '' # for any
#FPGA_SERIAL = '1739000J7V' # for module: CMU-CPU-F5500-FPGA1
#FPGA_SERIAL = '1908000OVP' # for module: CMU-CPU-F5500-FPGA2
#FPGA_SERIAL = '1739000J8A' # for module: CMU-CPU-F5500-FPGA3
#FPGA_SERIAL = '1739000J63' # for module: CMU-CPU-F5500-FPGA4
#FPGA_SERIAL = '1739000J8I' # for module: CMU-CPU-F5500-FPGA5
#
ret = pgu.ok_open(FPGA_SERIAL)
print(ret)
####


####
## FPGA_CONFIGURE
if FPGA_CONFIGURE==1: 
	ret = pgu.ok_conf(BIT_FILENAME)
	print(ret)
	print('Downloading bit file makes your debugger be reset!!')
####  

####
## read fpga_image_id
fpga_image_id__str = pgu.read_fpga_image_id()
print(fpga_image_id__str)
####

####
## read FPGA internal temp and volt
ret = pgu.monitor_fpga()
print(ret)
####

####
## test counter on 
ret = pgu.test_counter('ON')
print(ret)
####

####
## power control on : LED, PWR_DAC
#
pgu.spio_ext__pwr_led(led=1,pwr_dac=1,pwr_adc=0,pwr_amp=0)
####

####
pgu.sleep(1)
####


##--------------------------------------------------##

pgu.clkd_init()
#pgu.clkd_reg_read_b8(0x000)
#pgu.clkd_reg_write_b8(0x000,0x99) # SDO used for 4-wire SPI

#pgu.clkd_init()
pgu.clkd_reg_read_b8(0x000) # readback 0x18
#pgu.clkd_reg_read_b8(0x00F) # readback 0x18
#pgu.clkd_reg_read_b8(0x003) # read IC 0x41
#pgu.clkd_reg_read_b8(0x03F) # readback 041

pgu.clkd_reg_read_b8(0x003) # read IC 0x41 
#pgu.clkd_reg_read_b8(0x10F) # readback 18
#pgu.clkd_reg_write_b8(0x000,0x99) # SDO used for 4-wire SPI

pgu.clkd_reg_read_b8(0x03F) # readback 00
#pgu.clkd_reg_read_b8(0x16F) # readback 18
#pgu.clkd_reg_read_b8(0x16F) # readback 18
#pgu.clkd_reg_read_b8(0x003) # read IC 0x41
#pgu.clkd_reg_read_b8(0x232)
#pgu.clkd_reg_write_b8(0x232,0x01) # update registers

## CLKD reg check 
reg_0x000 = pgu.clkd_reg_read_b8(0x000)
reg_0x003 = pgu.clkd_reg_read_b8(0x003)
reg_0x010 = pgu.clkd_reg_read_b8(0x010)
reg_0x016 = pgu.clkd_reg_read_b8(0x016)
reg_0x017 = pgu.clkd_reg_read_b8(0x017)
reg_0x01A = pgu.clkd_reg_read_b8(0x01A)
reg_0x01B = pgu.clkd_reg_read_b8(0x01B)
reg_0x0F2 = pgu.clkd_reg_read_b8(0x0F2)
reg_0x0F3 = pgu.clkd_reg_read_b8(0x0F3)
reg_0x140 = pgu.clkd_reg_read_b8(0x140)
reg_0x142 = pgu.clkd_reg_read_b8(0x142)
reg_0x193 = pgu.clkd_reg_read_b8(0x193)
reg_0x194 = pgu.clkd_reg_read_b8(0x194)
reg_0x195 = pgu.clkd_reg_read_b8(0x195)
reg_0x199 = pgu.clkd_reg_read_b8(0x199)
reg_0x19B = pgu.clkd_reg_read_b8(0x19B)
reg_0x19C = pgu.clkd_reg_read_b8(0x19C)
reg_0x19E = pgu.clkd_reg_read_b8(0x19E)
reg_0x19F = pgu.clkd_reg_read_b8(0x19F)
reg_0x1A0 = pgu.clkd_reg_read_b8(0x1A0)
reg_0x1A1 = pgu.clkd_reg_read_b8(0x1A1)
reg_0x1A2 = pgu.clkd_reg_read_b8(0x1A2)
reg_0x1E1 = pgu.clkd_reg_read_b8(0x1E1)
reg_0x232 = pgu.clkd_reg_read_b8(0x232)
#
print('{} = 0x{:02X}'.format('reg_0x000',reg_0x000))
print('{} = 0x{:02X}'.format('reg_0x003',reg_0x003))
print('{} = 0x{:02X}'.format('reg_0x010',reg_0x010))
print('{} = 0x{:02X}'.format('reg_0x016',reg_0x016))
print('{} = 0x{:02X}'.format('reg_0x017',reg_0x017))
print('{} = 0x{:02X}'.format('reg_0x01A',reg_0x01A))
print('{} = 0x{:02X}'.format('reg_0x01B',reg_0x01B))
print('{} = 0x{:02X}'.format('reg_0x0F2',reg_0x0F2))
print('{} = 0x{:02X}'.format('reg_0x0F3',reg_0x0F3))
print('{} = 0x{:02X}'.format('reg_0x140',reg_0x140))
print('{} = 0x{:02X}'.format('reg_0x142',reg_0x142))
print('{} = 0x{:02X}'.format('reg_0x193',reg_0x193))
print('{} = 0x{:02X}'.format('reg_0x194',reg_0x194))
print('{} = 0x{:02X}'.format('reg_0x195',reg_0x195))
print('{} = 0x{:02X}'.format('reg_0x199',reg_0x199))
print('{} = 0x{:02X}'.format('reg_0x19B',reg_0x19B))
print('{} = 0x{:02X}'.format('reg_0x19C',reg_0x19C))
print('{} = 0x{:02X}'.format('reg_0x19E',reg_0x19E))
print('{} = 0x{:02X}'.format('reg_0x19F',reg_0x19F))
print('{} = 0x{:02X}'.format('reg_0x1A0',reg_0x1A0))
print('{} = 0x{:02X}'.format('reg_0x1A1',reg_0x1A1))
print('{} = 0x{:02X}'.format('reg_0x1A2',reg_0x1A2))
print('{} = 0x{:02X}'.format('reg_0x1E1',reg_0x1E1))
print('{} = 0x{:02X}'.format('reg_0x232',reg_0x232))

##  Settings for Clock Distribution : 
#reg_0x010 = pgu.clkd_reg_read_b8(0x010)
#reg_0x1E1 = pgu.clkd_reg_read_b8(0x1E1)
#pgu.clkd_reg_write_b8(0x010,(reg_0x010&0xFC)+0x01) # PLL power-down
#pgu.clkd_reg_write_b8(0x1E1,(reg_0x1E1&0xFC)+0x01) # Bypass VCO divider
#reg_0x010 = pgu.clkd_reg_read_b8(0x010)
#reg_0x1E1 = pgu.clkd_reg_read_b8(0x1E1)
#print('{} = 0x{:02X}'.format('reg_0x010',reg_0x010))
#print('{} = 0x{:02X}'.format('reg_0x1E1',reg_0x1E1))
#
pgu.clkd_reg_write_b8(0x010,0x7D) # PLL power-down
#
pgu.clkd_reg_write_b8(0x1E1,0x01) # Bypass VCO divider
#
#pgu.clkd_reg_write_b8(0x193,0xBB) # DVD1 div 2+11+11=24 --> DACx: 400MHz/24 = 16.7MHz
#pgu.clkd_reg_write_b8(0x193,0x00) # DVD1 div 2+0+0=2 --> DACx: 400MHz/2 = 200MHz
#pgu.clkd_reg_write_b8(0x193,0x33) # DVD1 div 2+3+3=8 --> DACx: 400MHz/8 = 50MHz
#
pgu.clkd_reg_write_b8(0x194,0x80) # DVD1 bypass --> DACx: 400MHz/1 = 400MHz
#
pgu.clkd_reg_write_b8(0x0F3,0x08) # enable path for DAC1
#
#pgu.clkd_reg_write_b8(0x199,0x22) # DVD3.1 div 2+2+2=6 
#pgu.clkd_reg_write_b8(0x19B,0x11) # DVD3.2 div 2+1+1=4  --> REFo: 400MHz/24 = 16.7MHz
#
#pgu.clkd_reg_write_b8(0x199,0x00) # DVD3.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x19B,0x00) # DVD3.2 div 2+0+0=2  --> REFo: 400MHz/4 = 100MHz
#
#pgu.clkd_reg_write_b8(0x199,0x00) # DVD3.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x19B,0x11) # DVD3.2 div 2+1+1=4  --> REFo: 400MHz/8 = 50MHz
#
pgu.clkd_reg_write_b8(0x19C,0x30) # DVD3.1, DVD3.2 all bypass --> REFo: 400MHz/1 = 400MHz
#
#pgu.clkd_reg_write_b8(0x19E,0x22) # DVD4.1 div 2+2+2=6 
#pgu.clkd_reg_write_b8(0x1A0,0x11) # DVD4.2 div 2+1+1=4  --> FPGA: 400MHz/24 = 16.7MHz
#
#pgu.clkd_reg_write_b8(0x19E,0x00) # DVD4.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x1A0,0x00) # DVD4.2 div 2+0+0=2  --> FPGA: 400MHz/4 = 100MHz
#
#pgu.clkd_reg_write_b8(0x19E,0x00) # DVD4.1 div 2+0+0=2 
#pgu.clkd_reg_write_b8(0x1A0,0x11) # DVD4.2 div 2+1+1=4  --> FPGA: 400MHz/8 = 50MHz
#
pgu.clkd_reg_write_b8(0x1A1,0x30) # DVD4.1, DVD4.2 all bypass --> FPGA: 400MHz/1 = 400MHz
#
pgu.clkd_reg_write_b8(0x142,0x44) # OUT8 drive current up 0x42(3.5mA) --> 0x44(5.25mA) --> 0x46(7mA)
#
pgu.clkd_reg_write_b8(0x232,0x01) # update registers
#
#
reg_0x010 = pgu.clkd_reg_read_b8(0x010)
reg_0x1E1 = pgu.clkd_reg_read_b8(0x1E1)
reg_0x194 = pgu.clkd_reg_read_b8(0x194)
reg_0x0F3 = pgu.clkd_reg_read_b8(0x0F3)
reg_0x19C = pgu.clkd_reg_read_b8(0x19C)
reg_0x1A1 = pgu.clkd_reg_read_b8(0x1A1)
reg_0x142 = pgu.clkd_reg_read_b8(0x142)
#
print('{} = 0x{:02X}'.format('reg_0x010',reg_0x010))
print('{} = 0x{:02X}'.format('reg_0x1E1',reg_0x1E1))
print('{} = 0x{:02X}'.format('reg_0x194',reg_0x194))
print('{} = 0x{:02X}'.format('reg_0x0F3',reg_0x0F3))
print('{} = 0x{:02X}'.format('reg_0x19C',reg_0x19C))
print('{} = 0x{:02X}'.format('reg_0x1A1',reg_0x1A1))
print('{} = 0x{:02X}'.format('reg_0x142',reg_0x142))

## setting for VCO fixed with div 6 # 2.5GHz/6= 416.666667 MHz // ...not work // later
#pgu.clkd_reg_write_b8(0x010,0x7C) 
#pgu.clkd_reg_write_b8(0x232,0x01) # update registers
#pgu.clkd_reg_write_b8(0x016,0x86) 
#pgu.clkd_reg_write_b8(0x1E0,0x06) # VCO div 6
#pgu.clkd_reg_write_b8(0x193,0xBB) # DVD1 div 2+11+11=24
#pgu.clkd_reg_write_b8(0x1E1,0x02) 
#pgu.clkd_reg_write_b8(0x232,0x01) # update registers
##
#reg_0x010 = pgu.clkd_reg_read_b8(0x010)
#reg_0x016 = pgu.clkd_reg_read_b8(0x016)
#reg_0x1E0 = pgu.clkd_reg_read_b8(0x1E0)
#reg_0x1E1 = pgu.clkd_reg_read_b8(0x1E1)
#print('{} = 0x{:02X}'.format('reg_0x010',reg_0x010))
#print('{} = 0x{:02X}'.format('reg_0x016',reg_0x016))
#print('{} = 0x{:02X}'.format('reg_0x1E0',reg_0x1E0))
#print('{} = 0x{:02X}'.format('reg_0x1E1',reg_0x1E1))


## test CLKD SPI frame 
input('>>> Press Enter to stop!')

##--------------------------------------------------##


####
## power control off
#
pgu.spio_ext__pwr_led(led=0,pwr_dac=0,pwr_adc=0,pwr_amp=0)
#

####
pgu.sleep(3)
####

####
## test counter off
ret = pgu.test_counter('OFF')
print(ret)
####

####
## test counter reset
ret = pgu.test_counter('RESET')
print(ret)
####


####
## close
ret = pgu.ok_close()
print(ret)
####
