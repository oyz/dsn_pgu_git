#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "microblaze_sleep.h" // for usleep

#include "../../Ethernet/socket.h"
#include "scpi.h"

//$$ support PGU-CPU 
#include "../../../pgu_cpu_config.h" // PGU-CPU board config
#include "../../../pgu_cpu.h"        // for hw_reset and hw device access

// MCS EP control for PGU 
#define  MCS_EP_BASE  ADRS_BASE_PGU 

// scpi parameters / data //{
//uint8_t* cmd_str__XXX = (uint8_t*)"XXX";
uint8_t* cmd_str__IDN       = (uint8_t*)"*IDN?\n";
uint8_t* cmd_str__RST       = (uint8_t*)"*RST\n";
//
uint8_t* cmd_str__PGEP_EN   = (uint8_t*)":PGEP:EN"; 
uint8_t* cmd_str__PGEP_MKWI = (uint8_t*)":PGEP:MKWI";
uint8_t* cmd_str__PGEP_MKWO = (uint8_t*)":PGEP:MKWO";
uint8_t* cmd_str__PGEP_MKTI = (uint8_t*)":PGEP:MKTI";
uint8_t* cmd_str__PGEP_MKTO = (uint8_t*)":PGEP:MKTO";
uint8_t* cmd_str__PGEP_WI   = (uint8_t*)":PGEP:WI";
uint8_t* cmd_str__PGEP_WO   = (uint8_t*)":PGEP:WO";
uint8_t* cmd_str__PGEP_TI   = (uint8_t*)":PGEP:TI";
uint8_t* cmd_str__PGEP_TO   = (uint8_t*)":PGEP:TO";
uint8_t* cmd_str__PGEP_TAC  = (uint8_t*)":PGEP:TAC";
uint8_t* cmd_str__PGEP_TMO  = (uint8_t*)":PGEP:TMO";
uint8_t* cmd_str__PGEP_PI   = (uint8_t*)":PGEP:PI";
uint8_t* cmd_str__PGEP_PO   = (uint8_t*)":PGEP:PO";
uint8_t* cmd_str__PGEP_WMI  = (uint8_t*)":PGEP:WMI";
uint8_t* cmd_str__PGEP_WMO  = (uint8_t*)":PGEP:WMO";
//
uint8_t* cmd_str__PGU_PWR            = (uint8_t*)":PGU:PWR";
uint8_t* cmd_str__PGU_OUTP           = (uint8_t*)":PGU:OUTP";
uint8_t* cmd_str__PGU_DCS_TRIG       = (uint8_t*)":PGU:DCS:TRIG";
uint8_t* cmd_str__PGU_DCS_DAC0_PNT   = (uint8_t*)":PGU:DCS:DAC0:PNT";
uint8_t* cmd_str__PGU_DCS_DAC1_PNT   = (uint8_t*)":PGU:DCS:DAC1:PNT";
uint8_t* cmd_str__PGU_DCS_RPT        = (uint8_t*)":PGU:DCS:RPT";
uint8_t* cmd_str__PGU_FDCS_TRIG      = (uint8_t*)":PGU:FDCS:TRIG";
uint8_t* cmd_str__PGU_FDCS_DAC0      = (uint8_t*)":PGU:FDCS:DAC0";
uint8_t* cmd_str__PGU_FDCS_DAC1      = (uint8_t*)":PGU:FDCS:DAC1";
uint8_t* cmd_str__PGU_FDCS_RPT       = (uint8_t*)":PGU:FDCS:RPT";
uint8_t* cmd_str__PGU_FREQ           = (uint8_t*)":PGU:FREQ";
uint8_t* cmd_str__PGU_OFST_DAC0      = (uint8_t*)":PGU:OFST:DAC0";
uint8_t* cmd_str__PGU_OFST_DAC1      = (uint8_t*)":PGU:OFST:DAC1";
uint8_t* cmd_str__PGU_GAIN_DAC0      = (uint8_t*)":PGU:GAIN:DAC0";
uint8_t* cmd_str__PGU_GAIN_DAC1      = (uint8_t*)":PGU:GAIN:DAC1";
//

//#define LEN_CMD_STR__XXX   3
#define LEN_CMD_STR__IDN         6 // "*IDN?\n"
#define LEN_CMD_STR__RST         5 // "*RST\n"
//
#define LEN_CMD_STR__PGEP_EN             8 // ":PGEP:EN"
#define LEN_CMD_STR__PGEP_MKWI          10 // ":PGEP:MKWI"
#define LEN_CMD_STR__PGEP_MKWO          10 // ":PGEP:MKWO"
#define LEN_CMD_STR__PGEP_MKTI          10 // ":PGEP:MKTI"
#define LEN_CMD_STR__PGEP_MKTO          10 // ":PGEP:MKTO"
#define LEN_CMD_STR__PGEP_WI             8 // ":PGEP:WI"
#define LEN_CMD_STR__PGEP_WO             8 // ":PGEP:WO"
#define LEN_CMD_STR__PGEP_TI             8 // ":PGEP:TI"
#define LEN_CMD_STR__PGEP_TO             8 // ":PGEP:TO"
#define LEN_CMD_STR__PGEP_TAC            9 // ":PGEP:TAC"
#define LEN_CMD_STR__PGEP_TMO            9 // ":PGEP:TMO"
#define LEN_CMD_STR__PGEP_PI             8 // ":PGEP:PI"
#define LEN_CMD_STR__PGEP_PO             8 // ":PGEP:PO"
#define LEN_CMD_STR__PGEP_WMI            9 // ":PGEP:WMI"
#define LEN_CMD_STR__PGEP_WMO            9 // ":PGEP:WMO"
//
#define LEN_CMD_STR__PGU_PWR             8 // ":PGU:PWR"
#define LEN_CMD_STR__PGU_OUTP            9 // ":PGU:OUTP"
#define LEN_CMD_STR__PGU_DCS_TRIG       13 // ":PGU:DCS:TRIG"
#define LEN_CMD_STR__PGU_DCS_DAC0_PNT   17 // ":PGU:DCS:DAC0:PNT"
#define LEN_CMD_STR__PGU_DCS_DAC1_PNT   17 // ":PGU:DCS:DAC1:PNT"
#define LEN_CMD_STR__PGU_DCS_RPT        12 // ":PGU:DCS:RPT"
#define LEN_CMD_STR__PGU_FDCS_TRIG      14 // ":PGU:FDCS:TRIG"
#define LEN_CMD_STR__PGU_FDCS_DAC0      14 // ":PGU:FDCS:DAC0"
#define LEN_CMD_STR__PGU_FDCS_DAC1      14 // ":PGU:FDCS:DAC1"
#define LEN_CMD_STR__PGU_FDCS_RPT       13 // ":PGU:FDCS:RPT"
#define LEN_CMD_STR__PGU_FREQ            9 // ":PGU:FREQ"
#define LEN_CMD_STR__PGU_OFST_DAC0      14 // ":PGU:OFST:DAC0"
#define LEN_CMD_STR__PGU_OFST_DAC1      14 // ":PGU:OFST:DAC1"
#define LEN_CMD_STR__PGU_GAIN_DAC0      14 // ":PGU:GAIN:DAC0"
#define LEN_CMD_STR__PGU_GAIN_DAC1      14 // ":PGU:GAIN:DAC1"
//

// https://mcuoneclipse.com/2013/04/14/text-data-and-bss-code-and-data-size-explained/

//
uint8_t* rsp_str__IDN = (uint8_t*)"PGU-CPU-F5500; SBT " __TIME__ ", " __DATE__;
//
uint8_t* rsp_str__NULL = (uint8_t*)"\0";
uint8_t* rsp_str__OK   = (uint8_t*)"OK\n";
uint8_t* rsp_str__NG   = (uint8_t*)"NG\n";
uint8_t* rsp_str__OFF  = (uint8_t*)"OFF\n";
uint8_t* rsp_str__ON   = (uint8_t*)"ON\n";
uint8_t* rsp_str__NL   = (uint8_t*)"\n"; // sentinel for numberic block

//}


// scpi subfunctions: //{

// send response all //{
int32_t send_response_all(uint8_t sn, uint8_t *p_rsp_str, int32_t size) {
	int32_t sentsize;
	int32_t ret;
	//
	if (size==0)
		return 0;
	//
	sentsize = 0;
	while(size != sentsize) {
		ret = send(sn, p_rsp_str+sentsize, size-sentsize); //$$ send
		if(ret < 0) {
			return ret;
		}
#ifdef _SCPI_DEBUG_
		printf("send size:%d , string size:%d, contents:%s \n",(int)ret,(int)(size-sentsize),(p_rsp_str+sentsize));
#endif
		sentsize += ret; // Don't care SOCKERR_BUSY, because it is zero.
	}
	return ret;
}
//}

// send_response_all_from_pipe32() //{
//   send data from pipe32 
//   new send_from_pipe32() in socket.c
//   new wiz_send_data_from_pipe32() in w5500.c
//   new WIZCHIP_WRITE_PIPE() in w5500.c
//   new write_data_pipe__wz850() in cmu_cpu.c
int32_t send_response_all_from_pipe32(uint8_t sn, uint32_t src_adrs_p32, int32_t size) {
	int32_t sentsize;
	int32_t ret;
	//
	if (size==0)
		return 0;
	//
	sentsize = 0;
	while(size != sentsize) {
		//$$ret = send(sn, p_rsp_str+sentsize, size-sentsize); //$$ send
		ret = send_from_pipe32(sn, src_adrs_p32, size-sentsize); //$$ send
		if(ret < 0) {
			return ret;
		}
#ifdef _SCPI_DEBUG_
		printf("sent size :%d , size to send:%d, prev sent size:%d \n",(int)ret,(int)(size-sentsize),(int)sentsize);
#endif
		sentsize += ret; // Don't care SOCKERR_BUSY, because it is zero.
	}
	return ret;
}
//}

//}


// SCPI server //{

// var //{	
static int8_t flag_SOCK_ESTABLISHED = 0;
static int8_t flag_get_rx = 0;
static int32_t cnt_stay_SOCK_ESTABLISHED = MAX_CNT_STAY_SOCK_ESTABLISHED;
//}

// scpi_tcps() //{
int32_t scpi_tcps(uint8_t sn, uint8_t* buf, uint16_t port) //$$
{
	// vars //{
	int32_t ret, ret2;
	uint16_t size = 0;
	//$$uint16_t sentsize=0;
	int32_t ii;
	int32_t flag__found_newline;
	//
#ifdef _SCPI_DEBUG_
	uint8_t destip[4];
	uint16_t destport;
#endif
	uint8_t sr; //$$
#ifdef _SCPI_DEBUG_WCMSG_
	uint8_t* msg_welcome = (uint8_t*)"> SCPI TCP server is established: \r\n";
#endif
	uint8_t rsp_str[128];
	uint8_t* p_rsp_str;
	//}

	switch(sr=getSn_SR(sn))
	{
		case SOCK_ESTABLISHED : //{
			
			// case of new establish //{
			if(getSn_IR(sn) & Sn_IR_CON)
			{
#ifdef _SCPI_DEBUG_
			getSn_DIPR(sn, destip);
			destport = getSn_DPORT(sn);
			//
			printf("%d:Connected - %d.%d.%d.%d : %d \n",sn, destip[0], destip[1], destip[2], destip[3], destport);
#endif
			setSn_IR(sn,Sn_IR_CON); //$$ clear establish intr.
			//
			flag_SOCK_ESTABLISHED = 1;
			flag_get_rx = 0;
			cnt_stay_SOCK_ESTABLISHED = MAX_CNT_STAY_SOCK_ESTABLISHED;
			//
#ifdef _SCPI_DEBUG_WCMSG_
			//$$ send welcome message
			size = strlen((char*)msg_welcome);
			ret = send(sn,msg_welcome,size); //$$ send welcome msg
			if(ret < 0)
			{
				close(sn);
				return ret;
			}
			//
#endif 
			}
			//}
			
			// check input buffer and process SCPI commands... //{
				
			if((size = getSn_RX_RSR(sn)) > 0) { //$$ check received data size //{
			
			// for reset counter //{
			flag_get_rx = 1;
			// cnt_stay_SOCK_ESTABLISHED = MAX_CNT_STAY_SOCK_ESTABLISHED;
			//}
			
			// see if size is too small... wait a moment ... check getSn_RX_RSR() again... //{
			if (size<5) {
#ifdef _SCPI_DEBUG_
				printf("get rx size again. size:%d \n",(int)size);
#endif
				//usleep(100); // wait for 100us
				usleep(10); // wait for 10us
				size = getSn_RX_RSR(sn);
			}
			//}
			
			// move data to buf //{
			if(size > DATA_BUF_SIZE_SCPI-1) size = DATA_BUF_SIZE_SCPI-1; //$$ a space for sentinel
			ret = recv(sn, buf, size); //$$ read socket data, and save them into buf 
			if(ret <= 0) 
				return ret;
			buf[ret] = '\0'; // add sentinel
			//}
			
			// see if buf has <NL> or end of command ... repeat recv() for a while... //{
			// 16KB buffer ... 100Mbps ... 16KB/(100Mbps) = 1.28 milliseconds
			// wait for 320us ... 4KB size 
			flag__found_newline = 0;
			ret2 = ret;
			while (1) {
				// find <NL> from rear-side
				for (ii=0;ii<ret2;ii++) {
					if (buf[ret-1-ii] == '\n') {
						flag__found_newline = 1;
#ifdef _SCPI_DEBUG_
						printf("flag__found_newline:%d, @ii=%d \n",(int)flag__found_newline,(int)ii);
#endif
						if ((ret-2-ii>=0)&&(buf[ret-2-ii]=='\r')) {
							buf[ret-2-ii]='\n'; // convert '\r' --> '\n'
						}
						break;
					}
				}
				if (flag__found_newline) break;
				//
#ifdef _SCPI_DEBUG_
				printf("get more socket data. flag__found_newline:%d \n",(int)flag__found_newline);
#endif
				//usleep(320); // wait for 320us
				usleep(100); // wait for 100us
				size = getSn_RX_RSR(sn);
				//
				if (size==0) {
					break; // no more data; leave!
				}
				//
				ret2 = recv(sn, buf+ret, size); //$$ read socket data, and save them into buf 
				if(ret2 <= 0) 
					return ret2; //$$
#ifdef _SCPI_DEBUG_
				printf("size=%d, ret=%d, ret2=%d \n",(int)size,(int)ret,(int)ret2);
#endif
				ret = ret+ret2;
				buf[ret] = '\0'; // add sentinel
				//
				// if too many try.... close socket and leave.... 
			}
			// note new line check may fail if command has numberic block...
			// need some method ... what if buf starting with '#4_' must be numberic block...?!
			// waiting for whole numberic block...
			//}
			
			//// find scpi command and respond //{
			//   - case: buf has the completed command 
			//   - case: buf has no valid command 
			// add 
#ifdef _SCPI_DEBUG_
			size = strlen((char*)buf); // assume buf has ascii... not binary...
			printf("recv size:%d , string size:%d, contents:%s \n",(int)ret,(int)size,buf);
#endif
			// TODO: process scpi commands 
			
			// TODO: case of ECHO //{
			if (buf[0]=='\n') { // echo '\n'
				// make scpi response string
				p_rsp_str = rsp_str__NL;
			}
			//}
			
			// TODO: case of  cmd_str__IDN //{
			else if (0==strncmp((char*)cmd_str__IDN,(char*)buf,LEN_CMD_STR__IDN)) { // 0 means eq
				u32 val;
				// make scpi response string
				//   - case: *IDN?<NL> --> "CMU-CPU-F5500, "__DATE__" \n"
				//            add FPGA image ID 
				val = XIomodule_In32 (ADRS_FPGA_IMAGE);
				sprintf((char*)rsp_str,"%s; FID#H%08X\n", rsp_str__IDN, (unsigned int)val);
				p_rsp_str = rsp_str;
			}
			//}
			
			// TODO: case of  cmd_str__RST //{
			else if (0==strncmp((char*)cmd_str__RST,(char*)buf,LEN_CMD_STR__RST)) { // 0 means eq
				// Reset process ... LAN reset (meaningless) vs CMU reset (SPO/DAVE/ADC init...)
				reset_mcs_ep();
				reset_io_dev();
				// make scpi response string
				p_rsp_str = rsp_str__OK;
			}
			//}
			
			//// :PGEP
			
			// TODO: case of  cmd_str__PGEP_EN //{
			else if (0==strncmp((char*)cmd_str__PGEP_EN,(char*)buf,LEN_CMD_STR__PGEP_EN)) { // 0 means eq
				// subfunctions:
				//        enable_mcs_ep()
				//       disable_mcs_ep()
				//    is_enabled_mcs_ep()
				//
				u32 loc = LEN_CMD_STR__PGEP_EN;
				u32 val;
				//
				// skip spaces ' ' and tap
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//
				// make scpi response string
				if (buf[loc]=='?') {
					val = is_enabled_mcs_ep();
					if (val == 0) p_rsp_str = rsp_str__OFF;
					else          p_rsp_str = rsp_str__ON;
				}
				else if (0==strncmp("ON", (char*)&buf[loc], 2)) {
					// enable
					enable_mcs_ep();
					//
					p_rsp_str = rsp_str__OK;
				}
				else if (0==strncmp("OFF", (char*)&buf[loc], 3)) {
					disable_mcs_ep();
					p_rsp_str = rsp_str__OK;
				}
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGEP_WMI  = (uint8_t*)":PGEP:WMI"; //{
			else if (0==strncmp((char*)cmd_str__PGEP_WMI,(char*)buf,LEN_CMD_STR__PGEP_WMI)) { // 0 means eq
				// subfunctions:
				//     write_mcs_ep_wi_mask(msk);
				//     write_mcs_ep_wi_data(ofs,val);
				//
				// # ":PGEP:WMI#H00 #HABCD1234 #HFF00FF00\n"
				// 
				u32 loc = LEN_CMD_STR__PGEP_WMI; //$$
				u32 val;
				u32 ofs; 
				u32 msk;
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("ofs: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					
					// find value
					if (0==strncmp("#H", (char*)&buf[loc], 2)) {
						loc = loc + 2; // locate the numeric parameter head
						val = hexstr2data_u32((u8*)(buf+loc),8);
						loc = loc + 8; //	
#ifdef _SCPI_DEBUG_
						printf("val: 0x%08X\n",(unsigned int)val); 
#endif
						// skip spaces ' ' and tap
						while (1) {
							if      (buf[loc]==' ') loc++;
							else if (buf[loc]=='\t') loc++;
							else break;
						}
						
						// find mask 
						if (0==strncmp("#H", (char*)&buf[loc], 2)) {
							loc = loc + 2; // locate the numeric parameter head
							msk = hexstr2data_u32((u8*)(buf+loc),8);
							//loc = loc + 8; //	
#ifdef _SCPI_DEBUG_
							printf("msk: 0x%08X\n",(unsigned int)msk); 
#endif
							write_mcs_ep_wi_mask(MCS_EP_BASE,msk);
							write_mcs_ep_wi_data(MCS_EP_BASE,ofs,val); //$$
							p_rsp_str = rsp_str__OK;
						}
						else {
							// return NG 
							p_rsp_str = rsp_str__NG;
						}
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGEP_WMO  = (uint8_t*)":PGEP:WMO"; //{
			else if (0==strncmp((char*)cmd_str__PGEP_WMO,(char*)buf,LEN_CMD_STR__PGEP_WMO)) { // 0 means eq
				// subfunctions:
				//    write_mcs_ep_wo_mask(msk);
				//     read_mcs_ep_wo_data(ofs);
				//
				// # ":PGEP:WMO#H20 #HFFFF0000\n"
				// 
				u32 loc = LEN_CMD_STR__PGEP_WMO; //$$
				u32 val;
				u32 ofs; 
				u32 msk;
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("ofs: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					
					// find mask 
					if (0==strncmp("#H", (char*)&buf[loc], 2)) {
						loc = loc + 2; // locate the numeric parameter head
						msk = hexstr2data_u32((u8*)(buf+loc),8);
						//loc = loc + 8; //	
#ifdef _SCPI_DEBUG_
						printf("msk: 0x%08X\n",(unsigned int)msk); 
#endif
						write_mcs_ep_wo_mask(MCS_EP_BASE,msk); // write mask 
						val = read_mcs_ep_wo_data(MCS_EP_BASE,ofs); // read wireout
						sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
						p_rsp_str = rsp_str;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGEP_TAC//{
			else if (0==strncmp((char*)cmd_str__PGEP_TAC,(char*)buf,LEN_CMD_STR__PGEP_TAC)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_wi_mask();
				//    u32 write_mcs_ep_wi_mask(u32 mask);
				//    u32  read_mcs_ep_ti_data(u32 offset);
				//    u32 write_mcs_ep_ti_data(u32 offset, u32 data);
				//    void activate_mcs_ep_ti(u32 offset, u32 bit_loc);
				//
				// # ":PGEP:TAC#H40 #H01\n"
				// ==
				// # ":PGEP:MKTI#H40 #H00000002\n"
				// # ":PGEP:TI#H40   #H00000002\n"
				// 
				u32 loc = LEN_CMD_STR__PGEP_TAC; //$$
				u32 val;
				u32 ofs; 
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// write command
					if (0==strncmp("#H", (char*)&buf[loc], 2)) {
						loc = loc + 2; // locate the numeric parameter head
						val = hexstr2data_u32((u8*)(buf+loc),2); //$$ read 2-byte para
#ifdef _SCPI_DEBUG_
						printf("check: 0x%08X\n",(unsigned int)val); 
#endif
						activate_mcs_ep_ti(MCS_EP_BASE,ofs,val);
						// convert bit_loc --> mask 
						//val = (0x00000001<<val);
						//write_mcs_ep_wi_mask(val); //$$
						//write_mcs_ep_ti_data(ofs,val); //$$
						p_rsp_str = rsp_str__OK;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGEP_TMO //{
			else if (0==strncmp((char*)cmd_str__PGEP_TMO,(char*)buf,LEN_CMD_STR__PGEP_TMO)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_to_mask(); //$$
				//    u32 write_mcs_ep_to_mask(u32 mask); //$$
				//    u32  read_mcs_ep_to_data(u32 offset);
				//    u32 is_triggered_mcs_ep_to(u32 offset, u32 mask);
				//
				// # cmd: ":PGEP:TMO#H60 #H0000FFFF\n" vs ":PGEP:TMO#H60? #H0000FFFF\n"
				// # rsp: "ON\n" or "OFF\n"
				// 
				u32 loc = LEN_CMD_STR__PGEP_TMO; //$$
				u32 val;
				u32 ofs; 
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// command
					if (0==strncmp("#H", (char*)&buf[loc], 2)) {
						loc = loc + 2; // locate the numeric parameter head
						val = hexstr2data_u32((u8*)(buf+loc),8); //$$ read 8-byte para
#ifdef _SCPI_DEBUG_
						printf("check: 0x%08X\n",(unsigned int)val); 
#endif
						val = is_triggered_mcs_ep_to(MCS_EP_BASE,ofs,val);
						if (val==0) 
							p_rsp_str = rsp_str__OFF;
						else        
							p_rsp_str = rsp_str__ON;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGEP_PI //{
			else if (0==strncmp((char*)cmd_str__PGEP_PI,(char*)buf,LEN_CMD_STR__PGEP_PI)) { // 0 means eq
				// subfunctions:
				//    void dcopy_buf8_to_pipe32(u8 *p_buf_u8, u32 adrs_p32, u32 len_byte); // (src,dst,len_byte)
				//
				// # cmd: ":PGEP:PI#H8A #4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
				// 
				u32 loc = LEN_CMD_STR__PGEP_PI; //$$
				//u32 val;
				u32 ofs; 
				u32 len_byte;
				u32 adrs_p32;
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// write command
					if (0==strncmp("#4_", (char*)&buf[loc], 3)) { //$$ #4 ... numeric block of 4-byte unit of binary byte(8 bit)
						loc = loc + 3; // locate the numeric parameter head
						len_byte = decstr2data_u32((u8*)(buf+loc),6); //$$ 6 bytes for data byte length
						loc = loc + 7; // skip a char '_'
#ifdef _SCPI_DEBUG_
						printf("check: 0x%06d\n",(unsigned int)len_byte); 
#endif
						// copy buf to pipe 
						//adrs_p32 = ADRS_BASE_CMU + (ofs<<4); 
						adrs_p32 = MCS_EP_BASE + (ofs<<4); 
						//dcopy_buf32_to_pipe32((u32*)(buf+loc), adrs_p32, len_byte);
						dcopy_buf8_to_pipe32((u8*)(buf+loc), adrs_p32, len_byte); 
						//
						p_rsp_str = rsp_str__OK;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGEP_PO //{
			else if (0==strncmp((char*)cmd_str__PGEP_PO,(char*)buf,LEN_CMD_STR__PGEP_PO)) { // 0 means eq
				// subfunctions:
				//    void dcopy_pipe32_to_pipe8 (u32 src_adrs_p32, u32 dst_adrs_p8,  u32 len_byte);
				//
				// # cmd: ":PGEP:PO#HAA 000040\n"
				// # cmd: ":PGEP:PO#HAA 001024\n"
				// # cmd: ":PGEP:PO#HBC 131072\n"
				// # rsp: #4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
				//
				u32 loc = LEN_CMD_STR__PGEP_PO; //$$
				//u32 val;
				u32 ofs; 
				u32 len_byte;
				u32 adrs_p32;
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// read command
					if (isdigit(buf[loc])) { //$$ isdigit() numeric parameter check
						len_byte = decstr2data_u32((u8*)(buf+loc),6); //$$ 6 bytes for data byte length
						loc = loc + 6; 
#ifdef _SCPI_DEBUG_
						printf("check: %06d\n",(unsigned int)len_byte); 
#endif
						// send numberic block head : #4_nnnnnn_
						sprintf((char*)rsp_str,"#4_%06d_",(int)len_byte); // '\0' added
						size = strlen((char*)rsp_str);
						ret = send_response_all(sn, rsp_str, size); //$$ first message
						if (ret < 0) {
							close(sn);
							return ret;
						}
						
						// send numeric block 
						// ... dcopy_pipe32_to_pipe8
						// send_response_all_from_pipe32() ... 
						//adrs_p32 = ADRS_BASE_CMU + (ofs<<4); 
						adrs_p32 = MCS_EP_BASE + (ofs<<4); 
						ret = send_response_all_from_pipe32(sn, adrs_p32, len_byte); //$$ first message block
						if (ret < 0) {
							close(sn);
							return ret;
						}
						
						// return NL
						p_rsp_str = rsp_str__NL; // Newline sentinel. this will be last message block
						//
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGEP_MKWI //{
			else if (0==strncmp((char*)cmd_str__PGEP_MKWI,(char*)buf,LEN_CMD_STR__PGEP_MKWI)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_wi_mask();
				//    u32 write_mcs_ep_wi_mask(u32 mask);
				//
				u32 loc = LEN_CMD_STR__PGEP_MKWI;
				u32 val;
				//
				// skip spaces ' ' and tap
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//
				// make scpi response string
				// ex ":PGEP:MKWI?\n"
				if (buf[loc]=='?') { 
					val = read_mcs_ep_wi_mask(MCS_EP_BASE);
					// "#H00AA00BB\n"
					// rsp_str ...
					//sprintf((char*)rsp_str,"#H%08X\n\0",(unsigned int)val); // ex ""
					//rsp_str[9]='\r'; rsp_str[10]='\r'; rsp_str[11]='\r';
					sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
					//printf("0x%02X 0x%02X 0x%02X \n",rsp_str[9],rsp_str[10],rsp_str[11]); // test
					p_rsp_str = rsp_str;
				}
				// ex ":PGEP:MKWI #HFFCCAA33\n"
				else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					//sscanf((char*)(buf+2),"%8x",(int*)&val); 
					// sscanf issue --> replace it with user subfunctions 
					// test sscanf -- pass with stack 2KB(0x800) and heap 32KB(0x8000).
					// NG 0x500 0x4000 ...  .rodata
					// issue ... sscanf footprint ... too big ...21KB more...
					// bss rodata heap on new mem... then works... how to add new mem...??
					// mem allocation??
					// bss... tcp buffer... make it small...  // looks possible.. large data must use pipe directly...
					// text... come from debuf test of the cmu_cpu... check! not much..
					// MicroBlaze MCS .. 128KB vs MicroBlaze .. 4GB
					//   https://www.xilinx.com/products/design-tools/mb-mcs.html
					//   https://www.xilinx.com/products/design-tools/microblaze.html
					//
					// 0x00050 0x1FFB0 // 0x00050+0x1FFB0=0x20000
					//
					// 0x06D00 0x19300 // 0x06D00+0x19300=0x20000
					// 0x00050 0x06CB0 // 0x00050+0x06CB0=0x6D00
					//
					// 0x06C00 0x19400 // 0x06C00+0x19400=0x20000
					// 0x00050 0x06BB0 // 0x00050+0x06BB0=0x06C00
					//
					// 0x06B00 0x19500 // 0x06B00+0x19500=0x20000
					// 0x00050 0x06AB0 // 0x00050+0x06AB0=0x06B00
					//
					// 0x06A00 0x19600 // 0x06A00+0x19600=0x20000
					// 0x00050 0x069B0 // 0x00050+0x069B0=0x06A00
					//
					loc = loc + 2; // locate the numeric parameter head
					// subfunction : u32 hexstr2data_u32(u8* hexstr, u32 len);
					// subfunction : u32 hexchr2data_u32(u8 hexchr);
					//
					//printf("para:%s\n",(char*)buf+loc);
					////
					//printf("h2d:%d\n",(int)hexchr2data_u32(buf[loc+0]));
					//printf("h2d:%d\n",(int)hexchr2data_u32(buf[loc+1]));
					//printf("h2d:%d\n",(int)hexchr2data_u32(buf[loc+2]));
					//printf("h2d:%d\n",(int)hexchr2data_u32(buf[loc+3]));
					//printf("h2d:%d\n",(int)hexchr2data_u32(buf[loc+4]));
					//printf("h2d:%d\n",(int)hexchr2data_u32(buf[loc+5]));					
					//printf("h2d:%d\n",(int)hexchr2data_u32(buf[loc+6]));
					//printf("h2d:%d\n",(int)hexchr2data_u32(buf[loc+7]));					
					////
					//val = hexchr2data_u32(buf[loc++]);
					////printf("val=%d\n",(int)val);
					//val = (val<<4) + hexchr2data_u32(buf[loc++]);
					//val = (val<<4) + hexchr2data_u32(buf[loc++]);
					//val = (val<<4) + hexchr2data_u32(buf[loc++]);
					//val = (val<<4) + hexchr2data_u32(buf[loc++]);
					//val = (val<<4) + hexchr2data_u32(buf[loc++]);
					//val = (val<<4) + hexchr2data_u32(buf[loc++]);
					//val = (val<<4) + hexchr2data_u32(buf[loc++]);
					//
					val = hexstr2data_u32((u8*)(buf+loc),8);
					//
#ifdef _SCPI_DEBUG_
					printf("check: 0x%08X\n",(unsigned int)val); 
#endif
					//
					// process...
					write_mcs_ep_wi_mask(MCS_EP_BASE,val);
					//
					p_rsp_str = rsp_str__OK;
				}
				//
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGEP_MKWO //{
			else if (0==strncmp((char*)cmd_str__PGEP_MKWO,(char*)buf,LEN_CMD_STR__PGEP_MKWO)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_wo_mask(); //$$
				//    u32 write_mcs_ep_wo_mask(u32 mask); //$$
				//
				u32 loc = LEN_CMD_STR__PGEP_MKWO; //$$
				u32 val;
				//
				// skip spaces ' ' and tap
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//
				// make scpi response string
				if (buf[loc]=='?') { 
					val = read_mcs_ep_wo_mask(MCS_EP_BASE); //$$
					sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
					p_rsp_str = rsp_str;
				}
				else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					loc = loc + 2; // locate the numeric parameter head
					val = hexstr2data_u32((u8*)(buf+loc),8);
#ifdef _SCPI_DEBUG_
					printf("check: 0x%08X\n",(unsigned int)val); 
#endif
					//
					// process...
					write_mcs_ep_wo_mask(MCS_EP_BASE,val); //$$
					//
					p_rsp_str = rsp_str__OK;
				}
				//
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGEP_MKTI //{
			else if (0==strncmp((char*)cmd_str__PGEP_MKTI,(char*)buf,LEN_CMD_STR__PGEP_MKTI)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_ti_mask(); //$$
				//    u32 write_mcs_ep_ti_mask(u32 mask); //$$
				//
				u32 loc = LEN_CMD_STR__PGEP_MKTI; //$$
				u32 val;
				//
				// skip spaces ' ' and tap
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//
				// make scpi response string
				if (buf[loc]=='?') { 
					val = read_mcs_ep_ti_mask(MCS_EP_BASE); //$$
					sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
					p_rsp_str = rsp_str;
				}
				else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					loc = loc + 2; // locate the numeric parameter head
					val = hexstr2data_u32((u8*)(buf+loc),8);
#ifdef _SCPI_DEBUG_
					printf("check: 0x%08X\n",(unsigned int)val); 
#endif
					//
					// process...
					write_mcs_ep_ti_mask(MCS_EP_BASE,val); //$$
					//
					p_rsp_str = rsp_str__OK;
				}
				//
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGEP_MKTO //{
			else if (0==strncmp((char*)cmd_str__PGEP_MKTO,(char*)buf,LEN_CMD_STR__PGEP_MKTO)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_to_mask(); //$$
				//    u32 write_mcs_ep_to_mask(u32 mask); //$$
				//
				u32 loc = LEN_CMD_STR__PGEP_MKTO; //$$
				u32 val;
				//
				// skip spaces ' ' and tap
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//
				// make scpi response string
				if (buf[loc]=='?') { 
					val = read_mcs_ep_to_mask(MCS_EP_BASE); //$$
					sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
					p_rsp_str = rsp_str;
				}
				else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					loc = loc + 2; // locate the numeric parameter head
					val = hexstr2data_u32((u8*)(buf+loc),8);
#ifdef _SCPI_DEBUG_
					printf("check: 0x%08X\n",(unsigned int)val); 
#endif
					//
					// process...
					write_mcs_ep_to_mask(MCS_EP_BASE,val); //$$
					//
					p_rsp_str = rsp_str__OK;
				}
				//
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGEP_WI //{
			else if (0==strncmp((char*)cmd_str__PGEP_WI,(char*)buf,LEN_CMD_STR__PGEP_WI)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_wi_data(u32 offset);
				//    u32 write_mcs_ep_wi_data(u32 offset, u32 data);
				//
				// # ":PGEP:WI#H00 #H00001234\n"
				// # ":PGEP:WI#H00?\n"
				// 
				u32 loc = LEN_CMD_STR__PGEP_WI; //$$
				u32 val;
				u32 ofs; 
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// read command 
					if (buf[loc]=='?') { 
						val = read_mcs_ep_wi_data(MCS_EP_BASE,ofs); //$$
						sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
						p_rsp_str = rsp_str;
					}
					// write command
					else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
						loc = loc + 2; // locate the numeric parameter head
						val = hexstr2data_u32((u8*)(buf+loc),8);
#ifdef _SCPI_DEBUG_
						printf("check: 0x%08X\n",(unsigned int)val); 
#endif
						write_mcs_ep_wi_data(MCS_EP_BASE,ofs,val); //$$
						p_rsp_str = rsp_str__OK;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGEP_WO //{
			else if (0==strncmp((char*)cmd_str__PGEP_WO,(char*)buf,LEN_CMD_STR__PGEP_WO)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_wo_data(u32 offset);
				//
				// # ":PGEP:WO#H20?\n"
				// 
				u32 loc = LEN_CMD_STR__PGEP_WO; //$$
				u32 val;
				u32 ofs; 
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// read command 
					if (buf[loc]=='?') { 
						val = read_mcs_ep_wo_data(MCS_EP_BASE,ofs); //$$
						sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
						p_rsp_str = rsp_str;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGEP_TI //{
			else if (0==strncmp((char*)cmd_str__PGEP_TI,(char*)buf,LEN_CMD_STR__PGEP_TI)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_ti_data(u32 offset);
				//    u32 write_mcs_ep_ti_data(u32 offset, u32 data);
				//
				// # ":PGEP:TI#H40 #H00000002\n"
				// # ":PGEP:TI#H40?\n"
				// 
				u32 loc = LEN_CMD_STR__PGEP_TI; //$$
				u32 val;
				u32 ofs; 
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// read command 
					if (buf[loc]=='?') { 
						val = read_mcs_ep_ti_data(MCS_EP_BASE,ofs); //$$
						sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
						p_rsp_str = rsp_str;
					}
					// write command
					else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
						loc = loc + 2; // locate the numeric parameter head
						val = hexstr2data_u32((u8*)(buf+loc),8);
#ifdef _SCPI_DEBUG_
						printf("check: 0x%08X\n",(unsigned int)val); 
#endif
						write_mcs_ep_ti_data(MCS_EP_BASE,ofs,val); //$$
						p_rsp_str = rsp_str__OK;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGEP_TO //{
			else if (0==strncmp((char*)cmd_str__PGEP_TO,(char*)buf,LEN_CMD_STR__PGEP_TO)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_to_data(u32 offset);
				//
				// # ":PGEP:TO#H60?\n"
				// 
				u32 loc = LEN_CMD_STR__PGEP_TO; //$$
				u32 val;
				u32 ofs; 
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// read command 
					if (buf[loc]=='?') { 
						val = read_mcs_ep_to_data(MCS_EP_BASE,ofs); //$$
						sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
						p_rsp_str = rsp_str;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			//// :PGU
			
			// TODO: case of  cmd_str__PGU_PWR //{
			else if (0==strncmp((char*)cmd_str__PGU_PWR,(char*)buf,LEN_CMD_STR__PGU_PWR)) { // 0 means eq
				// subfunctions:
				//    pgu_spio_ext_pwr_led_readback()
				//    pgu_spio_ext_pwr_led(u32 led, u32 pwr_dac, u32 pwr_adc, u32 pwr_amp)
				//
				u32 loc = LEN_CMD_STR__PGU_PWR;
				u32 val;
				//
				// skip spaces ' ' and tap //{
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//}
				
				// make scpi response string
				if (buf[loc]=='?') {
					val = pgu_spio_ext_pwr_led_readback();
					val = val & 0x0004; // 
					if (val == 0) p_rsp_str = rsp_str__OFF;
					else          p_rsp_str = rsp_str__ON;
				}
				else if (0==strncmp("ON", (char*)&buf[loc], 2)) {
					// local var
					u32 val_s0;
					u32 val_s1;
					// read power status 
					val = pgu_spio_ext_pwr_led_readback();
					val_s0 = (val>>0) & 0x0001;
					val_s1 = (val>>1) & 0x0001;
					// DAC power on
					pgu_spio_ext_pwr_led(1, 1, val_s1, val_s0);
					// DAC power on
					//pgu_spio_ext_pwr_led(1, 1, 0, 0); // test for no amp power
					//
					// CLKD init
					pgu_clkd_init();
					//
					// CLKD setup
					pgu_clkd_setup(2000); // preset 200MHz
					//
					// DACX init 
					pgu_dacx_init();
					//
					// DACX setup 
					pgu_dacx_setup();
					//
					// DACX_PG setup
					pgu_dacx_pg_setup();
					//
					p_rsp_str = rsp_str__OK;
				}
				else if (0==strncmp("OFF", (char*)&buf[loc], 3)) {
					// DAC power off
					pgu_spio_ext_pwr_led(0, 0, 0, 0);
					p_rsp_str = rsp_str__OK;
				}
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGU_OUTP //{
			else if (0==strncmp((char*)cmd_str__PGU_OUTP,(char*)buf,LEN_CMD_STR__PGU_OUTP)) { // 0 means eq
				// subfunctions:
				//    pgu_spio_ext_pwr_led_readback()
				//    pgu_spio_ext_pwr_led(u32 led, u32 pwr_dac, u32 pwr_adc, u32 pwr_amp)
				//
				u32 loc = LEN_CMD_STR__PGU_OUTP;
				u32 val;
				//
				// skip spaces ' ' and tap //{
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//}
				
				// make scpi response string
				if (buf[loc]=='?') {
					val = pgu_spio_ext_pwr_led_readback();
					val = val & 0x0001; // for pwr_amp
					if (val == 0) p_rsp_str = rsp_str__OFF;
					else          p_rsp_str = rsp_str__ON;
				}
				else if (0==strncmp("ON", (char*)&buf[loc], 2)) {
					// local var
					u32 val_s1;
					u32 val_s2;
					u32 val_s3;
					// read power status 
					val = pgu_spio_ext_pwr_led_readback();
					val_s1 = (val>>1) & 0x0001;
					val_s2 = (val>>2) & 0x0001;
					val_s3 = (val>>3) & 0x0001;
					// output power on
					pgu_spio_ext_pwr_led(val_s3, val_s2, val_s1, 1);
					//
					p_rsp_str = rsp_str__OK;
				}
				else if (0==strncmp("OFF", (char*)&buf[loc], 3)) {
					// local var
					u32 val_s1;
					u32 val_s2;
					u32 val_s3;
					// read power status 
					val = pgu_spio_ext_pwr_led_readback();
					val_s1 = (val>>1) & 0x0001;
					val_s2 = (val>>2) & 0x0001;
					val_s3 = (val>>3) & 0x0001;
					// output power off
					pgu_spio_ext_pwr_led(val_s3, val_s2, val_s1, 0);
					//
					p_rsp_str = rsp_str__OK;
				}
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGU_DCS_TRIG //{
			else if (0==strncmp((char*)cmd_str__PGU_DCS_TRIG,(char*)buf,LEN_CMD_STR__PGU_DCS_TRIG)) { // 0 means eq
				// subfunctions:
				//    pgu_dacx_dcs_run_test()
				//    pgu_dacx_dcs_stop_test()
				//
				u32 loc = LEN_CMD_STR__PGU_DCS_TRIG;
				//u32 val;
				//
				// skip spaces ' ' and tap //{
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//}
				
				// make scpi response string
				if (buf[loc]=='?') {
					// 
					p_rsp_str = rsp_str__NG;
				}
				else if (0==strncmp("ON", (char*)&buf[loc], 2)) {
					// trig on
					pgu_dacx_dcs_run_test();
					//
					p_rsp_str = rsp_str__OK;
				}
				else if (0==strncmp("OFF", (char*)&buf[loc], 3)) {
					// trig off
					pgu_dacx_dcs_stop_test();
					//
					p_rsp_str = rsp_str__OK;
				}
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			//}

			// TODO: case of  cmd_str__PGU_DCS_DAC0_PNT //{
			else if (0==strncmp((char*)cmd_str__PGU_DCS_DAC0_PNT,(char*)buf,LEN_CMD_STR__PGU_DCS_DAC0_PNT)) { // 0 means eq
				// subfunctions:
				//    pgu_dacx_dcs_write_adrs()
				//    pgu_dacx_dcs_read_data_dac0()
				//    pgu_dacx_dcs_write_data_dac0(u32 val_b32)
				//
				// # ":PGU:DCS:DAC0:PNT#H0001? \n"
				// # ":PGU:DCS:DAC0:PNT#H0001 #H00040001 \n"
				//
				u32 loc = LEN_CMD_STR__PGU_DCS_DAC0_PNT; //$$
				u32 val;
				u32 ofs; 
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),4); //$$ length 4 char
					loc = loc + 4; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%04X\n",(unsigned int)ofs); //$$ length 4 char
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// read command 
					if (buf[loc]=='?') { 
						//
						pgu_dacx_dcs_write_adrs(ofs);        //$$
						val = pgu_dacx_dcs_read_data_dac0(); //$$
						//
						sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "#H00000002\n\0"
						p_rsp_str = rsp_str;
					}
					// write command
					else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
						loc = loc + 2; // locate the numeric parameter head
						val = hexstr2data_u32((u8*)(buf+loc),8);
#ifdef _SCPI_DEBUG_
						printf("check: 0x%08X\n",(unsigned int)val); 
#endif
						//
						pgu_dacx_dcs_write_adrs(ofs);        //$$
						pgu_dacx_dcs_write_data_dac0(val); //$$
						//
						p_rsp_str = rsp_str__OK;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}				
			}
			//}
			
			// TODO: case of  cmd_str__PGU_DCS_DAC1_PNT //{
			else if (0==strncmp((char*)cmd_str__PGU_DCS_DAC1_PNT,(char*)buf,LEN_CMD_STR__PGU_DCS_DAC1_PNT)) { // 0 means eq
				// subfunctions:
				//    pgu_dacx_dcs_write_adrs()
				//    pgu_dacx_dcs_read_data_dac1()
				//    pgu_dacx_dcs_write_data_dac1(u32 val_b32)
				//
				// # ":PGU:DCS:DAC1:PNT#H0001? \n"
				// # ":PGU:DCS:DAC1:PNT#H0001 #H00040001 \n"
				//
				u32 loc = LEN_CMD_STR__PGU_DCS_DAC1_PNT; //$$
				u32 val;
				u32 ofs; 
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),4); //$$ length 4 char
					loc = loc + 4; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%04X\n",(unsigned int)ofs); //$$ length 4 char
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// read command 
					if (buf[loc]=='?') { 
						//
						pgu_dacx_dcs_write_adrs(ofs);        //$$
						val = pgu_dacx_dcs_read_data_dac1(); //$$
						//
						sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "#H00000002\n\0"
						p_rsp_str = rsp_str;
					}
					// write command
					else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
						loc = loc + 2; // locate the numeric parameter head
						val = hexstr2data_u32((u8*)(buf+loc),8);
#ifdef _SCPI_DEBUG_
						printf("check: 0x%08X\n",(unsigned int)val); 
#endif
						//
						pgu_dacx_dcs_write_adrs(ofs);        //$$
						pgu_dacx_dcs_write_data_dac1(val); //$$
						//
						p_rsp_str = rsp_str__OK;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}				
			}			//}
			
			// TODO: case of  cmd_str__PGU_DCS_RPT //{
			else if (0==strncmp((char*)cmd_str__PGU_DCS_RPT,(char*)buf,LEN_CMD_STR__PGU_DCS_RPT)) { // 0 means eq
				// subfunctions:
				//    pgu_dacx_dcs_read_repeat()
				//    pgu_dacx_dcs_write_repeat(u32 val_b32)
				//
				//    DACn repeat count = {16-bit DAC1 repeat count, 16-bit DAC0 repeat count}
				//
				// # ":PGU:DCS:RPT? \n"
				// # ":PGU:DCS:RPT #H00040001 \n"
				//
				u32 loc = LEN_CMD_STR__PGU_DCS_RPT;
				u32 val;
				//
				// skip spaces ' ' and tap //{
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//}
				
				// make scpi response string
				if (buf[loc]=='?') {
					// readback
					val = pgu_dacx_dcs_read_repeat();
					sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
					//
					p_rsp_str = rsp_str;
				}
				else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read value 
					loc = loc + 2; // locate the numeric parameter head
					val = hexstr2data_u32((u8*)(buf+loc),8);
					// set repeat data
					pgu_dacx_dcs_write_repeat(val);
				 	//
				 	p_rsp_str = rsp_str__OK;
				 }
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			//}


			// TODO: case of  cmd_str__PGU_FDCS_TRIG //{
			else if (0==strncmp((char*)cmd_str__PGU_FDCS_TRIG,(char*)buf,LEN_CMD_STR__PGU_FDCS_TRIG)) { // 0 means eq
				// subfunctions:
				//    pgu_dacx_fdcs_run_test()
				//    pgu_dacx_fdcs_stop_test()
				//
				// # ":PGU:FDCS:TRIG ON \n"
				// # ":PGU:FDCS:TRIG OFF \n"
				//
				u32 loc = LEN_CMD_STR__PGU_FDCS_TRIG;
				//u32 val;
				//
				// skip spaces ' ' and tap //{
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//}
				
				// make scpi response string
				if (buf[loc]=='?') {
					// 
					p_rsp_str = rsp_str__NG;
				}
				else if (0==strncmp("ON", (char*)&buf[loc], 2)) {
					// trig on
					pgu_dacx_fdcs_run_test();
					//
					p_rsp_str = rsp_str__OK;
				}
				else if (0==strncmp("OFF", (char*)&buf[loc], 3)) {
					// trig off
					pgu_dacx_fdcs_stop_test();
					//
					p_rsp_str = rsp_str__OK;
				}
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGU_FDCS_DAC0 //{
			else if (0==strncmp((char*)cmd_str__PGU_FDCS_DAC0,(char*)buf,LEN_CMD_STR__PGU_FDCS_DAC0)) { // 0 means eq
				// subfunctions:
				//    pgu_dac0_fifo_write_data(u32 val_b32)
				//
				// # :PGU:FDCS:DAC0 #H_nnnnnn_hhhhhhhh_hhhhhhhh_..._hhhhhhhh <NL>
				// # :PGU:FDCS:DAC0 #4_nnnnnn_rrrrrrrr...rrrr <NL>
				// 
				// # :PGU:FDCS:DAC0 #H_000064_3FFF0002_7FFF0004_3FFF0002_00000001_C0000002_80000004_C0000002_00000001 <NL>
				// # :PGU:FDCS:DAC0 #4_000032_rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr <NL>
				//
				// hexadecimal vs binary format (reserved)
				//
				u32 loc = LEN_CMD_STR__PGU_FDCS_DAC0; //$$
				u32 val;
				u32 len_byte;
				//
				// skip spaces ' ' and tap //{
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//}
				
				// make scpi response string
				if (0==strncmp("#N_", (char*)&buf[loc], 3)) { // check the numeric block header of hexadecimal bytes
					// read len_byte 
					loc = loc + 3; // locate the numeric parameter head //$$
					len_byte = decstr2data_u32((u8*)(buf+loc),6);
					// locate the first byte of data
					loc = loc + 7; // locate the numeric parameter head //$$
					// read 8 byte long data repeatly
					while (len_byte > 0) {
						len_byte = len_byte - 8;
						// read data
						val = hexstr2data_u32((u8*)(buf+loc),8);
						loc = loc + 8;
						// skip '_'
						while (1) {
							if      (buf[loc]=='_') loc++;
							else break;
						}
						// set data
						pgu_dac0_fifo_write_data(val);
					}
				 	//
				 	p_rsp_str = rsp_str__OK;
				}
				else if (0==strncmp("#4_", (char*)&buf[loc], 3)) { // check the numeric block header of binary bytes
					p_rsp_str = rsp_str__NG;
				}
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGU_FDCS_DAC1 //{
			else if (0==strncmp((char*)cmd_str__PGU_FDCS_DAC1,(char*)buf,LEN_CMD_STR__PGU_FDCS_DAC1)) { // 0 means eq
				// subfunctions:
				//    pgu_dac1_fifo_write_data(u32 val_b32)
				//
				// # :PGU:FDCS:DAC1 #H_nnnnnn_hhhhhhhh_hhhhhhhh_..._hhhhhhhh <NL>
				// # :PGU:FDCS:DAC1 #4_nnnnnn_rrrrrrrr...rrrr <NL>
				// 
				// # :PGU:FDCS:DAC1 #N_000064_3FFF0008_7FFF0010_3FFF0008_00000004_C0000008_80000010_C0000008_00000004 <NL>
				// # :PGU:FDCS:DAC1 #4_000032_rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr <NL>
				//
				// hexadecimal vs binary format (reserved)
				//
				u32 loc = LEN_CMD_STR__PGU_FDCS_DAC1; //$$
				u32 val;
				u32 len_byte;
				//
				// skip spaces ' ' and tap //{
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//}
				
				// make scpi response string
				if (0==strncmp("#N_", (char*)&buf[loc], 3)) { // check the numeric block header of hexadecimal bytes
					// read len_byte 
					loc = loc + 3; // locate the numeric parameter head //$$
					len_byte = decstr2data_u32((u8*)(buf+loc),6);
					// locate the first byte of data
					loc = loc + 7; // locate the numeric parameter head //$$
					// read 8 byte long data repeatly
					while (len_byte > 0) {
						len_byte = len_byte - 8;
						// read data
						val = hexstr2data_u32((u8*)(buf+loc),8);
						loc = loc + 8;
						// skip '_'
						while (1) {
							if      (buf[loc]=='_') loc++;
							else break;
						}
						// set data
						pgu_dac1_fifo_write_data(val);
					}
				 	//
				 	p_rsp_str = rsp_str__OK;
				}
				else if (0==strncmp("#4_", (char*)&buf[loc], 3)) { // check the numeric block header of binary bytes
					p_rsp_str = rsp_str__NG;
				}
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
			
			// TODO: case of  cmd_str__PGU_FDCS_RPT //{
			else if (0==strncmp((char*)cmd_str__PGU_FDCS_RPT,(char*)buf,LEN_CMD_STR__PGU_FDCS_RPT)) { // 0 means eq
				// subfunctions:
				//    pgu_dacx_fdcs_read_repeat()
				//    pgu_dacx_fdcs_write_repeat(u32 val_b32)
				//
				//    DACn repeat count = {16-bit DAC1 repeat count, 16-bit DAC0 repeat count}
				//
				// # ":PGU:FDCS:RPT? \n"
				// # ":PGU:FDCS:RPT #H00040001 \n"
				//
				u32 loc = LEN_CMD_STR__PGU_FDCS_RPT;
				u32 val;
				//
				// skip spaces ' ' and tap //{
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//}
				
				// make scpi response string
				if (buf[loc]=='?') {
					// readback
					val = pgu_dacx_fdcs_read_repeat();
					sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
					//
					p_rsp_str = rsp_str;
				}
				else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read value 
					loc = loc + 2; // locate the numeric parameter head
					val = hexstr2data_u32((u8*)(buf+loc),8);
					// set repeat data
					pgu_dacx_fdcs_write_repeat(val);
				 	//
				 	p_rsp_str = rsp_str__OK;
				 }
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			//}

			// TODO: case of  cmd_str__PGU_FREQ //{
			else if (0==strncmp((char*)cmd_str__PGU_FREQ,(char*)buf,LEN_CMD_STR__PGU_FREQ)) { // 0 means eq
				// subfunctions:
				//    pgu_clkd_setup(u32 freq_preset)
				//
				// # ":PGU:FREQ 2000 \n" // for 200.0MHz 
				// # ":PGU:FREQ 0200 \n" // for  20.0MHz
				//
				u32 loc = LEN_CMD_STR__PGU_FREQ;
				u32 val;
				u32 val_ret;
				//
				// skip spaces ' ' and tap //{
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//}
				
				// make scpi response string
				if (0==is_dec_char(buf[loc])) { // check if the first char is decimal
					// read value
					val = decstr2data_u32((u8*)(buf+loc),4); //$$ read 4-char decimal
					// set freq parameter
					val_ret = pgu_clkd_setup(val);
				 	//
					if (val_ret == val)
						p_rsp_str = rsp_str__OK;
					else 
						p_rsp_str = rsp_str__NG;
				}
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			//}

			// TODO: case of  cmd_str__PGU_OFST_DAC0 //{
			else if (0==strncmp((char*)cmd_str__PGU_OFST_DAC0,(char*)buf,LEN_CMD_STR__PGU_OFST_DAC0)) { // 0 means eq
				// subfunctions:
				//    pgu_dac0_reg_write_b8(u32 reg_adrs_b5, u32 val_b8)
				//    pgu_dac0_reg_read_b8(u32 reg_adrs_b5)
				//
				// reg_adrs_b5 = 0x12, 0x11 (for DAC_ch2_aux); 0x0E, 0x0D (for DAC_ch1_aux)
				//
				// # ":PGU:OFST:DAC0? \n" 
				// # ":PGU:OFST:DAC0 #HC140C140 \n" 
				//
				// data = {DAC_ch1_aux, DAC_ch2_aux}
				// DAC_ch#_aux = {PN_Pol_sel, Source_Sink_sel, 0000, 10 bit data}
				//                PN_Pol_sel      = 0/1 for P/N
				//                Source_Sink_sel = 0/1 for Source/Sink
				//
				// # offset DAC : 0x140 0.625mA, AUX2N active[7] (1) , sink current[6] (1)
				//
				u32 loc = LEN_CMD_STR__PGU_OFST_DAC0;
				u32 val;
				u32 val0_high;
				u32 val0_low;
				u32 val1_high;
				u32 val1_low;
				//
				// skip spaces ' ' and tap //{
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//}
				
				// make scpi response string
				if (buf[loc]=='?') {
					// read value from device
					val1_high = pgu_dac0_reg_read_b8(0x0E);
					val1_low  = pgu_dac0_reg_read_b8(0x0D);
					val0_high = pgu_dac0_reg_read_b8(0x12);
					val0_low  = pgu_dac0_reg_read_b8(0x11);
					// compose value
					val = (val1_high<<24) + (val1_low<<16) + (val0_high<<8) + val0_low;
					// set response string
					sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added
					// set response string
					p_rsp_str = rsp_str;
				}
				else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read value 
					loc = loc + 2; // locate the numeric parameter head
					val = hexstr2data_u32((u8*)(buf+loc),8);
					// resolve data
					val1_high = (val>>24) & 0x000000FF;
					val1_low  = (val>>16) & 0x000000FF;
					val0_high = (val>> 8) & 0x000000FF;
					val0_low  = (val>> 0) & 0x000000FF;
					// set data
					pgu_dac0_reg_write_b8(0x0E, val1_high);
					pgu_dac0_reg_write_b8(0x0D, val1_low );
					pgu_dac0_reg_write_b8(0x12, val0_high);
					pgu_dac0_reg_write_b8(0x11, val0_low );
				 	//
				 	p_rsp_str = rsp_str__OK;
				 }
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			//}
				
			// TODO: case of  cmd_str__PGU_OFST_DAC1 //{
			else if (0==strncmp((char*)cmd_str__PGU_OFST_DAC1,(char*)buf,LEN_CMD_STR__PGU_OFST_DAC1)) { // 0 means eq
				// subfunctions:
				//    pgu_dac1_reg_write_b8(u32 reg_adrs_b5, u32 val_b8)
				//    pgu_dac1_reg_read_b8(u32 reg_adrs_b5)
				//
				// reg_adrs_b5 = 0x12, 0x11 (for DAC_ch2_aux); 0x0E, 0x0D (for DAC_ch1_aux)
				//
				// # ":PGU:OFST:DAC1? \n" 
				// # ":PGU:OFST:DAC1 #HC140C140 \n" 
				//
				// data = {DAC_ch1_aux, DAC_ch2_aux}
				// DAC_ch#_aux = {PN_Pol_sel, Source_Sink_sel, 0000, 10 bit data}
				//                PN_Pol_sel      = 0/1 for P/N
				//                Source_Sink_sel = 0/1 for Source/Sink
				//
				// # offset DAC : 0x140 0.625mA, AUX2N active[7] (1) , sink current[6] (1)
				//
				u32 loc = LEN_CMD_STR__PGU_OFST_DAC1;
				u32 val;
				u32 val0_high;
				u32 val0_low;
				u32 val1_high;
				u32 val1_low;
				//
				// skip spaces ' ' and tap //{
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//}
				
				// make scpi response string
				if (buf[loc]=='?') {
					// read value from device
					val1_high = pgu_dac1_reg_read_b8(0x0E);
					val1_low  = pgu_dac1_reg_read_b8(0x0D);
					val0_high = pgu_dac1_reg_read_b8(0x12);
					val0_low  = pgu_dac1_reg_read_b8(0x11);
					// compose value
					val = (val1_high<<24) + (val1_low<<16) + (val0_high<<8) + val0_low;
					// set response string
					sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added
					// set response string
					p_rsp_str = rsp_str;
				}
				else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read value 
					loc = loc + 2; // locate the numeric parameter head
					val = hexstr2data_u32((u8*)(buf+loc),8);
					// resolve data
					val1_high = (val>>24) & 0x000000FF;
					val1_low  = (val>>16) & 0x000000FF;
					val0_high = (val>> 8) & 0x000000FF;
					val0_low  = (val>> 0) & 0x000000FF;
					// set data
					pgu_dac1_reg_write_b8(0x0E, val1_high);
					pgu_dac1_reg_write_b8(0x0D, val1_low );
					pgu_dac1_reg_write_b8(0x12, val0_high);
					pgu_dac1_reg_write_b8(0x11, val0_low );
				 	//
				 	p_rsp_str = rsp_str__OK;
				 }
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			//}

			// TODO: case of  cmd_str__PGU_GAIN_DAC0 //{
			else if (0==strncmp((char*)cmd_str__PGU_GAIN_DAC0,(char*)buf,LEN_CMD_STR__PGU_GAIN_DAC0)) { // 0 means eq
				// subfunctions:
				//    pgu_dac0_reg_write_b8(u32 reg_adrs_b5, u32 val_b8)
				//    pgu_dac0_reg_read_b8(u32 reg_adrs_b5)
				//
				// reg_adrs_b5 = 0x10, 0x0F (for DAC_ch2_fsc); 0x0C, 0x0B (for DAC_ch1_fsc)
				//
				// # ":PGU:GAIN:DAC0? \n" 
				// # ":PGU:GAIN:DAC0 #H02D002D0 \n" 
				//
				// data = {DAC_ch1_fsc, DAC_ch2_fsc}
				// DAC_ch#_fsc = {000000, 10 bit data}
				//
				// # full scale DAC : 28.1mA  @ 0x02D0
				//
				u32 loc = LEN_CMD_STR__PGU_GAIN_DAC0;
				u32 val;
				u32 val0_high;
				u32 val0_low;
				u32 val1_high;
				u32 val1_low;
				//
				// skip spaces ' ' and tap //{
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//}
				
				// make scpi response string
				if (buf[loc]=='?') {
					// read value from device
					val1_high = pgu_dac0_reg_read_b8(0x0C);
					val1_low  = pgu_dac0_reg_read_b8(0x0B);
					val0_high = pgu_dac0_reg_read_b8(0x10);
					val0_low  = pgu_dac0_reg_read_b8(0x0F);
					// compose value
					val = (val1_high<<24) + (val1_low<<16) + (val0_high<<8) + val0_low;
					// set response string
					sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added
					// set response string
					p_rsp_str = rsp_str;
				}
				else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read value 
					loc = loc + 2; // locate the numeric parameter head
					val = hexstr2data_u32((u8*)(buf+loc),8);
					// resolve data
					val1_high = (val>>24) & 0x000000FF;
					val1_low  = (val>>16) & 0x000000FF;
					val0_high = (val>> 8) & 0x000000FF;
					val0_low  = (val>> 0) & 0x000000FF;
					// set data
					pgu_dac0_reg_write_b8(0x0C, val1_high);
					pgu_dac0_reg_write_b8(0x0B, val1_low );
					pgu_dac0_reg_write_b8(0x10, val0_high);
					pgu_dac0_reg_write_b8(0x0F, val0_low );
				 	//
				 	p_rsp_str = rsp_str__OK;
				 }
				else {
					p_rsp_str = rsp_str__NG;
				}		
			}
			//}

			// TODO: case of  cmd_str__PGU_GAIN_DAC1 //{
			else if (0==strncmp((char*)cmd_str__PGU_GAIN_DAC1,(char*)buf,LEN_CMD_STR__PGU_GAIN_DAC1)) { // 0 means eq
				// subfunctions:
				//    pgu_dac1_reg_write_b8(u32 reg_adrs_b5, u32 val_b8)
				//    pgu_dac1_reg_read_b8(u32 reg_adrs_b5)
				//
				// reg_adrs_b5 = 0x10, 0x0F (for DAC_ch2_fsc); 0x0C, 0x0B (for DAC_ch1_fsc)
				//
				// # ":PGU:GAIN:DAC1? \n" 
				// # ":PGU:GAIN:DAC1 #H02D002D0 \n" 
				//
				// data = {DAC_ch1_fsc, DAC_ch2_fsc}
				// DAC_ch#_fsc = {000000, 10 bit data}
				//
				// # full scale DAC : 28.1mA  @ 0x02D0
				//
				u32 loc = LEN_CMD_STR__PGU_GAIN_DAC1;
				u32 val;
				u32 val0_high;
				u32 val0_low;
				u32 val1_high;
				u32 val1_low;
				//
				// skip spaces ' ' and tap //{
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//}
				
				// make scpi response string
				if (buf[loc]=='?') {
					// read value from device
					val1_high = pgu_dac1_reg_read_b8(0x0C);
					val1_low  = pgu_dac1_reg_read_b8(0x0B);
					val0_high = pgu_dac1_reg_read_b8(0x10);
					val0_low  = pgu_dac1_reg_read_b8(0x0F);
					// compose value
					val = (val1_high<<24) + (val1_low<<16) + (val0_high<<8) + val0_low;
					// set response string
					sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added
					// set response string
					p_rsp_str = rsp_str;
				}
				else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read value 
					loc = loc + 2; // locate the numeric parameter head
					val = hexstr2data_u32((u8*)(buf+loc),8);
					// resolve data
					val1_high = (val>>24) & 0x000000FF;
					val1_low  = (val>>16) & 0x000000FF;
					val0_high = (val>> 8) & 0x000000FF;
					val0_low  = (val>> 0) & 0x000000FF;
					// set data
					pgu_dac1_reg_write_b8(0x0C, val1_high);
					pgu_dac1_reg_write_b8(0x0B, val1_low );
					pgu_dac1_reg_write_b8(0x10, val0_high);
					pgu_dac1_reg_write_b8(0x0F, val0_low );
				 	//
				 	p_rsp_str = rsp_str__OK;
				 }
				else {
					p_rsp_str = rsp_str__NG;
				}		
			}
			//}


			// TODO: case of  unknown //{
			else { // unknown commands 
				//p_rsp_str = rsp_str__NULL;
				p_rsp_str = rsp_str__NG;
			}
			//}
			
			//}
			
			// send response //{
			//
			size = strlen((char*)p_rsp_str);
			ret = send_response_all(sn, p_rsp_str, size); //$$
			if (ret < 0) {
				close(sn);
				return ret;
			}
			//}
			
			} //}
			
			else { // recv size 0 //{
				if (flag_get_rx==0) {
					cnt_stay_SOCK_ESTABLISHED = cnt_stay_SOCK_ESTABLISHED - 1;
				}
				else {
					flag_get_rx = 0;
					cnt_stay_SOCK_ESTABLISHED = MAX_CNT_STAY_SOCK_ESTABLISHED;
				}
#ifdef _SCPI_DEBUG_
				printf("connected socket has no recv data: cnt_stay_SOCK_ESTABLISHED:%d \n",(int)cnt_stay_SOCK_ESTABLISHED);
#endif
				if (MAX_CNT_STAY_SOCK_ESTABLISHED!=0 && cnt_stay_SOCK_ESTABLISHED==0) { 
#ifdef _SCPI_DEBUG_
					printf("connected socket has no activity; force to close: cnt_stay_SOCK_ESTABLISHED:%d \n",(int)cnt_stay_SOCK_ESTABLISHED);
#endif
					// close socket
					close(sn);
				}
			} //}
			
			//}
			
			break;
		//}
		case SOCK_CLOSE_WAIT : //{
#ifdef _SCPI_DEBUG_
			//printf("%d:CloseWait \n",sn);
#endif
			if((ret=disconnect(sn)) != SOCK_OK) return ret;
#ifdef _SCPI_DEBUG_
			printf("%d:Socket closed \n",sn);
#endif
			break;
		//}
		case SOCK_INIT : //{
#ifdef _SCPI_DEBUG_
			printf("%d:Listen, TCP server, port [%d] \n",sn, port);
#endif
			if( (ret = listen(sn)) != SOCK_OK) return ret;
			break;
		//}
		case SOCK_CLOSED: //{
#ifdef _SCPI_DEBUG_
			//printf("%d:TCP server start \n",sn);
#endif
			flag_SOCK_ESTABLISHED = 0;
			//if((ret=socket(sn, Sn_MR_TCP, port, 0x00)) != sn)
			if((ret=socket(sn, Sn_MR_TCP, port, SF_TCP_NODELAY)) != sn) //$$ fast ack
			//if((ret=socket(sn, Sn_MR_TCP, port, Sn_MR_ND)) != sn)
			return ret;
#ifdef _SCPI_DEBUG_
			printf("%d:Socket opened \n",sn);
			//printf("%d:Opened, TCP server, port [%d] \n",sn, port);
#endif
			break;
		//}
		case SOCK_LISTEN: //{
			//$$ nothing to do...
			break;
		//}
		default: //{
			break;
		//}
	}
	return 1;
}
//}

//}
















