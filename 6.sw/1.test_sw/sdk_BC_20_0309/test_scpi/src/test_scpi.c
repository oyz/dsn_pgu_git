#include <stdio.h>
#include <string.h>
//#include "xil_printf.h"
#include "microblaze_sleep.h" // for usleep()

//$$ support PGU-CPU
#include "./PGU_CPU_LIB/platform.h" // for init_platform()
#include "./PGU_CPU_LIB/pgu_cpu_config.h" // PGU-CPU board config
#include "./PGU_CPU_LIB/pgu_cpu.h" // for hw_reset

//$$ support W5500
#include "./PGU_CPU_LIB/ioLibrary/Ethernet/W5500/w5500.h" // for w5500 io functions
#include "./PGU_CPU_LIB/ioLibrary/Ethernet/socket.h"	// Just include one header for WIZCHIP

//$$ loopback socket
//#include "./PGU_CPU_LIB/ioLibrary/Appmod/Loopback/loopback.h"
#include "./PGU_CPU_LIB/ioLibrary/Appmod/Loopback/scpi.h" // for scpi server


/////////////////////////////////////////
// SOCKET NUMBER DEFINION for Examples //
/////////////////////////////////////////
#define SOCK_TCPS        0
#define SOCK_TCPS_SUB    1

////////////////////////////////////////////////
// Shared Buffer Definition                   //
////////////////////////////////////////////////
//uint8_t gDATABUF[DATA_BUF_SIZE]; // DATA_BUF_SIZE from loopback.h // -->bss
uint8_t gDATABUF[DATA_BUF_SIZE_SCPI]; // DATA_BUF_SIZE_SCPI from scpi.h // -->bss
//uint8_t gDATABUF_sub[DATA_BUF_SIZE_SCPI_SUB];


///////////////////////////////////
// Default Network Configuration //
///////////////////////////////////
wiz_NetInfo gWIZNETINFO = { .mac = {0x00, 0x08, 0xdc,0x00, 0xab, 0xcd},
							.ip = {192, 168, 168, 119}, // device 119 // firmware test ip
							//.ip = {192, 168, 168, 122}, // device 122
							//.ip = {192, 168, 168, 123}, // device 123
							//.ip = {192, 168, 168, 124}, // device 124
							.sn = {255,255,255,0},
							.gw = {192, 168, 168, 1},
							.dns = {0,0,0,0},
							.dhcp = NETINFO_STATIC };





//////////////////////////////////
// For example of ioLibrary_BSD //
//////////////////////////////////
void network_init(void);								// Initialize Network information and display it
//////////////////////////////////

//#define MAX_ADC_BUF_SIZE 100 // in u32, OK
////#define MAX_ADC_BUF_SIZE 1024 // in u32, OK
////#define MAX_ADC_BUF_SIZE 2048 // in u32, OK with heap=0x7000
////$$#define MAX_ADC_BUF_SIZE 8192/2 // in u32, OK with heap=0x8000 // 32KB
////#define MAX_ADC_BUF_SIZE 16384 // in u32, NG with heap=0x10000 // 64KB // heap is out

int main(void)
{
   uint8_t tmp;
   int32_t ret = 0;
   //uint8_t memsize[2][8] = {{2,2,2,2,2,2,2,2},{2,2,2,2,2,2,2,2}}; // KB => 16KB // TX, RX
   //uint8_t memsize[2][8] = {{2,2,0,0,0,0,0,0},{8,4,0,0,0,0,0,0}}; // KB => 16KB // socket_1 FDCS command buffer overflow
   //uint8_t memsize[2][8] = {{1,1,0,0,0,0,0,0},{7,7,0,0,0,0,0,0}}; // KB => 16KB // OK both but slow a little.
   uint8_t memsize[2][8] = {{2,2,0,0,0,0,0,0},{8,4,0,0,0,0,0,0}}; // KB => 16KB // more rx buf on socket_0 // 0 1 2 3 4 8 16
   //
   //$$ for CMU-CPU
   u32 adrs;
   u32 value;
   //u32 mask;
   //u32 ii;
   //

   // test max inner memory
   //u32 data_adc[131072]; // 2^17 depth // 2^19 B = 2^9 KB =  512 KB
   //u32 data_adc[8192]; // 2^13 depth // 2^15 B = 2^5 KB =  32 KB
   //u32 data_adc[4096]; // 2^12 depth // 2^14 B = 2^4 KB =  16 KB
   //u32 data_adc0[MAX_ADC_BUF_SIZE];
   //u32 data_adc1[MAX_ADC_BUF_SIZE];
   //for (ii=0;ii<MAX_ADC_BUF_SIZE;ii++) {
   //	   data_adc0[ii] = 0xA1B2C3D4+ii;
   //	   data_adc1[ii] = 0x11223344+ii;
   //}
   //xil_printf(">>> mem heap alloc test: 0x%08X @ 0x%08X \r\n", data_adc0[0], &data_adc0[0]);
   //xil_printf(">>> mem heap alloc test: 0x%08X @ 0x%08X \r\n", data_adc1[0], &data_adc1[0]);

   ///////////////////////////////////////////
   // Host dependent peripheral initialized //
   ///////////////////////////////////////////
   init_platform();


   //// FPGA setup
   // test for print on jtag-terminal
   print("> Hello World!! PGU-CPU Start!! \r\n");
   print(">>> build_info: "__TIME__", " __DATE__ ".\r\n");

   // test mcs_io_bridge.v
   xil_printf(">> test mcs_io_bridge.v \r\n");
   xil_printf(">>> mcs_io_bridge_inst0 for LAN spi control \r\n");
   //

   // ADRS_FPGA_IMAGE_ID
   xil_printf(">>> FPGA_IMAGE_ID: \r\n");
   adrs = ADRS_FPGA_IMAGE;
   value = XIomodule_In32 (adrs);
   xil_printf(">done: rd: 0x%08X @ 0x%08X \r\n", value, adrs);


   //// test mcs endpoints
   xil_printf(">>> mcs_io_bridge_inst1 for MCS-PGU Endpoints \r\n");
   //

   // ADRS_BASE_PGU + ADRS_FPGA_IMAGE_OFST
   xil_printf(">>> ADRS_FPGA_IMAGE_OFST: \r\n");
   adrs = ADRS_BASE_PGU + ADRS_FPGA_IMAGE_OFST;
   value = read_mcs_fpga_img_id(ADRS_BASE_PGU);
   xil_printf(">done: rd: 0x%08X @ 0x%08X \r\n", value, adrs);

   // LAN EP enable
   xil_printf(">>> enable MCS endpoints : \r\n");
   enable_mcs_ep();
   value = is_enabled_mcs_ep();
   xil_printf(">done: wr: 0x%08X \r\n", value);

   xil_printf(">>> write SW_BUILD_ID from MCS endpoint : \r\n");
   value = 0x00000000;
   write_mcs_ep_wi(ADRS_BASE_PGU, 0x00, value, 0xFFFFFFFF);
   xil_printf(">done: wr: 0x%08X \r\n", value);

   xil_printf(">>> read FPGA_IMAGE_ID from MCS endpoint : \r\n");
   value = read_mcs_ep_wo(ADRS_BASE_PGU, 0x20, 0xFFFFFFFF);
   xil_printf(">done: rd: 0x%08X \r\n", value);


   //// PGU led control
   // test on
   // pgu.spio_ext__pwr_led(led=1,pwr_dac=1,pwr_adc=0,pwr_amp=1)
   pgu_spio_ext_pwr_led(1,0,0,0);
   value = pgu_spio_ext_pwr_led_readback();
   xil_printf(">> Check LED readback: 0x%08X \r\n", value);
   // sleep
   usleep(9000);
   // test off
   pgu_spio_ext_pwr_led(0,0,0,0);
   value = pgu_spio_ext_pwr_led_readback();
   xil_printf(">> Check LED readback: 0x%08X \r\n", value);


   //// TODO: PGU AUX IO
   // PGU AUX IO initialization
   //
   //pgu_spio_ext__aux_init();
   //value = pgu_spio_ext__aux_in();
   //xil_printf(">> Check AUX IO readback: 0x%08X \r\n", value);
   //pgu_spio_ext__aux_out(0x2); // test write // AUX_MOSI // R
   //pgu_spio_ext__aux_out(0x4); // test write // AUX_SCLK // G
   //pgu_spio_ext__aux_out(0x8); // test write // AUX_CS_B // B
   //pgu_spio_ext__aux_out(0x8); // test write // safe
   //value = pgu_spio_ext__aux_in();
   //xil_printf(">> Check AUX IO readback: 0x%08X \r\n", value);
   //
   //value = pgu_spio_ext__aux_send_spi_frame (0, 0x3C, 0xACAC); //
   //value = pgu_spio_ext__aux_send_spi_frame (1, 0xC3, 0xC8C8); //
   //pgu_spio_ext__aux_reg_write_b16(0x14, 0xACAC);
   //value = pgu_spio_ext__aux_reg_read_b16(0x12);
   //
   //
   pgu_spio_ext__aux_IO_init(); // must
   //
   //pgu_spio_ext__aux_IO_write_b16(0x0000); // safe out test
   //value = pgu_spio_ext__aux_IO_read_b16(); // test

   // LAN EP disable
   xil_printf("\r\n");
   xil_printf(">>> disable MCS endpoints : \r\n");
   disable_mcs_ep();
   value = is_enabled_mcs_ep();
   xil_printf(">done: wr: 0x%08X \r\n", value);

   //// hw reset wz850
   xil_printf(">>> hw reset wz850 \r\n");
   hw_reset__wz850();


   //// socket setup

	/* WIZCHIP SOCKET Buffer initialize */
	if(ctlwizchip(CW_INIT_WIZCHIP,(void*)memsize) == -1)
	{
	   printf("WIZCHIP Initialized fail. \n");
	   while(1);
	}

	/* PHY link status check */
	do
	{
	   if(ctlwizchip(CW_GET_PHYLINK, (void*)&tmp) == -1)
		  printf("Unknown PHY Link stauts. \n");
	}while(tmp == PHY_LINK_OFF);


	/* Network initialization */
	network_init();


	/*****************************************/
	/* WIZnet W5500 inside                   */
	/* TCP socket based SCPI server test     */
	/*****************************************/
	/* Main loop */
	while(1)
	{

		/* SCPI server Test */
		if( (ret = scpi_tcps(SOCK_TCPS, gDATABUF, 5025)) < 0) {
			printf("SOCKET ERROR : %ld \n", ret);
		}

		// sub port
		if( (ret = scpi_tcps(SOCK_TCPS_SUB, gDATABUF, 5050)) < 0) {
			printf("SOCKET ERROR : %ld \n", ret);
		}


		/* PHY link check */
		ctlwizchip(CW_GET_PHYLINK, (void*)&tmp);
		if (tmp == PHY_LINK_OFF) {
			// close socket
			//close(SOCK_TCPS);
			disconnect(SOCK_TCPS);

			// wait for PHY_LINK_ON
			do
			{
				ctlwizchip(CW_GET_PHYLINK, (void*)&tmp);
			}while(tmp == PHY_LINK_OFF);

			// re-init network
			network_init();
		}

	} // end of Main loop

    /////
    cleanup_platform();

} // end of main()

/////////////////////////////////////////////////////////////
// Intialize the network information to be used in WIZCHIP //
/////////////////////////////////////////////////////////////
void network_init(void)
{
   uint8_t tmpstr[6];
	ctlnetwork(CN_SET_NETINFO, (void*)&gWIZNETINFO);
	ctlnetwork(CN_GET_NETINFO, (void*)&gWIZNETINFO);

	// set timeout para
	setRTR(2000);
	setRCR(23);

	// Display Network Information
	ctlwizchip(CW_GET_ID,(void*)tmpstr);
	printf(" \n=== %s NET CONF === \n",(char*)tmpstr);
	printf("MAC: %02X:%02X:%02X:%02X:%02X:%02X \n",gWIZNETINFO.mac[0],gWIZNETINFO.mac[1],gWIZNETINFO.mac[2],
		  gWIZNETINFO.mac[3],gWIZNETINFO.mac[4],gWIZNETINFO.mac[5]);
	printf("SIP: %d.%d.%d.%d \n", gWIZNETINFO.ip[0],gWIZNETINFO.ip[1],gWIZNETINFO.ip[2],gWIZNETINFO.ip[3]);
	printf("GAR: %d.%d.%d.%d \n", gWIZNETINFO.gw[0],gWIZNETINFO.gw[1],gWIZNETINFO.gw[2],gWIZNETINFO.gw[3]);
	printf("SUB: %d.%d.%d.%d \n", gWIZNETINFO.sn[0],gWIZNETINFO.sn[1],gWIZNETINFO.sn[2],gWIZNETINFO.sn[3]);
	printf("DNS: %d.%d.%d.%d \n", gWIZNETINFO.dns[0],gWIZNETINFO.dns[1],gWIZNETINFO.dns[2],gWIZNETINFO.dns[3]);
	printf("====================== \n");
	printf("RTP: %d (0x%04X) \n", getRTR(), getRTR());
	printf("RCR: %d (0x%04X) \n", getRCR(), getRCR());
	printf("====================== \n");


}
/////////////////////////////////////////////////////////////
