#ifndef __PGU_CPU_H_
#define __PGU_CPU_H_

#include "xil_types.h"

//$$ PGU
#include "pgu_cpu_config.h"

#define _MCS_DEBUG_

//// common //{
u32 hexchr2data_u32(u8 hexchr);
u32 hexstr2data_u32(u8* hexstr, u32 len);
u32 decchr2data_u32(u8 chr);
u32 decstr2data_u32(u8* str, u32 len);
u32 is_dec_char(u8 chr); // 0 for dec; -1 for not
//}

//// wiznet 850io functions //{
u8     hw_reset__wz850();
u8    read_data__wz850 (u32 AddrSel);
void write_data__wz850 (u32 AddrSel, u8 value);
void  read_data_buf__wz850 (u32 AddrSel, u8* p_val, u16 len);
void write_data_buf__wz850 (u32 AddrSel, u8* p_val, u16 len);
//
void  read_data_pipe__wz850 (u32 AddrSel, u32 ep_offset, u32 len_u32);
void write_data_pipe__wz850 (u32 AddrSel, u32 src_adrs_p32, u32 len);
//}

//// MCS io access functions  //{
u32   read_mcs_fpga_img_id(u32 adrs_base);
u32   read_mcs_test_reg(u32 adrs_base);
void write_mcs_test_reg(u32 adrs_base, u32 data);
//}

//// MCS-EP access subfunctions //{
void  enable_mcs_ep(); // enable MCS end-point control over CMU-CPU
void disable_mcs_ep(); // disable MCS end-point control over CMU-CPU
void reset_mcs_ep(); // reset MCS end-point control
void reset_io_dev(); // reset ...  adc / dwave / bias / spo //$$ to see for PGU 
u32 is_enabled_mcs_ep(); 
//
u32  read_mcs_ep_wi_mask(u32 adrs_base);
u32 write_mcs_ep_wi_mask(u32 adrs_base, u32 mask);
u32  read_mcs_ep_wo_mask(u32 adrs_base);
u32 write_mcs_ep_wo_mask(u32 adrs_base, u32 mask);
u32  read_mcs_ep_ti_mask(u32 adrs_base);
u32 write_mcs_ep_ti_mask(u32 adrs_base, u32 mask);
u32  read_mcs_ep_to_mask(u32 adrs_base);
u32 write_mcs_ep_to_mask(u32 adrs_base, u32 mask);
u32  read_mcs_ep_wi_data(u32 adrs_base, u32 offset);
u32 write_mcs_ep_wi_data(u32 adrs_base, u32 offset, u32 data);
u32  read_mcs_ep_wo_data(u32 adrs_base, u32 offset);
u32  read_mcs_ep_ti_data(u32 adrs_base, u32 offset);
u32 write_mcs_ep_ti_data(u32 adrs_base, u32 offset, u32 data);
u32  read_mcs_ep_to_data(u32 adrs_base, u32 offset);
u32 write_mcs_ep_pi_data(u32 adrs_base, u32 offset, u32 data); // write data from a 32b-value to pipe-in(32b)
u32  read_mcs_ep_po_data(u32 adrs_base, u32 offset);           // read data from pipe-out(32b) to a 32b-value
//
u32   read_mcs_ep_wi(u32 adrs_base, u32 offset);
void write_mcs_ep_wi(u32 adrs_base, u32 offset, u32 data, u32 mask);
u32   read_mcs_ep_wo(u32 adrs_base, u32 offset, u32 mask);
u32   read_mcs_ep_ti(u32 adrs_base, u32 offset);
void write_mcs_ep_ti(u32 adrs_base, u32 offset, u32 data, u32 mask);
void activate_mcs_ep_ti(u32 adrs_base, u32 offset, u32 bit_loc);
u32   read_mcs_ep_to(u32 adrs_base, u32 offset, u32 mask);
u32 is_triggered_mcs_ep_to(u32 adrs_base, u32 offset, u32 mask);
//
u32 write_mcs_ep_pi_buf (u32 adrs_base, u32 offset, u32 len_byte, u8 *p_data); // write data from buffer(32b) to pipe-in(8b) // not used
u32  read_mcs_ep_po_buf (u32 adrs_base, u32 offset, u32 len_byte, u8 *p_data); // read data from pipe-out(8b) to buffer(32b) // not used
//u32 write_mcs_ep_pi_fifo(u32 offset, u32 len_byte, u8 *p_data); // write data from fifo(32b) to pipe-in(8b) 
//u32  read_mcs_ep_po_fifo(u32 offset, u32 len_byte, u8 *p_data); // read data from pipe-out(8b) to fifo(32b) 

//}

//// subfunctions for pipe //{

// note pipe end-points
// pipe-in  address: 
//    ADRS_PORT_PI_80 ... LAN  fifo  8-bit in MCS0
//    pi8A            ... TEST fifo 32-bit in MCS1
// pipe-out address: 
//    ADRS_PORT_PO_A0 ... LAN  fifo  8-bit in MCS0
//    poAA            ... TEST fifo 32-bit in MCS1
//    poBC            ... ADC  fifo 32-bit in MCS1
//    poBD            ... ADC  fifo 32-bit in MCS1


// PIPE-DATA in buffer (pointer)
//     buffer ... u32 buf[], u8 buf[]
//     pipe  ... 32-bit pipe, 8-bit pipe
//        32-bit pipe --> 32-bit buf
//        32-bit buf  --> 32-bit pipe
//         8-bit pipe -->  8-bit buf
//         8-bit buf  -->  8-bit pipe
//         8-bit pipe --> 32-bit buf
//        32-bit buf  -->  8-bit pipe
void dcopy_pipe8_to_buf8  (u32 adrs_p8, u8 *p_buf_u8, u32 len); // (src,dst,len)
void dcopy_buf8_to_pipe8  (u8 *p_buf_u8, u32 adrs_p8, u32 len); // (src,dst,len)
void dcopy_pipe32_to_buf32(u32 adrs_p32, u32 *p_buf_u32, u32 len_byte); // (src,dst,len_byte) 
void dcopy_buf32_to_pipe32(u32 *p_buf_u32, u32 adrs_p32, u32 len_byte); // (src,dst,len_byte)
void dcopy_buf8_to_pipe32(u8 *p_buf_u8, u32 adrs_p32, u32 len_byte); // (src,dst,len_byte) // used in cmd_str__PGEP_PI

// PIPE-DATA in fifo/pipe (IO address)
//     pipe  ... 32-bit pipe, 8-bit pipe
//        32-bit pipe --> 32-bit pipe
//        32-bit pipe -->  8-bit pipe
//         8-bit pipe --> 32-bit pipe
void dcopy_pipe32_to_pipe32(u32 src_adrs_p32, u32 dst_adrs_p32, u32 len_byte);
void dcopy_pipe8_to_pipe32 (u32 src_adrs_p8,  u32 dst_adrs_p32, u32 len_byte);
void dcopy_pipe32_to_pipe8 (u32 src_adrs_p32, u32 dst_adrs_p8,  u32 len_byte);


//}


//// PGU-CPU functions  //{
	
// SPIO
void  pgu_spio_ext_pwr_led(u32 led, u32 pwr_dac, u32 pwr_adc, u32 pwr_amp);
u32   pgu_spio_ext_pwr_led_readback();

// CLKD
u32   pgu_clkd_init();
u32   pgu_clkd_setup(u32 freq_preset);
//
u32  pgu_clkd_reg_write_b8(u32 reg_adrs_b10, u32 val_b8);
u32  pgu_clkd_reg_read_b8(u32 reg_adrs_b10);

// DACX
u32   pgu_dacx_init();
u32   pgu_dacx_fpga_pll_rst(u32 clkd_out_rst, u32 dac0_dco_rst, u32 dac1_dco_rst);
u32   pgu_dacx_setup();
//
u32  pgu_dac0_reg_write_b8(u32 reg_adrs_b5, u32 val_b8);
u32  pgu_dac0_reg_read_b8(u32 reg_adrs_b5);
u32  pgu_dac1_reg_write_b8(u32 reg_adrs_b5, u32 val_b8);
u32  pgu_dac1_reg_read_b8(u32 reg_adrs_b5);

// DACX_PG
u32  pgu_dacx_pg_setup();
//
void pgu_dacx_dcs_write_adrs(u32 adrs);
u32  pgu_dacx_dcs_read_adrs();
void pgu_dacx_dcs_write_data_dac0(u32 val_b32);
u32  pgu_dacx_dcs_read_data_dac0();
void pgu_dacx_dcs_write_data_dac1(u32 val_b32);
u32  pgu_dacx_dcs_read_data_dac1();
void pgu_dacx_dcs_run_test();
void pgu_dacx_dcs_stop_test();
void pgu_dacx_dcs_write_repeat(u32 val_b32);
u32  pgu_dacx_dcs_read_repeat();
//
void pgu_dac0_fifo_write_data(u32 val_b32);
void pgu_dac1_fifo_write_data(u32 val_b32);
void pgu_dacx_fdcs_run_test();
void pgu_dacx_fdcs_stop_test();
void pgu_dacx_fdcs_write_repeat(u32 val_b32);
u32  pgu_dacx_fdcs_read_repeat();

//}


//// CMU-CPU functions  //{
// based on MCS-EP subfunctions
// common
u32 cmu_read_fpga_image_id();
u32 cmu_monitor_fpga();
u32 cmu_test_counter(u32 opt);

u32 cmu_xxx_init(const u8* msg_title, u32 wi, u32 wo);
u32 cmu_xxx_update(const u8* msg_title, u32 wi, u32 wo);
u32 cmu_xxx_disable(const u8* msg_title, u32 wi, u32 wo);

// SPO
u32 cmu_spo_enable();
u32 cmu_spo_init();
u32 cmu_spo_update();
u32 cmu_spo_set_buffer(u32 spo_idx, u32 val, u32 mask);
u32 cmu_spo_read_buffer(u32 spo_idx);
u32 cmu_spo_disable();
u32 cmu_spo_bit__leds     (u32 opt);
u32 cmu_spo_bit__amp_pwr  (u32 opt);
u32 cmu_spo_bit__adc0_gain(u32 opt);
u32 cmu_spo_bit__adc1_gain(u32 opt);
u32 cmu_spo_bit__vi_bw    (u32 opt);
u32 cmu_spo_bit__vq_bw    (u32 opt);
// BIAS
u32 cmu_dac_bias_enable();
u32 cmu_dac_bias_init();
u32 cmu_dac_bias_update();
u32 cmu_dac_bias_disable();
u32 cmu_dac_bias_set_buffer();
u32 cmu_dac_bias_readback(u32 *p_DACx);
// DWAVE
u32 cmu_dwave_enable();
u32 cmu_dwave_disable();
u32 cmu_dwave_read_base_freq();
u32 cmu_dwave_rd__trig(u32 bit_loc);
u32 cmu_dwave_wr__trig(u32 bit_loc, u32 val);
u32 cmu_dwave_wr_cnt_period(u32 val);
u32 cmu_dwave_rd_cnt_period();
	

//cmu_dwave_wr_cnt_diff()
	//print('>>> {} : {}'.format('Write DWAVE cnt_diff',val))
	//cmu_dwave_wr__trig(17, val)
	//return True

//def cmu_dwave_rd_cnt_diff():
	//print('>>> {}'.format('Read DWAVE cnt_diff'))
	//val = cmu_dwave_rd__trig(25)
	//return val
	
//def cmu_dwave_wr_num_pulses(val):
	//print('>>> {} : {}'.format('Write DWAVE num_pulses',val))
	//cmu_dwave_wr__trig(18, val)
	//return True	
	
//def cmu_dwave_rd_num_pulses():
	//print('>>> {}'.format('Read DWAVE num_pulses'))
	//val = cmu_dwave_rd__trig(26)
	//return val
	

//cmu_dwave_set_para()
	//print('>>> {}'.format('Set DWAVE parameters'))
	//cmu_dwave_wr__trig(3, val=0)
	//return True

//def cmu_dwave_wr_output_dis(val):
	//print('>>> {} : {}'.format('Write DWAVE output_dis',val))
	//cmu_dwave_wr__trig(19, val)
	//return True
	
//def cmu_dwave_rd_output_dis():
	//print('>>> {}'.format('Read DWAVE output_dis'))
	//val = cmu_dwave_rd__trig(27)
	//return val
	
//cmu_dwave_wr_output_dis__enable_all()
	//print('>>>> {}'.format('Enable DWAVE all output'))
	//cmu_dwave_wr_output_dis(0x0)
	//return
	
//cmu_dwave_wr_output_dis__enable_path_i_only()
	//print('>>>> {}'.format('Enable DWAVE only I output'))
	//cmu_dwave_wr_output_dis(0xC)
	//return

//cmu_dwave_wr_output_dis__enable_path_q_only()
	//print('>>>> {}'.format('Enable DWAVE only Q output'))
	//cmu_dwave_wr_output_dis(0x3)
	//return

//def cmu_dwave_wr_output_dis__disable_all():
	//print('>>>> {}'.format('Disable DWAVE all output'))
	//cmu_dwave_wr_output_dis(0xF)
	//return

//def cmu_dwave_pulse_on_num():
	//print('>>> {}'.format('Set DWAVE pulse_on_num'))
	//cmu_dwave_wr__trig(2, val=0)
	//return True

//cmu_dwave_pulse_on()
	//print('>>> {}'.format('Set DWAVE pulse_on'))
	//cmu_dwave_wr__trig(1, val=0)
	//return True

//cmu_dwave_pulse_off()
	//print('>>> {}'.format('Set DWAVE pulse_off'))
	//cmu_dwave_wr__trig(0, val=0)
	//return True




//// ADC

//cmu_adc_enable()
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//print('>> {}'.format('Enable ADC'))
	//#
	//wi = EP_ADRS['ADC_HS_WI']
	//wo = EP_ADRS['ADC_HS_WO']
	//#
	//dev.SetWireInValue(wi,0x00000001,0x00000001) # (ep,val,mask)
	//dev.UpdateWireIns()
	//#
	//dev.UpdateWireOuts()
	//flag = dev.GetWireOutValue(wo)
	//print('{} = {:#010x}'.format('flag',flag))
	//#
	//flag = (flag&0x00000001)
	//#
	//return flag
	
//cmu_adc_reset()
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//print('>> {}'.format('Reset ADC'))
	//#
	//ti = EP_ADRS['ADC_HS_TI']
	//wo = EP_ADRS['ADC_HS_WO']
	//#
	//# trig
	//bit_loc = 0 # for reset
	//ret = dev.ActivateTriggerIn(ti, bit_loc) # (ep,bit) #
	//#
	//# flag
	//dev.UpdateWireOuts()
	//flag = dev.GetWireOutValue(wo)
	//print('{} = {:#010x}'.format('flag',flag))
	//#
	//flag = (flag&0x00000001)
	//#
	//return flag
	
//def cmu_adc_init():
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//print('>> {}'.format('Initialize ADC'))
	//#
	//wi = EP_ADRS['ADC_HS_WI']
	//ti = EP_ADRS['ADC_HS_TI']
	//wo = EP_ADRS['ADC_HS_WO']
	//#
	//# trig
	//dev.ActivateTriggerIn(ti, 1) # (ep,bit)
	//#
	//# check init_done flag
	//cnt_done = 0
	//MAX_CNT = 20000
	//while True:
	//	dev.UpdateWireOuts()
	//	flag = dev.GetWireOutValue(wo)
	//	init_done = (flag&0x00000002)>>1
	//	#print('{} = {:#010x}'.format('flag',flag))
	//	if (init_done==1):
	//		break
	//	cnt_done += 1
	//	if (cnt_done>=MAX_CNT):
	//		break
	//#  
	//print('{} = {}'.format('cnt_done',cnt_done))#
	//print('{} = {}'.format('init_done',init_done))
	//#
	//return init_done	
	
//cmu_adc_read_base_freq()
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//print('>> {}'.format('Read ADC_BASE_FREQ'))
	//#
	//wo = EP_ADRS['ADC_BASE_FREQ']
	//#
	//dev.UpdateWireOuts()
	//ADC_BASE_FREQ = dev.GetWireOutValue(wo)
	//#
	//print('{}: 0x{:08x}'.format('ADC_BASE_FREQ',ADC_BASE_FREQ))
	//#
	//return ADC_BASE_FREQ

//cmu_adc_write_wire_endpoint_dict()
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//print('>> {}'.format('Write endpoint'))
	//#
	//wi = EP_ADRS[idx_str]
	//#
	//dev.SetWireInValue(wi,val,msk) # (ep,val,mask)
	//dev.UpdateWireIns() # option
	//#
	//return wi

//def cmu_adc_set_para (
	//ADC_BASE_FREQ=125000000,FS_TARGET=10416666,ADC_NUM_SAMPLES=131072,
	//ADC_INPUT_DELAY_TAP=10,PIN_TEST_FRC_HIGH=0,PIN_DLLN_FRC_LOW=0,PTTN_CNT_UP_EN=0
	//):
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//print('>> {}'.format('Set ADC parameters'))
	//#
	//# read ADC base frequency @ ADC_BASE_FREQ
	//print('> {}'.format('Read adc base freq'))
	//dev.UpdateWireOuts()
	//adc_base_freq = dev.GetWireOutValue(EP_ADRS['ADC_BASE_FREQ'])
	//print('{}: {:#8.3f} MHz\r'.format('ADC Base Freq',adc_base_freq/1e6))
	//if (ADC_BASE_FREQ!=adc_base_freq):
	//	print('>>> {}: {}'.format('Warning!: ADC Base Freq is not matched',adc_base_freq))
	//#
	//# calculate sampling frequency : Fs from adc_base_freq and FS_TARGET
	//if adc_base_freq==0:
	//	print('>>> {}: {}'.format('Warning!: adc_base_freq is not accepted', 'ADC_BASE_FREQ is used.'))
	//	adc_base_freq = ADC_BASE_FREQ
	//#
	//ADC_CNT_SAMPLE_PERIOD_float = adc_base_freq/FS_TARGET
	//ADC_CNT_SAMPLE_PERIOD = int(ADC_CNT_SAMPLE_PERIOD_float + 0.5) # round-off
	//#
	//if ADC_CNT_SAMPLE_PERIOD==0:
	//	print('>>> {}: {}'.format('Warning!: ADC_CNT_SAMPLE_PERIOD is not accepted', '1 is assigned.'))
	//	ADC_CNT_SAMPLE_PERIOD = 1
	//#
	//Fs = adc_base_freq/ADC_CNT_SAMPLE_PERIOD
	//#
	//print('{}: {:#8.3f} Sps\r'.format('FS_TARGET',FS_TARGET))
	//print('{}: {:#8.3f} Counts\r'.format('ADC_CNT_SAMPLE_PERIOD_float',ADC_CNT_SAMPLE_PERIOD_float))
	//print('{}: {:#8.3f} Counts\r'.format('ADC_CNT_SAMPLE_PERIOD',ADC_CNT_SAMPLE_PERIOD))
	//print('{}: {:#8.3f} Msps\r'.format('Fs',Fs/1e6))	
	//#
	//# set sampling period count @ ADC_HS_SMP_PRD : ADC_CNT_SAMPLE_PERIOD (> 11)
	//#   (125 megahertz) / 12 = 10.4166667 megahertz // best
	//MIN_ADC_CNT_SAMPLE_PERIOD = 12
	//if (ADC_CNT_SAMPLE_PERIOD < MIN_ADC_CNT_SAMPLE_PERIOD):
	//	print('>>> {}'.format('Warning!: ADC_CNT_SAMPLE_PERIOD is below minimun.'))
	//dev.SetWireInValue(EP_ADRS['ADC_HS_SMP_PRD'],ADC_CNT_SAMPLE_PERIOD,0xFFFFFFFF) # (ep,val,mask)
	//dev.UpdateWireIns() # option
	//#
	//# set adc data length to read @ wi1D : ADC_NUM_SAMPLES <= 2^17 = 131072 max 
	//MAX_ADC_NUM_SAMPLES = 2**17
	//if (ADC_NUM_SAMPLES > MAX_ADC_NUM_SAMPLES):
	//	print('>>> {}'.format('Warning!: ADC_NUM_SAMPLES is above maximum.'))
	//dev.SetWireInValue(EP_ADRS['ADC_HS_UPD_SMP'],ADC_NUM_SAMPLES,0xFFFFFFFF) # (ep,val,mask)
	//dev.UpdateWireIns() # must in final setting	#
	//#
	//# //$$ TODO: ADC_INPUT_DELAY_TAP rev
	//# set input delay taps @ ADC_HS_DLY_TAP_OPT[31:27], ADC_HS_DLY_TAP_OPT[26:22]
	//#ADC_INPUT_DELAY_TAP = 31
	//print('{}: {:#8.3f} \r'.format('ADC_INPUT_DELAY_TAP',ADC_INPUT_DELAY_TAP))
	//#
	//#ADC_INPUT_DELAY_TAP_code = (ADC_INPUT_DELAY_TAP<<27)|(ADC_INPUT_DELAY_TAP<<22)
	//#ADC_INPUT_DELAY_TAP_code = (ADC1_INPUT_DELAY_TAP_H<<27)|(ADC1_INPUT_DELAY_TAP_L<<22)|(ADC0_INPUT_DELAY_TAP_H<<17)|(ADC0_INPUT_DELAY_TAP_L<<12)
	//ADC_INPUT_DELAY_TAP_code = (ADC_INPUT_DELAY_TAP<<27)|(ADC_INPUT_DELAY_TAP<<22)|(ADC_INPUT_DELAY_TAP<<17)|(ADC_INPUT_DELAY_TAP<<12)
	//#
	//print('{} = {:#010x}'.format('ADC_INPUT_DELAY_TAP_code',ADC_INPUT_DELAY_TAP_code))#
	//#dev.SetWireInValue(EP_ADRS['ADC_HS_DLY_TAP_OPT'],ADC_INPUT_DELAY_TAP_code,0xFFC00000) # (ep,val,mask)
	//dev.SetWireInValue(EP_ADRS['ADC_HS_DLY_TAP_OPT'],ADC_INPUT_DELAY_TAP_code,0xFFFFF000) # (ep,val,mask)
	//#
	//dev.UpdateWireIns() # must in final setting
	//#
	//# set w_pin_test_frc_high @ ADC_HS_DLY_TAP_OPT[0]
	//# set w_pin_dlln_frc_low  @ ADC_HS_DLY_TAP_OPT[1]
	//# set w_pttn_cnt_up_en    @ ADC_HS_DLY_TAP_OPT[2]
	//ADC_control_code = (PTTN_CNT_UP_EN<<2)|(PIN_DLLN_FRC_LOW<<1)|PIN_TEST_FRC_HIGH
	//print('{} = {:#010x}'.format('ADC_control_code',ADC_control_code))#
	//dev.SetWireInValue(EP_ADRS['ADC_HS_DLY_TAP_OPT'],ADC_control_code,0x00000007) # (ep,val,mask)
	//dev.UpdateWireIns() # must in final setting
	//#	
	//return True

//cmu_check_adc_test_pattern()
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//print('>> {}'.format('Check ADC test pattern'))
	//#
	//wi = EP_ADRS['ADC_HS_WI']
	//ti = EP_ADRS['ADC_HS_TI']
	//wo = EP_ADRS['ADC_HS_WO']
	//#
	//po_d0 = EP_ADRS['ADC_HS_DOUT0_PO']
	//po_d1 = EP_ADRS['ADC_HS_DOUT1_PO']
	//#
	//# read FIFO data generated by ADC initialization
	//print('> {}'.format('Read data from FIFO'))
	//#
	//ADC_NUM_SAMPLES = 4 
	//#
	//dataout0  = bytearray([0] * ADC_NUM_SAMPLES*4) # 4 word = 4 * 4 byte 
	//dataout1  = bytearray([0] * ADC_NUM_SAMPLES*4) # 4 word = 4 * 4 byte 
	//#
	//data_count0 = dev.ReadFromPipeOut(po_d0, dataout0)
	//data_count1 = dev.ReadFromPipeOut(po_d1, dataout1)
	//#
	//print('{}: {}'.format('data_count0 [byte]',data_count0))
	//print('{}: {}'.format('data_count1 [byte]',data_count1))
	//#
	//# convert 32-bit data into 18-bit adc_code
	//adc0_list_int = []
	//adc1_list_int = []
	//for ii in range(0,ADC_NUM_SAMPLES):
	//	# note unsigned shift in python ... see int.from_bytes(..., signed=True) for signed shift
	//	temp_data0 = int.from_bytes(dataout0[ii*4:ii*4+4], byteorder='little', signed=True)
	//	temp_data1 = int.from_bytes(dataout1[ii*4:ii*4+4], byteorder='little', signed=True)
	//	adc0_list_int += [temp_data0>>14]
	//	adc1_list_int += [temp_data1>>14]
	//#
	//print('{}: {:#010x}'.format('adc0_list_int[0]&0x3FFFF',adc0_list_int[0]&0x3FFFF))
	//print('{}: {:#010x}'.format('adc0_list_int[1]&0x3FFFF',adc0_list_int[1]&0x3FFFF))
	//print('{}: {:#010x}'.format('adc0_list_int[2]&0x3FFFF',adc0_list_int[2]&0x3FFFF))
	//print('{}: {:#010x}'.format('adc0_list_int[3]&0x3FFFF',adc0_list_int[3]&0x3FFFF))
	//print('{}: {:#010x}'.format('adc1_list_int[0]&0x3FFFF',adc1_list_int[0]&0x3FFFF))
	//print('{}: {:#010x}'.format('adc1_list_int[1]&0x3FFFF',adc1_list_int[1]&0x3FFFF))
	//print('{}: {:#010x}'.format('adc1_list_int[2]&0x3FFFF',adc1_list_int[2]&0x3FFFF))
	//print('{}: {:#010x}'.format('adc1_list_int[3]&0x3FFFF',adc1_list_int[3]&0x3FFFF))	
	//#
	//# check adc data by initialization (4 data of 0x330FC) // 209148 = 0x330FC
	//chk_adcX_count_mismatch_pattern = 0
	//for ii in range(0,ADC_NUM_SAMPLES):
	//	chk_adcX_count_mismatch_pattern += int((adc0_list_int[ii])&0x0003FFFF!=ADC_TEST_PATTERN)
	//	chk_adcX_count_mismatch_pattern += int((adc1_list_int[ii])&0x0003FFFF!=ADC_TEST_PATTERN)
	//print('{} = {}'.format('chk_adcX_count_mismatch_pattern',chk_adcX_count_mismatch_pattern))#
	//#
	//ret = True
	//#
	//if (chk_adcX_count_mismatch_pattern!=0):
	//	print('>>> {}'.format('Warning!: adc test patterns are mismatched. need to check HW.'))
	//	ret = False	
	//#
	//return ret
	
//cmu_adc_is_fifo_empty()
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//print('>> {}'.format('Is FIFO empty?'))
	//#
	//wi = EP_ADRS['ADC_HS_WI']
	//ti = EP_ADRS['ADC_HS_TI']
	//wo = EP_ADRS['ADC_HS_WO']
	//to = EP_ADRS['ADC_HS_TO']
	//#
	//# read FIFO flag
	//dev.UpdateWireOuts()
	//flag = dev.GetWireOutValue(wo)
	//#
	//fifo_adc0_empty = (flag&(0x00000001<<9 ))>>9
	//fifo_adc1_empty = (flag&(0x00000001<<15))>>15
	//#
	//print('{} = {}'.format('fifo_adc0_empty',fifo_adc0_empty))#
	//print('{} = {}'.format('fifo_adc1_empty',fifo_adc1_empty))#
	//#
	// chk_fifo_adcX_all_empty = fifo_adc0_empty * fifo_adc1_empty;
	//#
	//print('{} = {}'.format('chk_fifo_adcX_all_empty',chk_fifo_adcX_all_empty))#
	//ret = True
	//if (chk_fifo_adcX_all_empty!=1):
	//	print('>>> {}'.format('Warning!: all fifo are not empty.'))
	//	ret = False
	//#
	//return ret
	
//cmu_adc_update()
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//print('>> {}'.format('Update ADC samples'))
	//#
	//wi = EP_ADRS['ADC_HS_WI']
	//ti = EP_ADRS['ADC_HS_TI']
	//wo = EP_ADRS['ADC_HS_WO']
	//to = EP_ADRS['ADC_HS_TO']
	//#	
	//# trig
	//dev.ActivateTriggerIn(ti, 2) # (ep,bit)
	//#
	//# check update_done flag
	//cnt_done = 0
	//MAX_CNT = 20000
	//while True:
	//	dev.UpdateWireOuts()
	//	flag = dev.GetWireOutValue(wo)
	//	update_done = (flag&0x00000004)>>2
	//	#print('{} = {:#010x}'.format('flag',flag))
	//	if (update_done==1):
	//		break
	//	cnt_done += 1
	//	if (cnt_done>=MAX_CNT):
	//		break
	//#  
	//print('{} = {}'.format('cnt_done',cnt_done))#
	//print('{} = {}'.format('update_done',update_done))
	//#
	//return update_done	#
	
//cmu_adc_load_from_fifo() //$$ to mem or to LAN-fifo or to test-fifo ...
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//print('>> {}'.format('Load ADC data into memory'))
	//#
	//wi = EP_ADRS['ADC_HS_WI']
	//ti = EP_ADRS['ADC_HS_TI']
	//wo = EP_ADRS['ADC_HS_WO']
	//#
	//po_d0 = EP_ADRS['ADC_HS_DOUT0_PO']
	//po_d1 = EP_ADRS['ADC_HS_DOUT1_PO']
	//#
	//# init byte array
	//dataout0  = bytearray([0] * ADC_NUM_SAMPLES*4) # ADC_NUM_SAMPLES word = ADC_NUM_SAMPLES * 4 byte 
	//dataout1  = bytearray([0] * ADC_NUM_SAMPLES*4) # ADC_NUM_SAMPLES word = ADC_NUM_SAMPLES * 4 byte 
	//#
	//data_count0 = dev.ReadFromPipeOut(po_d0, dataout0)
	//data_count1 = dev.ReadFromPipeOut(po_d1, dataout1)
	//#
	//print('{}: {}'.format('data_count0 [byte]',data_count0))
	//print('{}: {}'.format('data_count1 [byte]',data_count1))
	//#
	//# convert 32-bit data into 18-bit adc_code
	//adc0_list_int = []
	//adc1_list_int = []
	//for ii in range(0,ADC_NUM_SAMPLES):
	//	# note unsigned shift in python ... see int.from_bytes(..., signed=True) for signed shift
	//	temp_data0 = int.from_bytes(dataout0[ii*4:ii*4+4], byteorder='little', signed=True)
	//	temp_data1 = int.from_bytes(dataout1[ii*4:ii*4+4], byteorder='little', signed=True)
	//	adc0_list_int += [temp_data0>>14]
	//	adc1_list_int += [temp_data1>>14]
	//#
	//return [adc0_list_int, adc1_list_int]
	
//cmu_adc_disable()
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//print('>> {}'.format('Disable ADC'))
	//#
	//wi = EP_ADRS['ADC_HS_WI']
	//wo = EP_ADRS['ADC_HS_WO']
	//#
	//dev.SetWireInValue(wi,0x00000000,0x00000001) # (ep,val,mask)
	//dev.UpdateWireIns()
	//#
	//dev.UpdateWireOuts()
	//flag = dev.GetWireOutValue(wo)
	//print('{} = {:#010x}'.format('flag',flag))
	//#
	//flag = (flag&0x00000001)
	//#
	//return flag

//}


//// SCPI exec functions // based on MCS-EP subfunctions



#endif
