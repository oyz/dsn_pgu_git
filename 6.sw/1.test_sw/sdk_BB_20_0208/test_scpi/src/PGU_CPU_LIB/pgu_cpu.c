//#include "xparameters.h"
//#include "xil_cache.h"
#include "microblaze_sleep.h"

//$$ PGU
#include "pgu_cpu.h"

#include "ioLibrary/Ethernet/W5500/w5500.h" // for w5500 io functions
//
#define __W5500_SPI_VDM_OP_          0x00
#define __W5500_SPI_FDM_OP_LEN1_     0x01
#define __W5500_SPI_FDM_OP_LEN2_     0x02
#define __W5500_SPI_FDM_OP_LEN4_     0x03


// common subfunctions //{
u32 hexchr2data_u32(u8 hexchr) {
	// '0' -->  0
	// 'A' --> 10
	u32 val;
	s32 val_L;
	s32 val_H;
	//
	val_L = (s32)hexchr - (s32)'0';
	//if ((val_L>=0)&&(val_L<10)) {
	if (val_L<10) {
		val = (u32)val_L;
	}
	else {
		val_H = (s32)hexchr - (s32)'A';
		//if ((val_H>=0)&&(val_H<6)) {
		//	val = (u32)val_H+10;
		// }
		//else {
		//	val = (u32)(-1); // no hex code.
		// }
		val = (u32)val_H+10;
	}
	//
	return val; 
}

u32 hexstr2data_u32(u8* hexstr, u32 len) {
	u32 val;
	u32 loc;
	u32 ii;
	loc = 0;
	val = 0;
	for (ii=0;ii<len;ii++) {
		val = (val<<4) + hexchr2data_u32(hexstr[loc++]);
	}
	return val;
}

u32 decchr2data_u32(u8 decchr) {
	// '0' -->  0
	u32 val;
	s32 val_t;
	//
	val_t = (s32)decchr - (s32)'0';
	if (val_t<10) {
		val = (u32)val_t;
	}
	else {
		val = (u32)(-1); // no valid code.
	}
	//
	return val; 
}

u32 decstr2data_u32(u8* decstr, u32 len) {
	u32 val;
	u32 loc;
	u32 ii;
	loc = 0;
	val = 0;
	for (ii=0;ii<len;ii++) {
		val = (val*10) + decchr2data_u32(decstr[loc++]);
	}
	return val;
}

u32 is_dec_char(u8 chr) { // 0 for dec; -1 for not
	u32 ret;
	ret = decchr2data_u32(chr);
	if (ret != -1)  ret = 0;
	return ret;
}

//}


// subfunctions for pipe //{

void dcopy_pipe8_to_buf8  (u32 adrs_p8, u8 *p_buf_u8, u32 len) {
	u32 ii;
	u8 val;
	//
	for (ii=0;ii<len;ii++) {
		val = (u8)XIomodule_In32 (adrs_p8);
		p_buf_u8[ii] = val;
	}
}
	
void dcopy_buf8_to_pipe8  (u8 *p_buf_u8, u32 adrs_p8, u32 len) {
	u32 ii;
	u32 val;
	//
	for (ii=0;ii<len;ii++) {
		val = p_buf_u8[ii]&0x000000FF;
		XIomodule_Out32 (adrs_p8, val);
	}
}

void dcopy_pipe32_to_buf32(u32 adrs_p32, u32 *p_buf_u32, u32 len_byte) { // (src,dst,len)
	u32 ii;
	u32 len;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		p_buf_u32[ii] = XIomodule_In32 (adrs_p32);
	}
}

void dcopy_buf32_to_pipe32(u32 *p_buf_u32, u32 adrs_p32, u32 len_byte) { // (src,dst,len)
	u32 ii;
	u32 val;
	u32 len;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		val = p_buf_u32[ii];
		XIomodule_Out32 (adrs_p32, val);
	}
}

void dcopy_buf8_to_pipe32(u8 *p_buf_u8, u32 adrs_p32, u32 len_byte) { // (src,dst,len_byte)
	u32 ii;
	u32 val;
	//u32 *p_val;
	u8  *p_val_u8;
	u32 len;
	//
	//p_val = (u32*)p_buf_u8; //$$ NG ...  MCS IO mem on buf8 -> buf32 location has some constraint.
	p_val_u8 = (u8*)&val;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		p_val_u8[0] = p_buf_u8[ii*4+0];
		p_val_u8[1] = p_buf_u8[ii*4+1];
		p_val_u8[2] = p_buf_u8[ii*4+2];
		p_val_u8[3] = p_buf_u8[ii*4+3];
		//
		//xil_printf("check: 0x%08X\r\n",(unsigned int)val); //$$ test
		//
		XIomodule_Out32 (adrs_p32, val);
	}
}

void dcopy_pipe32_to_pipe32(u32 src_adrs_p32, u32 dst_adrs_p32, u32 len_byte) { // (src,dst,len)
	u32 ii;
	u32 len;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		XIomodule_Out32 (dst_adrs_p32, XIomodule_In32 (src_adrs_p32));
	}
}

void dcopy_pipe8_to_pipe32 (u32 src_adrs_p8,  u32 dst_adrs_p32, u32 len_byte) { // (src,dst,len)
	// src_adrs_p8          dst_adrs_p32
	// {AA},{BB},{CC},{DD}  {DD,CC,BB,AA}   
	u32 ii;
	u32 len;
	u32 val;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		val = 0   +  ((XIomodule_In32 (src_adrs_p8))&0x000000FF)     ;
		val = val + (((XIomodule_In32 (src_adrs_p8))&0x000000FF)<< 8);
		val = val + (((XIomodule_In32 (src_adrs_p8))&0x000000FF)<<16);
		val = val + (((XIomodule_In32 (src_adrs_p8))&0x000000FF)<<24);
		XIomodule_Out32 (dst_adrs_p32, val);
	}
}

void dcopy_pipe32_to_pipe8 (u32 src_adrs_p32, u32 dst_adrs_p8,  u32 len_byte) { // (src,dst,len)
	// src_adrs_p32    dst_adrs_p8
	// {DD,CC,BB,AA}   {AA},{BB},{CC},{DD}
	u32 ii;
	u32 len;
	u32 val;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		val = XIomodule_In32 (src_adrs_p32);
		XIomodule_Out32 (dst_adrs_p8, (val&0x000000FF));
		val = val >> 8;
		XIomodule_Out32 (dst_adrs_p8, (val&0x000000FF));
		val = val >> 8;
		XIomodule_Out32 (dst_adrs_p8, (val&0x000000FF));
		val = val >> 8;
		XIomodule_Out32 (dst_adrs_p8, (val&0x000000FF));
	}
}

//}


// wiznet 850io functions //{

u8 hw_reset__wz850() {
	u32 adrs;
	u32 value;
	
	//// trig LAN RESET @  master_spi_wz850
	//xil_printf(">>> trig LAN RESET @  master_spi_wz850: \n\r");
	//
	adrs = ADRS_PORT_WO_20; // {...,w_LAN_INTn,w_LAN_SSNn,w_LAN_RSTn,w_LAN_valid}
	value = XIomodule_In32 (adrs);
	//xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x00000001;
	XIomodule_Out32 (adrs, value);
	//xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
	//
	adrs = ADRS_PORT_WO_20; // {...,w_LAN_INTn,w_LAN_SSNn,w_LAN_RSTn,w_LAN_valid}
	value = XIomodule_In32 (adrs);
	//xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x00000000;
	XIomodule_Out32 (adrs, value);
	//xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
	//
		while (1) {
		//
		adrs = ADRS_PORT_WO_20; // {...,w_LAN_INTn,w_LAN_SSNn,w_LAN_RSTn,w_LAN_valid}
		value = XIomodule_In32 (adrs);
		//xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
		//
		if ((value & 0x03) == 0x03) break;
		//
		//usleep(100); // 1us*100 sleep
		//
		}
	//
	return 0;
}

// for  uint8_t  WIZCHIP_READ(uint32_t AddrSel)
u8    read_data__wz850 (u32 AddrSel) {
	u8 ret;
	u32 adrs;
	u32 value;

	// set up mode
	AddrSel |= (_W5500_SPI_READ_ | __W5500_SPI_VDM_OP_);

	//// set up frame
	adrs = ADRS_PORT_WI_01;
	value = AddrSel&0x00FFFFFF;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_02;
	value = 1;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0002; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0000; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
		while (1) {
		//
		adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		value = XIomodule_In32 (adrs);
		//
		if ((value & 0x10) == 0x10) break;
		//
		//usleep(100); // 1us*100 sleep
		//
		}
	//

	//// read fifo data
	adrs = ADRS_PORT_PO_A0;
	value = XIomodule_In32 (adrs);
	//
	ret = value & (0x000000ff);
	return ret;
}

// for  void WIZCHIP_WRITE(uint32_t AddrSel, uint8_t wb )
void write_data__wz850 (u32 AddrSel, u8 wb) {
	u32 adrs;
	u32 value;

	// set up mode
	AddrSel |= (_W5500_SPI_WRITE_ | __W5500_SPI_VDM_OP_);

	// write fifo data
	adrs = ADRS_PORT_PI_80;
	value = 0x000000FF & wb;
	XIomodule_Out32 (adrs, value);

	//// set up frame
	adrs = ADRS_PORT_WI_01;
	value = AddrSel&0x00FFFFFF;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_02;
	value = 1;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0002; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0000; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
		while (1) {
		//
		adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		value = XIomodule_In32 (adrs);
		//
		if ((value & 0x10) == 0x10) break;
		//
		//usleep(100); // 1us*100 sleep
		//
		}
	//
}

// for  void WIZCHIP_READ_BUF (uint32_t AddrSel, uint8_t* pBuf, uint16_t len)
// read data from LAN-fifo to buffer  // recv
void  read_data_buf__wz850 (u32 AddrSel, u8* pBuf, u16 len) {
	u32 adrs;
	u32 value;

	// set up mode
	AddrSel |= (_W5500_SPI_READ_ | __W5500_SPI_VDM_OP_);

	//// set up frame
	adrs = ADRS_PORT_WI_01;
	value = AddrSel&0x00FFFFFF;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_02;
	value = len;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0002; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0000; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
	while (1) {
		//
		adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		value = XIomodule_In32 (adrs);
		//
		if ((value & 0x10) == 0x10) break;
		//
		//usleep(100); // 1us*100 sleep
		//
		}
	//

	//// read fifo data : dcopy pipe_8b to buf_8b
	dcopy_pipe8_to_buf8  (ADRS_PORT_PO_A0, pBuf, len);
	//
}

// for  void WIZCHIP_WRITE_BUF(uint32_t AddrSel, uint8_t* pBuf, uint16_t len)
// write data from buffer to LAN-fifo // send
void write_data_buf__wz850 (u32 AddrSel, u8* pBuf, u16 len) {
	u32 adrs;
	u32 value;
	
	// set up mode
	AddrSel |= (_W5500_SPI_WRITE_ | __W5500_SPI_VDM_OP_);

	// write buffer data to LAN-fifo : dcopy buf_8b to pipe_8b
	dcopy_buf8_to_pipe8(pBuf, ADRS_PORT_PI_80, len); 
	
	//// set up frame
	adrs = ADRS_PORT_WI_01;
	value = AddrSel&0x00FFFFFF;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_02;
	value = len;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0002; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0000; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
	while (1) {
		//
		adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		value = XIomodule_In32 (adrs);
		//
		if ((value & 0x10) == 0x10) break;
		//
		//usleep(100); // 1us*100 sleep
		//
		}
	//	
}

// FROM w5500.c : WIZCHIP_READ_BUF
// FROM w5500.c : void wiz_recv_data(uint8_t sn, uint8_t *wizdata, uint16_t len)
// FROM socket.c: int32_t recv(uint8_t sn, uint8_t * buf, uint16_t len)
// FROM socket.c: int32_t recvfrom(uint8_t sn, uint8_t * buf, uint16_t len, uint8_t * addr, uint16_t *port)
// ...
// TO    WIZCHIP_READ_PIPE // test 
// TO    void wiz_recv_data_pipe(uint8_t sn, uint8_t *wizdata, uint32_t len_u32)
// TO    int32_t recv_pipe(uint8_t sn, uint8_t * buf, uint32_t len_u32)
// TO    int32_t recvfrom_pipe(uint8_t sn, uint8_t * buf, uint32_t len_u32, uint8_t * addr, uint16_t *port)
//
// read data from LAN-fifo to pipe // recv
void  read_data_pipe__wz850 (u32 AddrSel, u32 ep_offset, u32 len_u32) { // test
	u32 adrs;
	u32 value;
	u32 ii;
	//
	u32 len_4byte;
	u32 data_u32;
	u8  *p_data_u8;
	
	// set up mode
	AddrSel |= (_W5500_SPI_READ_ | __W5500_SPI_VDM_OP_);

 	// length based on 4 bytes 
	len_4byte = len_u32>>2; // len_u32 must be muliple of 4.

	//// set up frame
	adrs = ADRS_PORT_WI_01;
	value = AddrSel&0x00FFFFFF;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_02;
	value = len_u32; //$$
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0002; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0000; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
		while (1) {
		//
		adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		value = XIomodule_In32 (adrs);
		//
		if ((value & 0x10) == 0x10) break;
		//
		//usleep(100); // 1us*100 sleep
		//
		}
	//

	//// read fifo data from LAN to pipe 
	//
	// address of pipe end-point
	adrs = ADRS_BASE_CMU + (ep_offset<<4); // MCS1 //$$
	//
	p_data_u8 = (u8*)&data_u32;
	for (ii=0;ii<len_4byte;ii++) {
		value = XIomodule_In32 (ADRS_PORT_PO_A0); // read data0 from LAN-fifo  
		p_data_u8[0] = (u8) (value & 0x00FF);
		//
		value = XIomodule_In32 (ADRS_PORT_PO_A0); // read data1 from LAN-fifo  
		p_data_u8[1] = (u8) (value & 0x00FF);
		//
		value = XIomodule_In32 (ADRS_PORT_PO_A0); // read data2 from LAN-fifo  
		p_data_u8[2] = (u8) (value & 0x00FF);
		//
		value = XIomodule_In32 (ADRS_PORT_PO_A0); // read data3 from LAN-fifo  
		p_data_u8[3] = (u8) (value & 0x00FF);
		//
		XIomodule_Out32 (adrs, data_u32);// write a 32-bit value to pipe end-point
	}
	//
}

// FROM w5500.c : WIZCHIP_WRITE_BUF
// FROM w5500.c : void wiz_send_data(uint8_t sn, uint8_t *wizdata, uint16_t len)
// FROM socket.c: int32_t send(uint8_t sn, uint8_t * buf, uint16_t len)
// FROM socket.c: int32_t sendto(uint8_t sn, uint8_t * buf, uint16_t len, uint8_t * addr, uint16_t port)
// ...
// TO    WIZCHIP_WRITE_PIPE // test  
// TO    void wiz_send_data_pipe(uint8_t sn, uint8_t *wizdata, uint32_t len_u32)
// TO    int32_t send_pipe(uint8_t sn, uint8_t * buf, uint32_t len_u32)
// TO    int32_t sendto_pipe(uint8_t sn, uint8_t * buf, uint32_t len_u32, uint8_t * addr, uint16_t port)
//
// write data from pipe to LAN-fifo // send
void write_data_pipe__wz850 (u32 AddrSel, u32 src_adrs_p32, u32 len_u32) { 
	u32 adrs;
	u32 value;
	u32 ii;
	//
	u32 len_4byte;
	u32 data_u32;
	u8  *p_data_u8;
	
	// set up LAN spi mode
	AddrSel |= (_W5500_SPI_WRITE_ | __W5500_SPI_VDM_OP_);
	
	// address of pipe end-point
	//adrs = ADRS_BASE_CMU + (ep_offset<<4); // MCS1 

 	// length based on 4 bytes 
	len_4byte = len_u32>>2; // len_u32 must be muliple of 4.
	
	// note that 32 bits from pipe-out and 8 bits into LAN-fifo 
	// need to send 4 bytes byte-by-byte ...
	p_data_u8 = (u8*)&data_u32;
	for (ii=0;ii<len_4byte;ii++) {
		//// read from data pipe such as adc pipe
		data_u32 = XIomodule_In32 (src_adrs_p32); // read a 32-bit value from pipe end-point
		//...
		//// write the value to LAN pipe end-point
		// send data in the order of ... p_data_u8[0], p_data_u8[1], p_data_u8[2], p_data_u8[3]
		value = 0x000000FF & p_data_u8[0];
		XIomodule_Out32 (ADRS_PORT_PI_80, value); // write data0 to LAN-fifo  
		//
		value = 0x000000FF & p_data_u8[1];
		XIomodule_Out32 (ADRS_PORT_PI_80, value); // write data1 to LAN-fifo  
		//
		value = 0x000000FF & p_data_u8[2];
		XIomodule_Out32 (ADRS_PORT_PI_80, value); // write data2 to LAN-fifo  
		//
		value = 0x000000FF & p_data_u8[3];
		XIomodule_Out32 (ADRS_PORT_PI_80, value); // write data3 to LAN-fifo  
		
	}
	
	//// set up frame
	adrs = ADRS_PORT_WI_01;
	value = AddrSel&0x00FFFFFF;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_02;
	value = len_u32; //$$ 
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0002; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0000; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
		while (1) {
		//
		adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		value = XIomodule_In32 (adrs);
		//
		if ((value & 0x10) == 0x10) break;
		//
		//usleep(100); // 1us*100 sleep
		//
		}
	// 	
}

//}


// === MCS io access functions === //{
u32   read_mcs_fpga_img_id(u32 adrs_base) {
	//u32 adrs;
	//u32 value;
	//adrs = ADRS_FPGA_IMAGE_CMU;
	//value = XIomodule_In32 (adrs);
	//return value;
	//return XIomodule_In32 (ADRS_FPGA_IMAGE_CMU);
	return XIomodule_In32 (adrs_base + ADRS_FPGA_IMAGE_OFST);
}

u32   read_mcs_test_reg(u32 adrs_base) {
	//return XIomodule_In32 (ADRS_TEST_REG___CMU);
	return XIomodule_In32 (adrs_base + ADRS_TEST_REG___OFST);
}

void write_mcs_test_reg(u32 adrs_base, u32 data) {
	//XIomodule_Out32 (ADRS_TEST_REG___CMU, data);
	XIomodule_Out32 (adrs_base + ADRS_TEST_REG___OFST, data);
}

//}


// === MCS-EP access functions === //{

// * common subfunctions : //{
// note that mask should be careful... MCS and LAN as well...

void  enable_mcs_ep() {
	//adrs = ADRS_PORT_WI_10;
	//value = 0x0000003F;
	//XIomodule_Out32 (adrs, value);
	XIomodule_Out32 (ADRS_MASK_WI,    0x0000003F); // write mask
	XIomodule_Out32 (ADRS_PORT_WI_10, 0x0000003F); // write MCS0
	XIomodule_Out32 (ADRS_MASK_WI,      MASK_ALL); // reset mask
}

void disable_mcs_ep() {
	//adrs = ADRS_PORT_WI_10;
	//value = 0x00000000;
	//XIomodule_Out32 (adrs, value);
	XIomodule_Out32 (ADRS_MASK_WI,    0x0000003F); // write mask
	XIomodule_Out32 (ADRS_PORT_WI_10, 0x00000000); // write MCS0
	XIomodule_Out32 (ADRS_MASK_WI,      MASK_ALL); // reset mask
}

void  reset_mcs_ep() {
	XIomodule_Out32 (ADRS_MASK_WI,    0x00010000); // write mask
	XIomodule_Out32 (ADRS_PORT_WI_10, 0x00010000); // write MCS0
	usleep(100);
	XIomodule_Out32 (ADRS_PORT_WI_10, 0x00000000); // write MCS0
	XIomodule_Out32 (ADRS_MASK_WI,      MASK_ALL); // reset mask
}

void  reset_io_dev() {
	// adc / dwave / bias / spo
	XIomodule_Out32 (ADRS_MASK_WI,    0x001E0000); // write mask
	XIomodule_Out32 (ADRS_PORT_WI_10, 0x001E0000); // write MCS0
	usleep(100);
	XIomodule_Out32 (ADRS_PORT_WI_10, 0x00000000); // write MCS0
	XIomodule_Out32 (ADRS_MASK_WI,      MASK_ALL); // reset mask
}


u32 is_enabled_mcs_ep() {
	u32 val;
	u32 ret;
	val = XIomodule_In32 (ADRS_PORT_WI_10); // assume MASK_ALL
	if ((val&0x003F)==0x003F) {
		ret = 1;
	}
	else ret = 0;
	return ret;
}

//}

// * io-mask subfunctions : //{

u32  read_mcs_ep_wi_mask(u32 adrs_base) {
	//return XIomodule_In32 (ADRS_MASK_WI____CMU);
	return XIomodule_In32 (adrs_base + ADRS_MASK_WI____OFST);
}

u32 write_mcs_ep_wi_mask(u32 adrs_base, u32 mask) {
	//XIomodule_Out32 (ADRS_MASK_WI____CMU, mask); // mask 
	XIomodule_Out32 (adrs_base + ADRS_MASK_WI____OFST, mask); // mask 
	return mask;
}

u32  read_mcs_ep_wo_mask(u32 adrs_base) {
	//return XIomodule_In32 (ADRS_MASK_WO____CMU);
	return XIomodule_In32 (adrs_base + ADRS_MASK_WO____OFST);
}

u32 write_mcs_ep_wo_mask(u32 adrs_base, u32 mask) {
	//XIomodule_Out32 (ADRS_MASK_WO____CMU, mask); // mask 
	XIomodule_Out32 (adrs_base + ADRS_MASK_WO____OFST, mask); // mask 
	return mask;
}

u32  read_mcs_ep_ti_mask(u32 adrs_base) {
	//return XIomodule_In32 (ADRS_MASK_TI____CMU);
	return XIomodule_In32 (adrs_base + ADRS_MASK_TI____OFST);
}

u32 write_mcs_ep_ti_mask(u32 adrs_base, u32 mask) {
	//XIomodule_Out32 (ADRS_MASK_TI____CMU, mask); // mask 
	XIomodule_Out32 (adrs_base + ADRS_MASK_TI____OFST, mask); // mask 
	return mask;
}

u32  read_mcs_ep_to_mask(u32 adrs_base) {
	//return XIomodule_In32 (ADRS_MASK_TO____CMU);
	return XIomodule_In32 (adrs_base + ADRS_MASK_TO____OFST);
}

u32 write_mcs_ep_to_mask(u32 adrs_base, u32 mask) {
	//XIomodule_Out32 (ADRS_MASK_TO____CMU, mask); // mask 
	XIomodule_Out32 (adrs_base + ADRS_MASK_TO____OFST, mask); // mask 
	return mask;
}


u32  read_mcs_ep_xx_data(u32 adrs_base, u32 offset) {
	u32 adrs;
	u32 value;
	//adrs = ADRS_BASE_CMU + (offset<<4); // MCS1
	adrs = adrs_base + (offset<<4); // MCS1
	value = XIomodule_In32 (adrs);
	return value;	
}

u32 write_mcs_ep_xx_data(u32 adrs_base, u32 offset, u32 data) {
	u32 adrs;
	//adrs = ADRS_BASE_CMU + (offset<<4); // MCS1
	adrs = adrs_base + (offset<<4); // MCS1
	XIomodule_Out32 (adrs, data);
	return data;
}


u32  read_mcs_ep_wi_data(u32 adrs_base, u32 offset) {
	return read_mcs_ep_xx_data(adrs_base, offset);
}

u32 write_mcs_ep_wi_data(u32 adrs_base, u32 offset, u32 data) {
	return write_mcs_ep_xx_data(adrs_base, offset, data);
}

u32  read_mcs_ep_wo_data(u32 adrs_base, u32 offset) {
	return read_mcs_ep_xx_data(adrs_base, offset);
}

// u32 write_mcs_ep_wo_data(u32 offset, u32 data) // NA

u32  read_mcs_ep_ti_data(u32 adrs_base, u32 offset) {
	return read_mcs_ep_xx_data(adrs_base, offset);
}

u32 write_mcs_ep_ti_data(u32 adrs_base, u32 offset, u32 data) {
	return write_mcs_ep_xx_data(adrs_base, offset, data);
}

u32  read_mcs_ep_to_data(u32 adrs_base, u32 offset) {
	return read_mcs_ep_xx_data(adrs_base, offset);
}

// u32 write_mcs_ep_to_data(u32 offset, u32 data) // NA


//// PIPE-DATA direct

// u32  read_mcs_ep_pi_data() // NA

u32 write_mcs_ep_pi_data(u32 adrs_base, u32 offset, u32 data) { // OK
	return write_mcs_ep_xx_data(adrs_base, offset, data);
}

u32  read_mcs_ep_po_data(u32 adrs_base, u32 offset) { // OK
	return read_mcs_ep_xx_data(adrs_base, offset);
}

// u32 write_mcs_ep_po_data() // NA

//}

// * end-point functions : //{

u32   read_mcs_ep_wi(u32 adrs_base, u32 offset) {
	return read_mcs_ep_wi_data(adrs_base, offset);
}

void write_mcs_ep_wi(u32 adrs_base, u32 offset, u32 data, u32 mask) {
	write_mcs_ep_wi_mask(adrs_base, mask);
	write_mcs_ep_wi_data(adrs_base, offset, data);
}

u32   read_mcs_ep_wo(u32 adrs_base, u32 offset, u32 mask) {
	write_mcs_ep_wo_mask(adrs_base, mask);
	return read_mcs_ep_wo_data(adrs_base, offset);
}

u32   read_mcs_ep_ti(u32 adrs_base, u32 offset) {
	return read_mcs_ep_ti_data(adrs_base, offset);
}

void write_mcs_ep_ti(u32 adrs_base, u32 offset, u32 data, u32 mask) {
	write_mcs_ep_ti_mask(adrs_base, mask);
	write_mcs_ep_ti_data(adrs_base, offset, data);
}

void activate_mcs_ep_ti(u32 adrs_base, u32 offset, u32 bit_loc) { 
	u32 value;
	value = 0x00000001 << bit_loc;
	write_mcs_ep_ti(adrs_base, offset, value, value);
}

u32   read_mcs_ep_to(u32 adrs_base, u32 offset, u32 mask) {
	write_mcs_ep_to_mask(adrs_base, mask);
	return read_mcs_ep_to_data(adrs_base, offset);
}

u32 is_triggered_mcs_ep_to(u32 adrs_base, u32 offset, u32 mask) {
	u32 ret;
	u32 value;
	//
	value = read_mcs_ep_to(adrs_base, offset, mask);
	if (value==0) ret = 0;
	else ret = 1;
	return ret;
}


//// PIPE-DATA in buffer 

// write data from buffer to pipe-in 
u32 write_mcs_ep_pi_buf(u32 adrs_base, u32 offset, u32 len_byte, u8 *p_data) {
	u32 adrs;
	//
	//adrs = ADRS_BASE_CMU + (offset<<4); // MCS1
	adrs = adrs_base + (offset<<4); // MCS1
	dcopy_buf32_to_pipe32((u32*)p_data, adrs, len_byte);
	//
	return (len_byte&0xFFFFFFFC);
}

// read data from pipe-out to buffer 
u32  read_mcs_ep_po_buf(u32 adrs_base, u32 offset, u32 len_byte, u8 *p_data) { 
	u32 adrs;
	//
	//adrs = ADRS_BASE_CMU + (offset<<4); // MCS1
	adrs = adrs_base + (offset<<4); // MCS1
	dcopy_pipe32_to_buf32(adrs, (u32*)p_data, len_byte);
	//
	return (len_byte&0xFFFFFFFC);
}


//// PIPE-DATA in fifo 

// write data from fifo to pipe-in 
// u32 write_mcs_ep_pi_fifo(u32 offset, u32 len_byte, u8 *p_data) { // test
// 	return 0;
// }

// read data from pipe-out to fifo 
// u32  read_mcs_ep_po_fifo(u32 offset, u32 len_byte, u8 *p_data) { // test
// 	return 0;
// }

//}

//}

// NOTE:
//
//  GetWireOutValue
//  UpdateWireOuts 
//  SetWireInValue
//  UpdateWireIns // dummy
//  ActivateTriggerIn // ActivateTriggerIn(int epAddr, int bit)
//  UpdateTriggerOuts // not much // dummy
//  IsTriggered // not much // IsTriggered(int epAddr, UINT32 mask)
//  ReadFromPipeOut
//  WriteToPipeIn // not much
//  // https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel-members.html

//// PGU-CPU functions //{
	
u32 pgu_test(u32 opt) {
	return opt;
}

// SPIO //{

u32  pgu_spio_send_spi_frame(u32 frame_data) {
	//dev = lib_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//print('>> {}'.format('Send SPIO frame'))
	//#
	//wi = EP_ADRS['SPIO_WI']
	//wo = EP_ADRS['SPIO_WO']
	//ti = EP_ADRS['SPIO_TI']
	//#
	//#frame_data = (ctrl_b16<<16) + val_b16
	//print('{} = 0x{:08X}'.format('frame_data',frame_data))#
	//#
	//# write control 
	//dev.SetWireInValue(wi,frame_data,0xFFFFFFFF) # (ep,val,mask)
	write_mcs_ep_wi(ADRS_BASE_PGU, EP_ADRS__SPIO_WI__PGU, frame_data, MASK_ALL);
	//dev.UpdateWireIns()
	//#
	//# trig spi frame
	//#   wire w_trig_SPIO_SPI_frame = w_SPIO_TI[1];
	//ret = dev.ActivateTriggerIn(ti, 1) # (ep,bit) 
	activate_mcs_ep_ti(ADRS_BASE_PGU, EP_ADRS__SPIO_TI__PGU, 1);
	//#
	//# check spi frame done
	//#   assign w_SPIO_WO[25] = w_done_SPIO_SPI_frame;
	u32 cnt_done = 0    ;
	u32 MAX_CNT  = 20000;
	u32 bit_loc  = 25   ;
	u32 flag;
	u32 flag_done;
	//while True:
	while (1) {
		//dev.UpdateWireOuts()
		//flag = dev.GetWireOutValue(wo)
		flag = read_mcs_ep_wo(ADRS_BASE_PGU, EP_ADRS__SPIO_WO__PGU, MASK_ALL);
		
		flag_done = (flag&(1<<bit_loc))>>bit_loc;
		//#print('{} = {:#010x}'.format('flag',flag))
		if (flag_done==1)
			break;
		cnt_done += 1;
		if (cnt_done>=MAX_CNT)
			break;
	}
	//#  
	//print('{} = {}'.format('cnt_done',cnt_done))#
	//print('{} = {}'.format('flag_done',flag_done))
	//#
	//# read received data 
	//#   assign w_SPIO_WO[15:8] = w_SPIO_rd_DA;
	//#   assign w_SPIO_WO[ 7:0] = w_SPIO_rd_DB;
	u32 val_recv = flag & 0x0000FFFF;
	//#
	//print('{} = 0x{:02X}'.format('val_recv',val_recv))#
	//#
	return val_recv;
}

u32  pgu_sp_1_reg_read_b16(u32 reg_adrs_b8) {
	//#
	u32 val_b16 =0;
	//
	u32 CS_id      = 1;
	u32 pin_adrs_A = 0; 
	u32 R_W_bar    = 1;
	u32 reg_adrs_A = reg_adrs_b8;
	//#
	u32 framedata = (CS_id<<28) + (pin_adrs_A<<25) + (R_W_bar<<24) + (reg_adrs_A<<16) + val_b16;
	//#
	return pgu_spio_send_spi_frame(framedata);
}

u32  pgu_sp_1_reg_write_b16(u32 reg_adrs_b8, u32 val_b16) {
	//#
	u32 CS_id      = 1;
	u32 pin_adrs_A = 0;
	u32 R_W_bar    = 0;
	u32 reg_adrs_A = reg_adrs_b8;
	//#
	u32 framedata = (CS_id<<28) + (pin_adrs_A<<25) + (R_W_bar<<24) + (reg_adrs_A<<16) + val_b16;
	//#
	return pgu_spio_send_spi_frame(framedata);
}

void pgu_spio_ext_pwr_led(u32 led, u32 pwr_dac, u32 pwr_adc, u32 pwr_amp) {
	//
	//u32 dir_read;
	u32 lat_read;
	//
	//# read IO direction 
	//# check IO direction : 0xFFX0 where (SPA,SPB)
	//dir_read = pgu_sp_1_reg_read_b16(0x00); // unused
	//print('>>>{} = {}'.format('dir_read',form_hex_32b(dir_read)))
	//# read output Latch
	lat_read = pgu_sp_1_reg_read_b16(0x14);
	//print('>>>{} = {}'.format('lat_read',form_hex_32b(lat_read)))
	//# set IO direction for SP1 PB[3:0] - all output
	pgu_sp_1_reg_write_b16(0x00,0xFFF0);
	//# set IO for SP1 PB[3:0]
	u32 val = (lat_read & 0xFFF0) | ( (led<<3) + (pwr_dac<<2) + (pwr_adc<<1) + (pwr_amp<<0));
	pgu_sp_1_reg_write_b16(0x12,val);
}

u32  pgu_spio_ext_pwr_led_readback() {
	//
	u32 lat_read;
	//
	//# read output Latch
	lat_read = pgu_sp_1_reg_read_b16(0x14);
	//
	return lat_read & 0x000F;
}

//}

// CLKD //{

// reset signal to CLKD device
u32  pgu_clkd_init() {
	//
	//ret = dev.ActivateTriggerIn(ti, 0) # (ep,bit) 
	activate_mcs_ep_ti(ADRS_BASE_PGU, EP_ADRS__CLKD_TI__PGU, 0);
	//
	u32 cnt_done = 0    ;
	u32 MAX_CNT  = 20000;
	u32 bit_loc  = 24   ;
	u32 flag            ;
	u32 flag_done       ;
	//
	while (1) {
		//flag = dev.GetWireOutValue(wo)
		flag = read_mcs_ep_wo(ADRS_BASE_PGU, EP_ADRS__CLKD_WO__PGU, MASK_ALL);
		flag_done = (flag&(1<<bit_loc))>>bit_loc;
		if (flag_done==1)
			break;
		cnt_done += 1;
		if (cnt_done>=MAX_CNT)
			break;
	}
	//
	return flag_done;
}

// clkd_send_spi_frame
u32  pgu_clkd_send_spi_frame(u32 frame_data) {
	//
	// write control 
	write_mcs_ep_wi(ADRS_BASE_PGU, EP_ADRS__CLKD_WI__PGU, frame_data, MASK_ALL);
	//
	// trig spi frame
	activate_mcs_ep_ti(ADRS_BASE_PGU, EP_ADRS__CLKD_TI__PGU, 1);
	//
	// check spi frame done
	u32 cnt_done = 0    ;
	u32 MAX_CNT  = 20000;
	u32 bit_loc  = 25   ;
	u32 flag;
	u32 flag_done;
	// check if done is low // when sclk is slow < 1MHz
	while (1) {
		//
		flag = read_mcs_ep_wo(ADRS_BASE_PGU, EP_ADRS__CLKD_WO__PGU, MASK_ALL);
		flag_done = (flag&(1<<bit_loc))>>bit_loc;
		//
		if (flag_done==0)
			break;
		cnt_done += 1;
		if (cnt_done>=MAX_CNT)
			break;
	}
	// check if done is high
	while (1) {
		//
		flag = read_mcs_ep_wo(ADRS_BASE_PGU, EP_ADRS__CLKD_WO__PGU, MASK_ALL);
		flag_done = (flag&(1<<bit_loc))>>bit_loc;
		//
		if (flag_done==1)
			break;
		cnt_done += 1;
		if (cnt_done>=MAX_CNT)
			break;
	}
	//
	// copy received data
	u32 val_recv = flag & 0x000000FF;
	//
	return val_recv;
}

// clkd_reg_write_b8
u32  pgu_clkd_reg_write_b8(u32 reg_adrs_b10, u32 val_b8) {
	//
	u32 R_W_bar     = 0           ;
	u32 byte_mode_W = 0x0         ;
	u32 reg_adrs    = reg_adrs_b10;
	u32 val         = val_b8      ;
	//
	u32 framedata = (R_W_bar<<31) + (byte_mode_W<<29) + (reg_adrs<<16) + val;
	//
	return pgu_clkd_send_spi_frame(framedata);
}

// clkd_reg_read_b8
u32  pgu_clkd_reg_read_b8(u32 reg_adrs_b10) {
	//
	u32 R_W_bar     = 1           ;
	u32 byte_mode_W = 0x0         ;
	u32 reg_adrs    = reg_adrs_b10;
	u32 val         = 0xFF        ;
	//
	u32 framedata = (R_W_bar<<31) + (byte_mode_W<<29) + (reg_adrs<<16) + val;
	//
	return pgu_clkd_send_spi_frame(framedata);
}

// write check 
u32  pgu_clkd_reg_write_b8_check (u32 reg_adrs_b10, u32 val_b8) {
	u32 tmp;
	u32 retry_count = 0;
	while(1) {
		// write 
		pgu_clkd_reg_write_b8(reg_adrs_b10, val_b8);
		// readback
		tmp = pgu_clkd_reg_read_b8(reg_adrs_b10); // readback 0x18
		if (tmp == val_b8) 
			break;
		retry_count++;
	}
	return retry_count;
}

// read check 
u32  pgu_clkd_reg_read_b8_check (u32 reg_adrs_b10, u32 val_b8) {
	u32 tmp;
	u32 retry_count = 0;
	while(1) {
		// read
		tmp = pgu_clkd_reg_read_b8(reg_adrs_b10); // readback 0x18
		if (tmp == val_b8) 
			break;
		retry_count++;
	}
	return retry_count;
}

// setup CLKD 
u32  pgu_clkd_setup(u32 freq_preset) {
	u32 ret = freq_preset;
	u32 tmp = 0;
	
	// write conf : SDO active 0x99
	tmp += pgu_clkd_reg_write_b8_check(0x000,0x99);

	// read conf 
	//tmp = pgu_clkd_reg_read_b8_check(0x000, 0x18); // readback 0x18
	tmp += pgu_clkd_reg_read_b8_check(0x000, 0x99); // readback 0x99
	
	// read ID
	tmp += pgu_clkd_reg_read_b8_check(0x003, 0x41); // read ID 0x41 
	
	
	// power down for output ports
	// ## LVPECL outputs:
	// ##   0x0F0 OUT0 ... 0x0A for power down; 0x08 for power up.
	// ##   0x0F1 OUT1 ... 0x0A for power down; 0x08 for power up.
	// ##   0x0F2 OUT2 ... 0x0A for power down; 0x08 for power up. // TO DAC 
	// ##   0x0F3 OUT3 ... 0x0A for power down; 0x08 for power up. // TO DAC 
	// ##   0x0F4 OUT4 ... 0x0A for power down; 0x08 for power up.
	// ##   0x0F5 OUT5 ... 0x0A for power down; 0x08 for power up.
	// ## LVDS outputs:
	// ##   0x140 OUT6 ... 0x43 for power down; 0x42 for power up. // TO REF OUT
	// ##   0x141 OUT7 ... 0x43 for power down; 0x42 for power up.
	// ##   0x142 OUT8 ... 0x43 for power down; 0x42 for power up. // TO FPGA
	// ##   0x143 OUT9 ... 0x43 for power down; 0x42 for power up.
	// ##
	tmp += pgu_clkd_reg_write_b8_check(0x0F0,0x0A);
	tmp += pgu_clkd_reg_write_b8_check(0x0F1,0x0A);
	tmp += pgu_clkd_reg_write_b8_check(0x0F2,0x0A);
	tmp += pgu_clkd_reg_write_b8_check(0x0F3,0x0A);
	tmp += pgu_clkd_reg_write_b8_check(0x0F4,0x0A);
	tmp += pgu_clkd_reg_write_b8_check(0x0F5,0x0A);
	// ##
	tmp += pgu_clkd_reg_write_b8_check(0x140,0x43);
	tmp += pgu_clkd_reg_write_b8_check(0x141,0x43);
	tmp += pgu_clkd_reg_write_b8_check(0x142,0x43);
	tmp += pgu_clkd_reg_write_b8_check(0x143,0x43);

	// update registers // no readback
	pgu_clkd_reg_write_b8(0x232,0x01); 
	//
	
	//// clock distribution setting
	tmp += pgu_clkd_reg_write_b8_check(0x010,0x7D); //# PLL power-down
	
	if (freq_preset == 4000) { // 400MHz // OK
		//# 400MHz common = 400MHz/1
		tmp += pgu_clkd_reg_write_b8_check(0x1E1,0x01); //# Bypass VCO divider # for 400MHz common clock 
		//
		tmp += pgu_clkd_reg_write_b8_check(0x194,0x80); //# DVD1 bypass --> DACx: ()/1 
		tmp += pgu_clkd_reg_write_b8_check(0x19C,0x30); //# DVD3.1, DVD3.2 all bypass --> REFo: ()/1
		tmp += pgu_clkd_reg_write_b8_check(0x1A1,0x30); //# DVD4.1, DVD4.2 all bypass --> FPGA: ()/1 = 400MHz
	}
	else if (freq_preset == 2000) { // 200MHz // OK
		//# 200MHz common = 400MHz/(2+0)
		tmp += pgu_clkd_reg_write_b8_check(0x1E0,0x00); //# Set VCO divider # [0,1,2,3,4] for [/2,/3,/4,/5,/6]
		tmp += pgu_clkd_reg_write_b8_check(0x1E1,0x00); //# Use VCO divider # for 400MHz/X common clock 
		// ()/1
		tmp += pgu_clkd_reg_write_b8_check(0x194,0x80); //# DVD1 bypass --> DACx: ()/1 
		tmp += pgu_clkd_reg_write_b8_check(0x19C,0x30); //# DVD3.1, DVD3.2 all bypass --> REFo: ()/1
		tmp += pgu_clkd_reg_write_b8_check(0x1A1,0x30); //# DVD4.1, DVD4.2 all bypass --> FPGA: ()/1 = 400MHz
	}
	else if (freq_preset == 1000) { // 100MHz // OK
		//# 100MHz common = 400MHz/(2+2)
		tmp += pgu_clkd_reg_write_b8_check(0x1E0,0x02); //# Set VCO divider # [0,1,2,3,4] for [/2,/3,/4,/5,/6]
		tmp += pgu_clkd_reg_write_b8_check(0x1E1,0x00); //# Use VCO divider # for 400MHz/X common clock 
		// ()/1
		tmp += pgu_clkd_reg_write_b8_check(0x194,0x80); //# DVD1 bypass --> DACx: ()/1 
		tmp += pgu_clkd_reg_write_b8_check(0x19C,0x30); //# DVD3.1, DVD3.2 all bypass --> REFo: ()/1
		tmp += pgu_clkd_reg_write_b8_check(0x1A1,0x30); //# DVD4.1, DVD4.2 all bypass --> FPGA: ()/1 = 400MHz
	}
	else if (freq_preset == 800) { // 80MHz //OK
		//# 80MHz common = 400MHz/(2+3)
		tmp += pgu_clkd_reg_write_b8_check(0x1E0,0x03); //# Set VCO divider # [0,1,2,3,4] for [/2,/3,/4,/5,/6]
		tmp += pgu_clkd_reg_write_b8_check(0x1E1,0x00); //# Use VCO divider # for 400MHz/X common clock 
		// ()/1
		tmp += pgu_clkd_reg_write_b8_check(0x194,0x80); //# DVD1 bypass --> DACx: ()/1 
		tmp += pgu_clkd_reg_write_b8_check(0x19C,0x30); //# DVD3.1, DVD3.2 all bypass --> REFo: ()/1
		tmp += pgu_clkd_reg_write_b8_check(0x1A1,0x30); //# DVD4.1, DVD4.2 all bypass --> FPGA: ()/1 = 400MHz
	}
	else if (freq_preset == 500) { // 50MHz //OK
		//# 200MHz common = 400MHz/(2+0)
		tmp += pgu_clkd_reg_write_b8_check(0x1E0,0x00); //# Set VCO divider # [0,1,2,3,4] for [/2,/3,/4,/5,/6]
		tmp += pgu_clkd_reg_write_b8_check(0x1E1,0x00); //# Use VCO divider # for 400MHz/X common clock 
		// ()/4
		tmp += pgu_clkd_reg_write_b8_check(0x193,0x11); //# DVD1 div 2+1+1=4 --> DACx: ()/4 
		tmp += pgu_clkd_reg_write_b8_check(0x194,0x00); //# DVD1 bypass off 
		tmp += pgu_clkd_reg_write_b8_check(0x199,0x00); //# DVD3.1 div 2+0+0=2 
		tmp += pgu_clkd_reg_write_b8_check(0x19B,0x00); //# DVD3.2 div 2+0+0=2  --> REFo: ()/4
		tmp += pgu_clkd_reg_write_b8_check(0x19C,0x00); //# DVD3.1, DVD3.2 all bypass off
		tmp += pgu_clkd_reg_write_b8_check(0x19E,0x00); //# DVD4.1 div 2+0+0=2 
		tmp += pgu_clkd_reg_write_b8_check(0x1A0,0x00); //# DVD4.2 div 2+0+0=2  --> FPGA: ()/4
		tmp += pgu_clkd_reg_write_b8_check(0x1A1,0x00); //# DVD4.1, DVD4.2 all bypass off
	}
	else if (freq_preset == 200) { // 20MHz //OK
		//# 80MHz common = 400MHz/(2+3)
		tmp += pgu_clkd_reg_write_b8_check(0x1E0,0x03); //# Set VCO divider # [0,1,2,3,4] for [/2,/3,/4,/5,/6]
		tmp += pgu_clkd_reg_write_b8_check(0x1E1,0x00); //# Use VCO divider # for 400MHz/X common clock 
		// ()/4  
		tmp += pgu_clkd_reg_write_b8_check(0x193,0x11); //# DVD1 div 2+1+1=4 --> DACx: ()/4 
		tmp += pgu_clkd_reg_write_b8_check(0x194,0x00); //# DVD1 bypass off 
		tmp += pgu_clkd_reg_write_b8_check(0x199,0x00); //# DVD3.1 div 2+0+0=2 
		tmp += pgu_clkd_reg_write_b8_check(0x19B,0x00); //# DVD3.2 div 2+0+0=2  --> REFo: ()/4
		tmp += pgu_clkd_reg_write_b8_check(0x19C,0x00); //# DVD3.1, DVD3.2 all bypass off
		tmp += pgu_clkd_reg_write_b8_check(0x19E,0x00); //# DVD4.1 div 2+0+0=2 
		tmp += pgu_clkd_reg_write_b8_check(0x1A0,0x00); //# DVD4.2 div 2+0+0=2  --> FPGA: ()/4
		tmp += pgu_clkd_reg_write_b8_check(0x1A1,0x00); //# DVD4.1, DVD4.2 all bypass off
	}
	//  else if (freq_preset == 100) { // 10MHz //NG
	//  	//# 80MHz common = 400MHz/(2+3)
	//  	tmp += pgu_clkd_reg_write_b8_check(0x1E0,0x03); //# Set VCO divider # [0,1,2,3,4] for [/2,/3,/4,/5,/6]
	//  	tmp += pgu_clkd_reg_write_b8_check(0x1E1,0x00); //# Use VCO divider # for 400MHz/X common clock 
	//  	// ()/8
	//  	tmp += pgu_clkd_reg_write_b8_check(0x193,0x33); //# DVD1 div 2+3+3=8 --> DACx: ()/8
	//  	tmp += pgu_clkd_reg_write_b8_check(0x194,0x00); //# DVD1 bypass off 
	//  	tmp += pgu_clkd_reg_write_b8_check(0x199,0x00); //# DVD3.1 div 2+0+0=2 
	//  	tmp += pgu_clkd_reg_write_b8_check(0x19B,0x11); //# DVD3.2 div 2+1+1=4  --> REFo: ()/8
	//  	tmp += pgu_clkd_reg_write_b8_check(0x19C,0x00); //# DVD3.1, DVD3.2 all bypass off
	//  	tmp += pgu_clkd_reg_write_b8_check(0x19E,0x00); //# DVD4.1 div 2+0+0=2 
	//  	tmp += pgu_clkd_reg_write_b8_check(0x1A0,0x11); //# DVD4.2 div 2+1+1=4  --> FPGA: ()/8
	//  	tmp += pgu_clkd_reg_write_b8_check(0x1A1,0x00); //# DVD4.1, DVD4.2 all bypass off
	//  }
	//  else if (freq_preset == 50) { // 5MHz //NG
	//  	//# 80MHz common = 400MHz/(2+3)
	//  	tmp += pgu_clkd_reg_write_b8_check(0x1E0,0x03); //# Set VCO divider # [0,1,2,3,4] for [/2,/3,/4,/5,/6]
	//  	tmp += pgu_clkd_reg_write_b8_check(0x1E1,0x00); //# Use VCO divider # for 400MHz/X common clock 
	//  	// ()/16 
	//  	tmp += pgu_clkd_reg_write_b8_check(0x193,0x77); //# DVD1 div 2+7+7=16 --> DACx: ()/16
	//  	tmp += pgu_clkd_reg_write_b8_check(0x194,0x00); //# DVD1 bypass off 
	//  	tmp += pgu_clkd_reg_write_b8_check(0x199,0x00); //# DVD3.1 div 2+0+0=2 
	//  	tmp += pgu_clkd_reg_write_b8_check(0x19B,0x33); //# DVD3.2 div 2+3+3=8  --> REFo: ()/16
	//  	tmp += pgu_clkd_reg_write_b8_check(0x19C,0x00); //# DVD3.1, DVD3.2 all bypass off
	//  	tmp += pgu_clkd_reg_write_b8_check(0x19E,0x00); //# DVD4.1 div 2+0+0=2 
	//  	tmp += pgu_clkd_reg_write_b8_check(0x1A0,0x33); //# DVD4.2 div 2+3+3=8  --> FPGA: ()/16
	//  	tmp += pgu_clkd_reg_write_b8_check(0x1A1,0x00); //# DVD4.1, DVD4.2 all bypass off
	//  }
	//  else if (freq_preset == 25) { // 2.5MHz // NG
	//  	//# 80MHz common = 400MHz/(2+3)
	//  	tmp += pgu_clkd_reg_write_b8_check(0x1E0,0x03); //# Set VCO divider # [0,1,2,3,4] for [/2,/3,/4,/5,/6]
	//  	tmp += pgu_clkd_reg_write_b8_check(0x1E1,0x00); //# Use VCO divider # for 400MHz/X common clock 
	//  	// ()/32
	//  	tmp += pgu_clkd_reg_write_b8_check(0x193,0xFF); //# DVD1 div 2+15+15=32 --> DACx: ()/32
	//  	tmp += pgu_clkd_reg_write_b8_check(0x194,0x00); //# DVD1 bypass off 
	//  	tmp += pgu_clkd_reg_write_b8_check(0x199,0x11); //# DVD3.1 div 2+1+1=4 
	//  	tmp += pgu_clkd_reg_write_b8_check(0x19B,0x33); //# DVD3.2 div 2+3+3=8  --> REFo: ()/32
	//  	tmp += pgu_clkd_reg_write_b8_check(0x19C,0x00); //# DVD3.1, DVD3.2 all bypass off
	//  	tmp += pgu_clkd_reg_write_b8_check(0x19E,0x11); //# DVD4.1 div 2+1+1=4 
	//  	tmp += pgu_clkd_reg_write_b8_check(0x1A0,0x33); //# DVD4.2 div 2+3+3=8  --> FPGA: ()/32
	//  	tmp += pgu_clkd_reg_write_b8_check(0x1A1,0x00); //# DVD4.1, DVD4.2 all bypass off
	//  }
	else {
		// return 0
		ret = 0;
		//# 200MHz common = 400MHz/(2+0)
		tmp += pgu_clkd_reg_write_b8_check(0x1E0,0x00); //# Set VCO divider # [0,1,2,3,4] for [/2,/3,/4,/5,/6]
		tmp += pgu_clkd_reg_write_b8_check(0x1E1,0x00); //# Use VCO divider # for 400MHz/X common clock 
		// ()/1
		tmp += pgu_clkd_reg_write_b8_check(0x194,0x80); //# DVD1 bypass --> DACx: ()/1 
		tmp += pgu_clkd_reg_write_b8_check(0x19C,0x30); //# DVD3.1, DVD3.2 all bypass --> REFo: ()/1
		tmp += pgu_clkd_reg_write_b8_check(0x1A1,0x30); //# DVD4.1, DVD4.2 all bypass --> FPGA: ()/1 = 400MHz
	}
	
	// power up for clock outs
	tmp += pgu_clkd_reg_write_b8_check(0x0F0,0x0A);
	tmp += pgu_clkd_reg_write_b8_check(0x0F1,0x0A);
	tmp += pgu_clkd_reg_write_b8_check(0x0F2,0x08); //$$ power up
	tmp += pgu_clkd_reg_write_b8_check(0x0F3,0x08); //$$ power up
	tmp += pgu_clkd_reg_write_b8_check(0x0F4,0x0A);
	tmp += pgu_clkd_reg_write_b8_check(0x0F5,0x0A);
	// ##
	tmp += pgu_clkd_reg_write_b8_check(0x140,0x42); //$$ power up
	tmp += pgu_clkd_reg_write_b8_check(0x141,0x43);
	tmp += pgu_clkd_reg_write_b8_check(0x142,0x42); //$$ power up
	tmp += pgu_clkd_reg_write_b8_check(0x143,0x43);
	
	//// readbacks
	//pgu_clkd_reg_read_b8(0x1E0);
	//pgu_clkd_reg_read_b8(0x1E1);
	//pgu_clkd_reg_read_b8(0x193);
	//pgu_clkd_reg_read_b8(0x194);
	//pgu_clkd_reg_read_b8(0x199);
	//pgu_clkd_reg_read_b8(0x19B);
	//pgu_clkd_reg_read_b8(0x19C);
	//pgu_clkd_reg_read_b8(0x19E);
	//pgu_clkd_reg_read_b8(0x1A0);
	//pgu_clkd_reg_read_b8(0x1A1);
	
	
	// update registers // no readback
	pgu_clkd_reg_write_b8(0x232,0x01); 
	
	// check if retry count > 0
	if (tmp>0) {
		ret = 0;
	}
	
	return ret;
}

//}


// DACX //{

// dacx_init
u32  pgu_dacx_init() {
	//
	//ret = dev.ActivateTriggerIn(ti, 0) # (ep,bit) 
	activate_mcs_ep_ti(ADRS_BASE_PGU, EP_ADRS__DACX_TI__PGU, 0);
	//
	u32 cnt_done = 0    ;
	u32 MAX_CNT  = 20000;
	u32 bit_loc  = 24   ;
	u32 flag            ;
	u32 flag_done       ;
	//
	while (1) {
		//flag = dev.GetWireOutValue(wo)
		flag = read_mcs_ep_wo(ADRS_BASE_PGU, EP_ADRS__DACX_WO__PGU, MASK_ALL);
		flag_done = (flag&(1<<bit_loc))>>bit_loc;
		if (flag_done==1)
			break;
		cnt_done += 1;
		if (cnt_done>=MAX_CNT)
			break;
	}
	//
	return flag_done;
}

// dacx_fpga_pll_rst
u32  pgu_dacx_fpga_pll_rst(u32 clkd_out_rst, u32 dac0_dco_rst, u32 dac1_dco_rst) {
	u32 control_data;
	u32 status_pll;
	//
	// control data
	control_data = (dac1_dco_rst<<30) + (dac0_dco_rst<<29) + (clkd_out_rst<<28);
	// write control 
	write_mcs_ep_wi(ADRS_BASE_PGU, EP_ADRS__DACX_WI__PGU, control_data, 0x70000000);
	// read status
	//   assign w_TEST_IO_MON[31] = S_IO_2; //
	//   assign w_TEST_IO_MON[30] = S_IO_1; //
	//   assign w_TEST_IO_MON[29] = S_IO_0; //
	//   assign w_TEST_IO_MON[28:27] =  2'b0;
	//   assign w_TEST_IO_MON[26] = dac1_dco_clk_locked;
	//   assign w_TEST_IO_MON[25] = dac0_dco_clk_locked;
	//   assign w_TEST_IO_MON[24] = clk_dac_locked;
	//
	//   assign w_TEST_IO_MON[23:20] =  4'b0;
	//   assign w_TEST_IO_MON[19] = clk4_locked;
	//   assign w_TEST_IO_MON[18] = clk3_locked;
	//   assign w_TEST_IO_MON[17] = clk2_locked;
	//   assign w_TEST_IO_MON[16] = clk1_locked;
	//
	//   assign w_TEST_IO_MON[15: 0] = 16'b0;	
	status_pll = read_mcs_ep_wo(ADRS_BASE_PGU, EP_ADRS__TEST_IO_MON__PGU, 0x07000000);
	//
	return status_pll;
}

//  # pll reset 
//  def dacx_fpga_pll_rst(clkd_out_rst=0, dac0_dco_rst=0, dac1_dco_rst=0):
//  	dev = lib_ctrl.dev
//  	EP_ADRS = conf.OK_EP_ADRS_CONFIG
//  	#
//  	#assign dac1_dco_clk_rst = w_DACX_WI[30];
//  	#assign dac0_dco_clk_rst = w_DACX_WI[29];
//  	#assign  clk_dac_clk_rst = w_DACX_WI[28];
//  	#
//  	print('>> {}'.format('Update FPGA DACX pll reset bits'))
//  	#
//  	wi = EP_ADRS['DACX_WI']
//  	#wo = EP_ADRS['DACX_WO']
//  	#ti = EP_ADRS['DACX_TI']
//  	#
//  	control_data = (dac1_dco_rst<<30) + (dac0_dco_rst<<29) + (clkd_out_rst<<28)
//  	#
//  	print('{} = 0x{:08X}'.format('control_data',control_data))#
//  	#
//  	# write control 
//  	dev.SetWireInValue(wi,control_data,0x70000000) # (ep,val,mask)
//  	dev.UpdateWireIns()
//  	#
//  	return None
//  	

// dacx_send_spi_frame
u32  pgu_dacx_send_spi_frame(u32 frame_data) {
	//
	// write control 
	write_mcs_ep_wi(ADRS_BASE_PGU, EP_ADRS__DACX_WI__PGU, frame_data, MASK_ALL);
	//
	// trig spi frame
	activate_mcs_ep_ti(ADRS_BASE_PGU, EP_ADRS__DACX_TI__PGU, 1);
	//
	// check spi frame done
	u32 cnt_done = 0    ;
	u32 MAX_CNT  = 20000;
	u32 bit_loc  = 25   ;
	u32 flag;
	u32 flag_done;
	//while True:
	while (1) {
		//
		flag = read_mcs_ep_wo(ADRS_BASE_PGU, EP_ADRS__DACX_WO__PGU, MASK_ALL);
		flag_done = (flag&(1<<bit_loc))>>bit_loc;
		//
		if (flag_done==1)
			break;
		cnt_done += 1;
		if (cnt_done>=MAX_CNT)
			break;
	}
	//
	u32 val_recv = flag & 0x000000FF;
	//
	return val_recv;
}

// dac0_reg_write_b8
u32  pgu_dac0_reg_write_b8(u32 reg_adrs_b5, u32 val_b8) {
	//
	u32 CS_id       = 0          ;
	u32 R_W_bar     = 0          ;
	u32 byte_mode_N = 0x0        ;
	u32 reg_adrs    = reg_adrs_b5;
	u32 val         = val_b8     ;
	//
	u32 framedata = (CS_id<<24) + (R_W_bar<<23) + (byte_mode_N<<21) + (reg_adrs<<16) + val;
	//
	return pgu_dacx_send_spi_frame(framedata);
}

// dac0_reg_read_b8
u32  pgu_dac0_reg_read_b8(u32 reg_adrs_b5) {
	//
	u32 CS_id       = 0          ;
	u32 R_W_bar     = 1          ;
	u32 byte_mode_N = 0x0        ;
	u32 reg_adrs    = reg_adrs_b5;
	u32 val         = 0xFF       ;
	//
	u32 framedata = (CS_id<<24) + (R_W_bar<<23) + (byte_mode_N<<21) + (reg_adrs<<16) + val;
	//
	return pgu_dacx_send_spi_frame(framedata);
}

// dac1_reg_write_b8
u32  pgu_dac1_reg_write_b8(u32 reg_adrs_b5, u32 val_b8) {
	//
	u32 CS_id       = 1          ;
	u32 R_W_bar     = 0          ;
	u32 byte_mode_N = 0x0        ;
	u32 reg_adrs    = reg_adrs_b5;
	u32 val         = val_b8     ;
	//
	u32 framedata = (CS_id<<24) + (R_W_bar<<23) + (byte_mode_N<<21) + (reg_adrs<<16) + val;
	//
	return pgu_dacx_send_spi_frame(framedata);
}

// dac1_reg_read_b8
u32  pgu_dac1_reg_read_b8(u32 reg_adrs_b5) {
	//
	u32 CS_id       = 1          ;
	u32 R_W_bar     = 1          ;
	u32 byte_mode_N = 0x0        ;
	u32 reg_adrs    = reg_adrs_b5;
	u32 val         = 0xFF       ;
	//
	u32 framedata = (CS_id<<24) + (R_W_bar<<23) + (byte_mode_N<<21) + (reg_adrs<<16) + val;
	//
	return pgu_dacx_send_spi_frame(framedata);
}

// setup DACX 
u32  pgu_dacx_setup() {

	// # pulse path  : full scale 28.1mA  @ 0x02D0 <<<<<< 21.6V / 13.5ns = 1600 V/us // best with 14V supply
	// pgu.dac0_reg_write_b8(0x0F,0xD0)
	// pgu.dac0_reg_write_b8(0x10,0x02)
	// pgu.dac0_reg_write_b8(0x0B,0xD0)
	// pgu.dac0_reg_write_b8(0x0C,0x02)
	pgu_dac0_reg_write_b8(0x0F,0xD0);
	pgu_dac0_reg_write_b8(0x10,0x02);
	pgu_dac0_reg_write_b8(0x0B,0xD0);
	pgu_dac0_reg_write_b8(0x0C,0x02);
	// #
	// pgu.dac1_reg_write_b8(0x0F,0xD0)
	// pgu.dac1_reg_write_b8(0x10,0x02)
	// pgu.dac1_reg_write_b8(0x0B,0xD0)
	// pgu.dac1_reg_write_b8(0x0C,0x02)
	pgu_dac1_reg_write_b8(0x0F,0xD0);
	pgu_dac1_reg_write_b8(0x10,0x02);
	pgu_dac1_reg_write_b8(0x0B,0xD0);
	pgu_dac1_reg_write_b8(0x0C,0x02);
	// 
	// # offset DAC : 0x140 0.625mA, AUX2N active[7] (1) , sink current[6] (1) <<< offset 1.81mV
	// pgu.dac0_reg_write_b8(0x11,0x40)
	// pgu.dac0_reg_write_b8(0x12,0xC1)
	// pgu.dac0_reg_write_b8(0x0D,0x40)
	// pgu.dac0_reg_write_b8(0x0E,0xC1)
	pgu_dac0_reg_write_b8(0x11,0x40);
	pgu_dac0_reg_write_b8(0x12,0xC1);
	pgu_dac0_reg_write_b8(0x0D,0x40);
	pgu_dac0_reg_write_b8(0x0E,0xC1);
	// #
	// pgu.dac1_reg_write_b8(0x11,0x40)
	// pgu.dac1_reg_write_b8(0x12,0xC1)
	// pgu.dac1_reg_write_b8(0x0D,0x40)
	// pgu.dac1_reg_write_b8(0x0E,0xC1)
	pgu_dac1_reg_write_b8(0x11,0x40);
	pgu_dac1_reg_write_b8(0x12,0xC1);
	pgu_dac1_reg_write_b8(0x0D,0x40);
	pgu_dac1_reg_write_b8(0x0E,0xC1);

	
	return 0;
}

//}


// DACX_PG //{

// subfunctions: dacx_dat_write
void pgu_dacx_dat_write(u32 dacx_dat, u32 bit_loc_trig) {
	// write control 
	//dev.SetWireInValue(wi,dacx_dat,0xFFFFFFFF) # (ep,val,mask)	
	write_mcs_ep_wi(ADRS_BASE_PGU, EP_ADRS__DACX_DAT_WI__PGU, dacx_dat, MASK_ALL);
	//
	// trig
	activate_mcs_ep_ti(ADRS_BASE_PGU, EP_ADRS__DACX_DAT_TI__PGU, bit_loc_trig);
}

// subfunctions: dacx_dat_read
u32  pgu_dacx_dat_read(u32 bit_loc_trig) {
	// trig
	activate_mcs_ep_ti(ADRS_BASE_PGU, EP_ADRS__DACX_DAT_TI__PGU, bit_loc_trig);
	//
	return read_mcs_ep_wo(ADRS_BASE_PGU, EP_ADRS__DACX_DAT_WO__PGU, MASK_ALL);
}


// dacx_dcs_write_adrs
void pgu_dacx_dcs_write_adrs(u32 adrs) {
	pgu_dacx_dat_write(adrs, 16);
}

// dacx_dcs_read_adrs
u32  pgu_dacx_dcs_read_adrs() {
	return pgu_dacx_dat_read(17);
}

// dacx_dcs_write_data_dac0
void pgu_dacx_dcs_write_data_dac0(u32 val_b32) {
	pgu_dacx_dat_write(val_b32, 18);
}

// dacx_dcs_read_data_dac0
u32  pgu_dacx_dcs_read_data_dac0() {
	return pgu_dacx_dat_read(19);
}

// dacx_dcs_write_data_dac1
void pgu_dacx_dcs_write_data_dac1(u32 val_b32) {
	pgu_dacx_dat_write(val_b32, 20);
}

// dacx_dcs_read_data_dac1
u32  pgu_dacx_dcs_read_data_dac1() {
	return pgu_dacx_dat_read(21);
}


// dacx_dcs_run_test
void pgu_dacx_dcs_run_test() {
	pgu_dacx_dat_write(0, 22);
}


// dacx_dcs_stop_test
void pgu_dacx_dcs_stop_test() {
	pgu_dacx_dat_write(0, 23);
}


// dacx_dcs_write_repeat
void pgu_dacx_dcs_write_repeat(u32 val_b32) {
	pgu_dacx_dat_write(val_b32, 24);
}

// dacx_dcs_read_repeat
u32  pgu_dacx_dcs_read_repeat() {
	return pgu_dacx_dat_read(25);
}

// dac0_fifo_write_data
	// see // data_count =  dev.WriteToPipeIn(pi, bdata) # (ep, bdata)  in dac0_fifo_write_data()
	// see // write_mcs_ep_pi_data 
	// see // write_mcs_ep_pi_buf // for dac0_fifo_write_buf to be.
void pgu_dac0_fifo_write_data(u32 val_b32) {
	// call pipe-in data 
	write_mcs_ep_pi_data(ADRS_BASE_PGU, EP_ADRS__DAC0_DAT_PI__PGU, val_b32);
}
// dac1_fifo_write_data
void pgu_dac1_fifo_write_data(u32 val_b32) {
	// call pipe-in data 
	write_mcs_ep_pi_data(ADRS_BASE_PGU, EP_ADRS__DAC1_DAT_PI__PGU, val_b32);
}

// dacx_fdcs_run_test
void pgu_dacx_fdcs_run_test() {
	pgu_dacx_dat_write(0, 28);
}
// dacx_fdcs_stop_test
void pgu_dacx_fdcs_stop_test() {
	pgu_dacx_dat_write(0, 29);
}

// TODO: dacx_fdcs_write_repeat
void pgu_dacx_fdcs_write_repeat(u32 val_b32) {
	pgu_dacx_dat_write(val_b32, 30);
}
// TODO: dacx_fdcs_read_repeat
u32  pgu_dacx_fdcs_read_repeat() {
	return pgu_dacx_dat_read(31);
}

// setup DACX 
u32  pgu_dacx_pg_setup() {
	
	// ## setup DCS configuration and data 
	// pgu.dacx_dcs_write_adrs     (0x00000000)
	// pgu.dacx_dcs_write_data_dac0(0x3FFF0008)
	// pgu.dacx_dcs_write_data_dac1(0x3FFF0002)
	pgu_dacx_dcs_write_adrs(0);
	pgu_dacx_dcs_write_data_dac0(0x3FFF0008);
	pgu_dacx_dcs_write_data_dac1(0x3FFF0002);
	// #
	// pgu.dacx_dcs_write_adrs     (0x00000001)
	// pgu.dacx_dcs_write_data_dac0(0x7FFF0010)
	// pgu.dacx_dcs_write_data_dac1(0x7FFF0004)
	pgu_dacx_dcs_write_adrs(1);
	pgu_dacx_dcs_write_data_dac0(0x7FFF0010);
	pgu_dacx_dcs_write_data_dac1(0x7FFF0004);
	// #
	// pgu.dacx_dcs_write_adrs     (0x00000002)
	// pgu.dacx_dcs_write_data_dac0(0x3FFF0008)
	// pgu.dacx_dcs_write_data_dac1(0x3FFF0002)
	pgu_dacx_dcs_write_adrs(2);
	pgu_dacx_dcs_write_data_dac0(0x3FFF0008);
	pgu_dacx_dcs_write_data_dac1(0x3FFF0002);
	// #
	// pgu.dacx_dcs_write_adrs     (0x00000003)
	// pgu.dacx_dcs_write_data_dac0(0x00000004)
	// pgu.dacx_dcs_write_data_dac1(0x00000001)
	pgu_dacx_dcs_write_adrs(3);
	pgu_dacx_dcs_write_data_dac0(0x00000004);
	pgu_dacx_dcs_write_data_dac1(0x00000001);
	// #
	// pgu.dacx_dcs_write_adrs     (0x00000004)
	// pgu.dacx_dcs_write_data_dac0(0xC0000008)
	// pgu.dacx_dcs_write_data_dac1(0xC0000002)
	pgu_dacx_dcs_write_adrs(4);
	pgu_dacx_dcs_write_data_dac0(0xC0000008);
	pgu_dacx_dcs_write_data_dac1(0xC0000002);
	// #
	// pgu.dacx_dcs_write_adrs     (0x00000005)
	// pgu.dacx_dcs_write_data_dac0(0x80000010)
	// pgu.dacx_dcs_write_data_dac1(0x80000004)
	pgu_dacx_dcs_write_adrs(5);
	pgu_dacx_dcs_write_data_dac0(0x80000010);
	pgu_dacx_dcs_write_data_dac1(0x80000004);
	// #
	// pgu.dacx_dcs_write_adrs     (0x00000006)
	// pgu.dacx_dcs_write_data_dac0(0xC0000008)
	// pgu.dacx_dcs_write_data_dac1(0xC0000002)
	pgu_dacx_dcs_write_adrs(6);
	pgu_dacx_dcs_write_data_dac0(0xC0000008);
	pgu_dacx_dcs_write_data_dac1(0xC0000002);
	// #
	// pgu.dacx_dcs_write_adrs     (0x00000007)
	// pgu.dacx_dcs_write_data_dac0(0x00000004)
	// pgu.dacx_dcs_write_data_dac1(0x00000001)
	pgu_dacx_dcs_write_adrs(7);
	pgu_dacx_dcs_write_data_dac0(0x00000004);
	pgu_dacx_dcs_write_data_dac1(0x00000001);
	// #
	// 
	// ## DCS test repeat setup 
	// #pgu.dacx_dcs_write_repeat  (0x00000000)
	// pgu.dacx_dcs_write_repeat  (0x00040001)	
	pgu_dacx_dcs_write_repeat(0x00040001);
	
	return 0;
}


//}




//}


//// CMU-CPU functions //{ 
/*

// common

u32 cmu_read_fpga_image_id() {
	u32 fpga_image_id;
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//print('\n>> {}'.format('Read FPGA image ID'))
#ifdef _MCS_DEBUG_
	xil_printf("\r\n>> %s", "Read FPGA image ID");
#endif
	//#
	//dev.UpdateWireOuts()
	//fpga_image_id = dev.GetWireOutValue(EP_ADRS['FPGA_IMAGE_ID'])
	fpga_image_id = read_mcs_ep_wo(EP_ADRS__FPGA_IMAGE_ID, MASK_ALL);
	//#
	//print('{} = {:#10x}'.format('fpga_image_id',fpga_image_id))
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s = 0x%08X", "fpga_image_id", fpga_image_id);
	//#
	//fpga_image_id_expected = EP_ADRS['ver']
	//print('{} = {}'.format('fpga_image_id_expected',fpga_image_id_expected))
	xil_printf("\r\n> %s = 0x%08X", "EP_ADRS__ver", EP_ADRS__ver);
	//#
	//if form_hex_32b(fpga_image_id) == fpga_image_id_expected:
	if (fpga_image_id == EP_ADRS__ver) 
		xil_printf("\r\n> %s", "fpga_image_id is same as expected!");
	//else:
	//	print('> fpga_image_id is NOT same as expected!')
	else 
		xil_printf("\r\n> %s", "fpga_image_id is NOT same as expected!");
#endif
	//#
	return fpga_image_id;	
}

u32 cmu_monitor_fpga() {
	u32 mon_fpga_temp_mC;
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//print('\n>> {}'.format('Monitor FPGA'))
#ifdef _MCS_DEBUG_
	xil_printf("\r\n>> %s", "Monitor FPGA");
#endif
	//#
	//dev.UpdateWireOuts()
	//mon_fpga_temp_mC = dev.GetWireOutValue(EP_ADRS['XADC_TEMP'])
	mon_fpga_temp_mC =  read_mcs_ep_wo(EP_ADRS__XADC_TEMP, MASK_ALL);
	//mon_fpga_volt_mV = dev.GetWireOutValue(EP_ADRS['XADC_VOLT'])
	//#
	//print('{}: {}\r'.format('FPGA temp[C]',mon_fpga_temp_mC/1000))
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s : %d", "FPGA temp[C]", mon_fpga_temp_mC/1000);
#endif
	//print('{}: {}\r'.format('FPGA volt[V]',mon_fpga_volt_mV/1000))
	//#
	//return [mon_fpga_temp_mC, mon_fpga_volt_mV]
	return mon_fpga_temp_mC;
}

u32 cmu_test_counter(u32 opt) {
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//if not opt:
	//	opt = 'OFF'
	//#
	//print('\n>> {}'.format('Test counter'))
#ifdef _MCS_DEBUG_
	xil_printf("\r\n>> %s", "Test counter");
#endif
	//#
	//# clear reset1
	//dev.SetWireInValue(EP_ADRS['TEST_CON'],0x00,0x01) # (ep,val,mask)
	write_mcs_ep_wi(EP_ADRS__TEST_CON, 0x00, 0x01);
	//# clear disable1
	//dev.SetWireInValue(EP_ADRS['TEST_CON'],0x00,0x02) # (ep,val,mask)
	write_mcs_ep_wi(EP_ADRS__TEST_CON, 0x00, 0x02);
	//#
	//if opt.upper()=='ON':
	if (opt==CMU_PAR__ON) {
	//	# set autocount2
	//	dev.SetWireInValue(EP_ADRS['TEST_CON'],0x04,0x04) # (ep,val,mask)
		write_mcs_ep_wi(EP_ADRS__TEST_CON, 0x04, 0x04);
	}
	//elif opt.upper()=='OFF':
	else if (opt==CMU_PAR__OFF) {
	//	# clear autocount2
	//	dev.SetWireInValue(EP_ADRS['TEST_CON'],0x00,0x04) # (ep,val,mask)
		write_mcs_ep_wi(EP_ADRS__TEST_CON, 0x00, 0x04);
	}
	//else:
	//	pass
	//#
	//dev.UpdateWireIns()
	//#
	//#
	//if opt.upper()=='RESET':
	if (opt==CMU_PAR__RESET) {
	//	# set reset2 // reset counter #2
	//	dev.ActivateTriggerIn(EP_ADRS['TEST_TI'], 0) # (ep,bit)
		activate_mcs_ep_ti(EP_ADRS__TEST_TI, 0);
	}
	//else:
	//	pass
	//#
#ifdef _MCS_DEBUG_
	//# read counters 
	u32 test_out;
	u32 count1;
	u32 count2;
	//dev.UpdateWireOuts()
	//test_out = dev.GetWireOutValue(EP_ADRS['TEST_OUT'])
	test_out =  read_mcs_ep_wo(EP_ADRS__TEST_OUT, MASK_ALL);
	//count1 = test_out & 0xFF
	count1 = test_out & 0xFF;
	//count2 = (test_out>>8) & 0xFF
	count2 = (test_out>>8) & 0xFF;
	xil_printf("\r\n> %s : %d", "count1", count1);
	xil_printf("\r\n> %s : %d", "count2", count2);
#endif
	//#
	//return [count1, count2]
	return 0;
}

u32 cmu_xxx_enable(const u8* msg_title, u32 wi, u32 wo) {
	xil_printf("\r\n>> %s", msg_title);
	write_mcs_ep_wi(wi, 0x01, 0x01);
	u32 flag;
	flag =  read_mcs_ep_wo(wo, MASK_ALL);
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s = 0x%08X", "flag", flag);
#endif
	flag = (flag&0x00000001);
	return flag;
}

u32 cmu_xxx_init(const u8* msg_title, u32 wi, u32 wo) {
	//xil_printf("\r\n>> %s", "Initialize SPO");
	xil_printf("\r\n>> %s", msg_title);
	//# set bit
	//write_mcs_ep_wi(EP_ADRS__SPO_CON, 0x02, 0x02); // set init bit
	write_mcs_ep_wi(wi, 0x02, 0x02); // set init bit
	//# reset bit
	//write_mcs_ep_wi(EP_ADRS__SPO_CON, 0x00, 0x02); // reset init bit
	write_mcs_ep_wi(wi, 0x00, 0x02); // reset init bit
	//#
	u32 cnt_done;
	u32 MAX_CNT;
	u32 flag;
	u32 init_done;
	//# check init_done flag
	cnt_done = 0;
	MAX_CNT = 20000;
	while (1) {
		//flag =  read_mcs_ep_wo(EP_ADRS__SPO_FLAG, MASK_ALL);
		flag =  read_mcs_ep_wo(wo, MASK_ALL);
		init_done = (flag&0x00000002)>>1;
#ifdef _MCS_DEBUG_
		xil_printf("\r\n> %s = 0x%08X", "flag", flag);
#endif
		if (init_done==1) break;
		cnt_done++;
		if (cnt_done>=MAX_CNT) break;
	}
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s = 0x%08X", "cnt_done", cnt_done);
	xil_printf("\r\n> %s = 0x%08X", "init_done", init_done);
#endif
	return init_done;
}

u32 cmu_xxx_update(const u8* msg_title, u32 wi, u32 wo) {
	//xil_printf("\r\n>> %s", "Update SPO");
	xil_printf("\r\n>> %s", msg_title);
	//# set bit
	//write_mcs_ep_wi(EP_ADRS__SPO_CON, 0x04, 0x04); // set init bit
	write_mcs_ep_wi(wi, 0x04, 0x04); // set init bit
	//# reset bit
	//write_mcs_ep_wi(EP_ADRS__SPO_CON, 0x00, 0x04); // reset init bit
	write_mcs_ep_wi(wi, 0x00, 0x04); // reset init bit
	//dev.UpdateWireIns()
	//#
	u32 cnt_done;
	u32 MAX_CNT;
	u32 flag;
	u32 update_done;
	//# check update_done flag
	cnt_done = 0;
	MAX_CNT = 20000;
	while (1) {
		//flag =  read_mcs_ep_wo(EP_ADRS__SPO_FLAG, MASK_ALL);
		flag =  read_mcs_ep_wo(wo, MASK_ALL);
		update_done = (flag&0x00000004)>>2;
		if (update_done==1) break;
		cnt_done++;
		if (cnt_done>=MAX_CNT) break;
	}
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s = 0x%08X", "cnt_done", cnt_done);
	xil_printf("\r\n> %s = 0x%08X", "update_done", update_done);
#endif
	return update_done;
}

u32 cmu_xxx_disable(const u8* msg_title, u32 wi, u32 wo) {
	xil_printf("\r\n>> %s", msg_title);
	write_mcs_ep_wi(wi, 0x00, 0x01);
	u32 flag;
	flag =  read_mcs_ep_wo(wo, MASK_ALL);
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s = 0x%08X", "flag", flag);
#endif
	flag = (flag&0x00000001);
	return flag;
}


// SPO 

u32 cmu_spo_enable() {
	u8* msg_title = (u8*)"Enable SPO";
	u32 wi = EP_ADRS__SPO_CON;
	u32 wo = EP_ADRS__SPO_FLAG;
	u32 flag;
	flag = cmu_xxx_enable(msg_title, wi, wo);
	//xil_printf("\r\n>> %s", "Enable SPO");
	//write_mcs_ep_wi(EP_ADRS__SPO_CON, 0x01, 0x01);
	//u32 flag;
	//flag =  read_mcs_ep_wo(EP_ADRS__SPO_FLAG, MASK_ALL);
	//xil_printf("\r\n> %s = 0x%08X", "flag", flag);
	//flag = (flag&0x00000001);
	return flag;
}

u32 cmu_spo_init() {
	u8* msg_title = (u8*)"Initialize SPO";
	u32 wi = EP_ADRS__SPO_CON;
	u32 wo = EP_ADRS__SPO_FLAG;
	u32 flag;
	//
	//# set parameters adrs_start: 0
	write_mcs_ep_wi(wi, 0x00<<4, 0x07<<4);
	//# set parameters num_bytes: 7
	write_mcs_ep_wi(wi, 0x07<<8, 0x07<<8);
	//
	// clear buffer
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B3_H, 0, MASK_ALL);
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B3_L, 0, MASK_ALL);
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B2_H, 0, MASK_ALL);
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B2_L, 0, MASK_ALL);
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B1_H, 0, MASK_ALL);
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B1_L, 0, MASK_ALL);
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B0_H, 0, MASK_ALL);
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B0_L, 0, MASK_ALL);
	//
	// common 
	flag = cmu_xxx_init(msg_title, wi, wo);
	return flag;
}

u32 cmu_spo_update() {
	u8* msg_title = (u8*)"Update SPO";
	u32 wi = EP_ADRS__SPO_CON;
	u32 wo = EP_ADRS__SPO_FLAG;
	u32 flag;
	flag = cmu_xxx_update(msg_title, wi, wo);
	return flag;
}

u32 cmu_spo_disable() {
	u8* msg_title = (u8*)"Disable SPO";
	u32 wi = EP_ADRS__SPO_CON;
	u32 wo = EP_ADRS__SPO_FLAG;
	u32 flag;
	flag = cmu_xxx_disable(msg_title, wi, wo);
	return flag;
}

u32 cmu_spo_set_buffer(u32 spo_idx, u32 val, u32 mask) {
//	dev = cmu_ctrl.dev
//	EP_ADRS = conf.OK_EP_ADRS_CONFIG
//	#
//	print('>> {}'.format('Set SPO buffer'))
	xil_printf("\r\n>> %s", "Set SPO buffer");
//	#
//	try:
//		ep = EP_ADRS[spo_idx]
//	except:
//		print('> spo_idx is expected as SPO_DIN_B#_L or SPO_DIN_B#_H.')
//		return False
//	#
#ifdef _MCS_DEBUG_
//	print('{} = 0x{:02X}'.format('ep',ep))
	xil_printf("\r\n> %s = 0x%02X", "spo_idx", spo_idx);
//	print('{} = 0x{:08X}'.format('val',val))
	xil_printf("\r\n> %s = 0x%08X", "val", val);
//	print('{} = 0x{:08X}'.format('mask',mask))
	xil_printf("\r\n> %s = 0x%08X", "mask", mask);
//	#
#endif
//	dev.SetWireInValue(ep,val,mask) 
	write_mcs_ep_wi(spo_idx, val, mask); 
//	dev.UpdateWireIns()
//	#
//	return True
	return 0;
}

u32 cmu_spo_read_buffer(u32 spo_idx) {
//	dev = cmu_ctrl.dev
//	EP_ADRS = conf.OK_EP_ADRS_CONFIG
//	#
//	print('>> {}'.format('Read SPO buffer'))
	xil_printf("\r\n>> %s", "Read SPO buffer");
//	#
//	try:
//		ep = EP_ADRS[spo_idx]
//	except:
//		print('> spo_idx is expected as SPO_MON_B#_L or SPO_MON_B#_H.')
//		return False
//	#
//	dev.UpdateWireOuts()
	u32 val;
//	val = dev.GetWireOutValue(ep)
	val =  read_mcs_ep_wo(spo_idx, MASK_ALL);
//	#
//	print('{} = 0x{:02X}'.format('ep',ep))
//	#
//	return val
	return val;
}

u32 cmu_spo_bit__leds(u32 opt) {
	//print('>>> {} : 0x{:02X}'.format('Control LEDs',opt))
	xil_printf("\r\n>>> %s : 0x%02X", "Control LEDs", opt);
	//#
	//try:
	u32 val;
	//	val = (opt&0xFF)<<8
	val = (opt&0xFF)<<8;
	//except:
	//	print('> opt is expected as 0x00 ~ 0xFF.')
	//	return False
	//mask = 0x0000FF00
	//cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B3_L, val, 0x0000FF00);
	//cmu_spo_update()
	cmu_spo_update();
	//
	// readback buffer
	val = cmu_spo_read_buffer(EP_ADRS__SPO_DIN_B3_L);
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s : 0x%08X", "EP_ADRS__SPO_DIN_B3_L", val);
#endif
	//return True
	return 0;
}

u32 cmu_spo_bit__amp_pwr  (u32 opt) {
	//print('>>> {} : {}'.format('Control AMP_PWR',opt))
	xil_printf("\r\n>>> %s : 0x%08X", "Control AMP_PWR", opt);
	//val_dict = {
	//	'OFF' : 0x00000000, 
	//	'ON'  : 0x00000001
	// }
	//try:
	u32 val;
	//	val = val_dict[opt]
	if (opt == CMU_PAR__ON) 
		val = 0x01;
	else 
		val = 0x00;
	//except:
	//	print('> opt is expected as OFF or ON.')
	//	return False
	//mask = 0x00000001
	//cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B3_L, val, 0x00000001);
	//cmu_spo_update()
	cmu_spo_update();
	//return True
	return 0;
}
	
u32 cmu_spo_bit__adc0_gain(u32 opt) {
//	print('>>> {} : {}'.format('Control ADC0 gain',opt))
	xil_printf("\r\n>>> %s : 0x%08X", "Control ADC0 gain", opt);
//	val_dict = {
//		'100X' : 0x00020000, 
//		'10X'  : 0x00010000, 
//		'1X'   : 0x00000000,
//		'OFF'  : 0x00000000
//	}
//	try:
	u32 val;
//		val = val_dict[opt]
	if      (opt == CMU_PAR__GN_100X) 
		val = 0x00020000;
	else if (opt == CMU_PAR__GN_10X) 
		val = 0x00010000;
	else if (opt == CMU_PAR__GN_1X) 
		val = 0x00000000;
	else if (opt == CMU_PAR__GN_OFF) 
		val = 0x00000000;
	else 
		val = 0x00000000;
//	except:
//		print('> opt is expected as 1X, 10X or 100X.')
//		return False
//	mask = 0x000F0000
//	cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B3_L, val, 0x000F0000);
//	cmu_spo_update()
	cmu_spo_update();
//	return True
	return 0;
}

u32 cmu_spo_bit__adc1_gain(u32 opt) {
//	print('>>> {} : {}'.format('Control ADC1 gain',opt))
	xil_printf("\r\n>>> %s : 0x%08X", "Control ADC1 gain", opt);
//	val_dict = {
//		'100X' : 0x00200000, 
//		'10X'  : 0x00100000, 
//		'1X'   : 0x00000000,
//		'OFF'  : 0x00000000
//	}
//	try:
	u32 val;
//		val = val_dict[opt]
	if      (opt == CMU_PAR__GN_100X) 
		val = 0x00020000;
	else if (opt == CMU_PAR__GN_10X) 
		val = 0x00010000;
	else if (opt == CMU_PAR__GN_1X) 
		val = 0x00000000;
	else if (opt == CMU_PAR__GN_OFF) 
		val = 0x00000000;
	else 
		val = 0x00000000;
//	except:
//		print('> opt is expected as 1X, 10X or 100X.')
//		return False
//	mask = 0x00F00000
//	cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B3_L, val, 0x00F00000);
//	cmu_spo_update()
	cmu_spo_update();
//	return True
	return 0;
}

u32 cmu_spo_bit__vi_bw    (u32 opt) {
//	print('>>> {} : {}'.format('Control vi bandwidth',opt))
	xil_printf("\r\n>>> %s : 0x%08X", "Control vi bandwidth", opt);
//	val_dict = {
//		'120K' : 0x08000000, 
//		'1M2'  : 0x04000000, 
//		'12M'  : 0x02000000, 
//		'120M' : 0x01000000, 
//		'OFF'  : 0x00000000
//	}
//	try:
	u32 val;
//		val = val_dict[opt]
	if      (opt == CMU_PAR__BW_120K) 
		val = 0x08000000;
	else if (opt == CMU_PAR__BW_1M2) 
		val = 0x04000000;
	else if (opt == CMU_PAR__BW_12M) 
		val = 0x02000000;
	else if (opt == CMU_PAR__BW_120M) 
		val = 0x01000000;
	else if (opt == CMU_PAR__BW_OFF) 
		val = 0x00000000;
	else 
		val = 0x00000000;
//	except:
//		print('> opt is expected as 1M2, 120K or ...')
//		return False
//	mask = 0x0F000000
//	cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B3_L, val, 0x0F000000);
//	cmu_spo_update()
	cmu_spo_update();
//	return True
	return 0;
}

u32 cmu_spo_bit__vq_bw    (u32 opt) {
//	print('>>> {} : {}'.format('Control vq bandwidth',opt))
	xil_printf("\r\n>>> %s : 0x%08X", "Control vq bandwidth", opt);
//	val_dict = {
//		'120K' : 0x80000000, 
//		'1M2'  : 0x40000000, 
//		'12M'  : 0x20000000, 
//		'120M' : 0x10000000, 
//		'OFF'  : 0x00000000
//	}
//	try:
	u32 val;
//		val = val_dict[opt]
	if      (opt == CMU_PAR__BW_120K) 
		val = 0x08000000;
	else if (opt == CMU_PAR__BW_1M2) 
		val = 0x04000000;
	else if (opt == CMU_PAR__BW_12M) 
		val = 0x02000000;
	else if (opt == CMU_PAR__BW_120M) 
		val = 0x01000000;
	else if (opt == CMU_PAR__BW_OFF) 
		val = 0x00000000;
	else 
		val = 0x00000000;
//	except:
//		print('> opt is expected as 1M2, 120K or ...')
//		return False
//	mask = 0xF0000000
//	cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B3_L, val, 0xF0000000);
//	cmu_spo_update()
	cmu_spo_update();
//	return True
	return 0;
}
	
// DAC_BIAS 
u32 cmu_dac_bias_enable() {
	u8* msg_title = (u8*)"Enable DAC_BIAS";
	u32 wi = EP_ADRS__DAC_BIAS_CON;
	u32 wo = EP_ADRS__DAC_BIAS_FLAG;
	u32 flag;
	flag = cmu_xxx_enable(msg_title, wi, wo);
	return flag;
}

u32 cmu_dac_bias_init() {
	u8* msg_title = (u8*)"Initialize DAC_BIAS";
	u32 wi = EP_ADRS__DAC_BIAS_CON;
	u32 wo = EP_ADRS__DAC_BIAS_FLAG;
	u32 flag;
	//
	//# set parameters ... forced pin output : 0x0B
	u32 val;
	val = 0x0B;
	//dev.SetWireInValue(wi,val<<16,0xFF<<16) # (ep,val,mask) 
	write_mcs_ep_wi(wi,val<<16,0xFF<<16);
	//# set parameters ... range : 0xFF for +/-10V; 0xAA for +/-5V.
	val = 0xFF;
	//dev.SetWireInValue(wi,val<<24,0xFF<<24) # (ep,val,mask) 
	write_mcs_ep_wi(wi,val<<24,0xFF<<24);
	//
	// common 
	flag = cmu_xxx_init(msg_title, wi, wo);
	return flag;
}

u32 cmu_dac_bias_update() {
	u8* msg_title = (u8*)"Update DAC_BIAS";
	u32 wi = EP_ADRS__DAC_BIAS_CON;
	u32 wo = EP_ADRS__DAC_BIAS_FLAG;
	u32 flag;
	flag = cmu_xxx_update(msg_title, wi, wo);
	return flag;	
}

u32 cmu_dac_bias_disable() {
	u8* msg_title = (u8*)"Disable DAC_BIAS";
	u32 wi = EP_ADRS__DAC_BIAS_CON;
	u32 wo = EP_ADRS__DAC_BIAS_FLAG;
	u32 flag;
	flag = cmu_xxx_disable(msg_title, wi, wo);
	return flag;
}

u32 cmu_dac_bias_set_buffer(u32 DAC1, u32 DAC2, u32 DAC3, u32 DAC4) {
	u8* msg_title = (u8*)"Set DAC_BIAS buffer";
	u32 wi21 = EP_ADRS__DAC_BIAS_DIN21;
	u32 wi43 = EP_ADRS__DAC_BIAS_DIN43;
	u32 DAC_21;
	u32 DAC_43;
	//
	xil_printf("\r\n>> %s", msg_title);
	//
	DAC_21 = (DAC2<<16)|DAC1;
	DAC_43 = (DAC4<<16)|DAC3;
	//
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s = 0x%08X", "DAC_21", DAC_21);
	xil_printf("\r\n> %s = 0x%08X", "DAC_43", DAC_43);
#endif
	//
	write_mcs_ep_wi(wi21, DAC_21, MASK_ALL);
	write_mcs_ep_wi(wi43, DAC_43, MASK_ALL);
	return 0;
}

u32 cmu_dac_bias_readback(u32 *p_DACx) {
	u8* msg_title = (u8*)"Readback DAC_BIAS";
	u32 wo21 = EP_ADRS__DAC_BIAS_RB21;
	u32 wo43 = EP_ADRS__DAC_BIAS_RB43;
	u32 DAC_21;
	u32 DAC_43;
	u32 DAC1;
	u32 DAC2;
	u32 DAC3;
	u32 DAC4;
	//
	xil_printf("\r\n>> %s", msg_title);
	//
	DAC_21 = read_mcs_ep_wo(wo21, MASK_ALL);
	DAC_43 = read_mcs_ep_wo(wo43, MASK_ALL);
	//
	DAC1 = (DAC_21    )&0xFFFF;
	DAC2 = (DAC_21>>16)&0xFFFF;
	DAC3 = (DAC_43    )&0xFFFF;
	DAC4 = (DAC_43>>16)&0xFFFF;
	//
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s = 0x%04X", "DAC1", DAC1);
	xil_printf("\r\n> %s = 0x%04X", "DAC2", DAC2);
	xil_printf("\r\n> %s = 0x%04X", "DAC3", DAC3);
	xil_printf("\r\n> %s = 0x%04X", "DAC4", DAC4);
#endif
	//
	p_DACx[0] = DAC1;
	p_DACx[1] = DAC2;
	p_DACx[2] = DAC3;
	p_DACx[3] = DAC4;
	//
	return 0;
}

// DAC_A2A3
//... 

// DWAVE

u32 cmu_dwave_enable() {
	u8* msg_title = (u8*)"Enable DWAVE";
	u32 wi = EP_ADRS__DWAVE_CON;
	u32 wo = EP_ADRS__DWAVE_FLAG;
	u32 flag;
	flag = cmu_xxx_enable(msg_title, wi, wo);
	return flag;
}

u32 cmu_dwave_disable() {
	u8* msg_title = (u8*)"Disable DWAVE";
	u32 wi = EP_ADRS__DWAVE_CON;
	u32 wo = EP_ADRS__DWAVE_FLAG;
	u32 flag;
	flag = cmu_xxx_disable(msg_title, wi, wo);
	return flag;
}

u32 cmu_dwave_read_base_freq() {
	u8* msg_title = (u8*)"Read DWAVE_BASE_FREQ";
	u32 wo = EP_ADRS__DWAVE_BASE_FREQ;
	u32 dwave_base_freq;
	//
	xil_printf("\r\n>> %s", msg_title);
	dwave_base_freq = read_mcs_ep_wo(wo, MASK_ALL);
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s = %d", "dwave_base_freq", dwave_base_freq);
#endif
	return dwave_base_freq;
}

u32 cmu_dwave_rd__trig(u32 bit_loc) {
	u8* msg_title = (u8*)"Trigger read DWAVE";
	u32 ti = EP_ADRS__DWAVE_TI;
	u32 wo = EP_ADRS__DWAVE_DOUT_BY_TRIG;
	u32 val;
	//
	xil_printf("\r\n>> %s TI[%d]", msg_title,bit_loc);
	activate_mcs_ep_ti(ti,bit_loc);
	val = read_mcs_ep_wo(wo, MASK_ALL);
	return val;
}

u32 cmu_dwave_wr__trig(u32 bit_loc, u32 val) {
	u8* msg_title = (u8*)"Trigger write DWAVE";
	u32 ti = EP_ADRS__DWAVE_TI;
	u32 wi = EP_ADRS__DWAVE_DIN_BY_TRIG;
	//
	xil_printf("\r\n>> %s TI[%d] : %d", msg_title,bit_loc,val);
	write_mcs_ep_wi(wi, val, MASK_ALL);
	activate_mcs_ep_ti(ti,bit_loc);
	return val;
}

u32 cmu_dwave_wr_cnt_period(u32 val) {
	u8* msg_title = (u8*)"Write DWAVE cnt_period";
	xil_printf("\r\n>>> %s : %d", msg_title, val);
	cmu_dwave_wr__trig(16, val);
	return 0;
}

u32 cmu_dwave_rd_cnt_period() {
	u8* msg_title = (u8*)"Read DWAVE cnt_period";
	u32 val;
	//
	val = cmu_dwave_rd__trig(24);
	xil_printf("\r\n>>> %s : %d", msg_title, val);
	return val;
}


//....

*/ 
//} 


//// PGU SCPI exec functions // based on MCS-EP subfunctions //{
// 
// *RST <NL>			
// *IDN? <NL>			
// 			
// end-point access //{
//
// :PGEP:EN ON|OFF <NL>			
// :PGEP:EN? <NL>			
// 			
// :PGEP:WIMK  #Hnnnnnnnn <NL>			
// :PGEP:WIMK? <NL>			
// 			
// :PGEP:WOMK  #Hnnnnnnnn <NL>			
// :PGEP:WOMK? <NL>			
// 			
// :PGEP:TIMK  #Hnnnnnnnn <NL>			
// :PGEP:TIMK? <NL>			
// 			
// :PGEP:TOMK  #Hnnnnnnnn <NL>			
// :PGEP:TOMK? <NL>			
// 			
// 			
// :PGEP:WI#Hnn  #Hnnnnnnnn <NL>			
// :PGEP:WI#Hnn? <NL>			
// 			
// :PGEP:WO#Hnn  #Hnnnnnnnn <NL>			
// :PGEP:WO#Hnn? <NL>			
// 			
// :PGEP:TI#Hnn  #Hnnnnnnnn <NL>			
// :PGEP:TI#Hnn? <NL>			
// 			
// :PGEP:TO#Hnn  #Hnnnnnnnn <NL>			
// :PGEP:TO#Hnn? <NL>			
// 			
// :PGEP:PI#Hnn  #m_nnnnnn_rrrrrrrrrrrrrrrrrrrrr <NL>			
// :PGEP:PI#Hnn? <NL>			
// 			
// :PGEP:PO#Hnn  #Hnnnnnnnn <NL>			
// :PGEP:PO#Hnn? <NL>		
//	
//}

// PGU commands //{
//
// pgu power supply  on / off
// :PGU:PWR   ON|OFF <NL>	
//
// pgu sw trig on / off
// :PGU:TRIG   ON|OFF <NL>	
//
// duration count and dac code setting for DAC0 and DAC1 channels
// :PGU:DAC0:PNT#Hnnn:DUR  #Hnnnn
// :PGU:DAC0:PNT#Hnnn:VAL  #Hnnnn
// :PGU:DAC1:PNT#Hnnn:DUR  #Hnnnn
// :PGU:DAC1:PNT#Hnnn:VAL  #Hnnnn
// :PGU:DAC0:FDCS #8_nnnn_rrrrrrrrrrrrrrrrrrrrrrrr  <NL>			
// :PGU:DAC1:FDCS #8_nnnn_rrrrrrrrrrrrrrrrrrrrrrrr  <NL>			
//
// dac update frequency in MHz (decimal)
// :PGU:FREQ  nnnn
//
// dac gain/offset setting 
// :PGU:DAC0:GAIN #Hnnnn
// :PGU:DAC0:OFST #Hnnnn
// :PGU:DAC1:GAIN #Hnnnn
// :PGU:DAC1:OFST #Hnnnn
//
//}

//}

//// CMU SCPI exec functions // based on MCS-EP subfunctions //{
// 
// *RST <NL>			
// *IDN? <NL>			
// 			
// :CMEP:EN ON|OFF <NL>			
// :CMEP:EN? <NL>			
// 			
// :CMEP:WIMK  #Hnnnnnnnn <NL>			
// :CMEP:WIMK? <NL>			
// 			
// :CMEP:WOMK  #Hnnnnnnnn <NL>			
// :CMEP:WOMK? <NL>			
// 			
// :CMEP:TIMK  #Hnnnnnnnn <NL>			
// :CMEP:TIMK? <NL>			
// 			
// :CMEP:TOMK  #Hnnnnnnnn <NL>			
// :CMEP:TOMK? <NL>			
// 			
// 			
// :CMEP:WI#Hnn  #Hnnnnnnnn <NL>			
// :CMEP:WI#Hnn? <NL>			
// 			
// :CMEP:WO#Hnn  #Hnnnnnnnn <NL>			
// :CMEP:WO#Hnn? <NL>			
// 			
// :CMEP:TI#Hnn  #Hnnnnnnnn <NL>			
// :CMEP:TI#Hnn? <NL>			
// 			
// :CMEP:TO#Hnn  #Hnnnnnnnn <NL>			
// :CMEP:TO#Hnn? <NL>			
// 			
// :CMEP:PI#Hnn  #m_nnnnnn_ rrrrrrrrrrrrrrrrrrrrr <NL>			
// :CMEP:PI#Hnn? <NL>			
// 			
// :CMEP:PO#Hnn  #Hnnnnnnnn <NL>			
// :CMEP:PO#Hnn? <NL>			

//}




























