clear;
close all;
format short e;
pkg load signal;

tr=20e-9;
tf=tr;
ton=20e-9;
td=ton;
ttail=ton;

tint=2.5e-9;
N=16;

%%%%%% input delay time & signal
ptime1=[0:tint:td-tint];
pwave1=ones(size(ptime1))*0;

%%%%%% input rising time & signal
ptime=[0:tint:tr];
p1=linspace(-1,1,length(ptime));
p2=uencode(p1,N);
pwave2=(p2/2^N)*0.02;
ptime2=ptime+td;

%%%%%% input on time & signal
ptime3=[0:tint:ton-tint]+td+tr;
pwave3=ones(size(ptime3))*20e-3;

%%%%%% input falling time & signal
ptime=[0:tint:tf];
p1=linspace(-1,1,length(ptime));
p2=uencode(p1,N);
p3=(p2/2^N)*0.02;
pwave4=0.02-p3;
ptime4=ptime+td+tr+ton;

%%%%%% input end time & singal
ptime5=[0:tint:ton-tint]+td+tr+ton+tf;
pwave5=ones(size(ptime5))*0;

%%%%%% input rising time & signal
ptime=[0:tint:tr];
p1=linspace(-1,1,length(ptime));
p2=uencode(p1,N);
pwave6=(p2/2^N)*0.02;
ptime6=ptime+td+tr+ton+tf+ttail;

%%%%%% input on time & signal
ptime7=[0:tint:ton-tint]+td+tr+ton+tf+ttail+tr;
pwave7=ones(size(ptime3))*20e-3;

%%%%%% concatenation
time=[ptime1 ptime2 ptime3 ptime4 ptime5 ptime6 ptime7];
pwave=[pwave1 pwave2 pwave3 pwave4 pwave5 pwave6 pwave7];
nwave=20e-3-pwave;

time(end)
plot(time,pwave,'o',time,nwave,'x');

t=num2str(tr);
pname=strcat("slew",t,"P.txt");
nname=strcat("slew",t,"N.txt");
dlmwrite(pname,[time' pwave']," ");
dlmwrite(nname,[time' nwave']," ");